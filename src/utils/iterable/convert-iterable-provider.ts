import { ValueOrPromise } from '../reflection';
import { IterableProvider } from './interfaces';

/** Converts iterable items. */
export abstract class ConvertIterableProvider<TSource, TTarget> implements IterableProvider<TTarget> {
    constructor(
        private readonly innerProvider: IterableProvider<TSource>,
    ) {}

    async *iterate(): AsyncIterableIterator<TTarget> {
        for await (const sourceItem of this.innerProvider.iterate()) {
            const targetItem = await this.convertItem(sourceItem);
            yield targetItem;
        }
    }

    abstract convertItem(item: TSource): ValueOrPromise<TTarget>;
}
