export interface IterableProvider<TItem> {
    iterate(): AsyncIterableIterator<TItem>;
}

export interface ItemProcessor<TItem> {
    processItem(item: TItem): void;
}

export interface InfoProvider<TValue> {
    getInfo(value: TValue): unknown;
}

export interface Identifier<TValue> {
    getId(value: TValue): string;
}
