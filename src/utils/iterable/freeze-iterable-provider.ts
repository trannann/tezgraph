import { deepFreeze } from '../conversion';
import { DeepReadonly } from '../reflection';
import { ConvertIterableProvider } from './convert-iterable-provider';

/**
 * Deep-freezes items to prevent their further modification
 * including hacks (e.g. Object.assign()) if they are already read-only on TypeScript level.
 */
export class FreezeIterableProvider<TItem extends object> extends ConvertIterableProvider<TItem, DeepReadonly<TItem>> {
    convertItem(item: TItem): DeepReadonly<TItem> {
        return deepFreeze(item);
    }
}
