import { Logger } from '../logging';
import { Clock } from '../time/clock';
import { InfoProvider, ItemProcessor, IterableProvider } from './interfaces';

/** Processes items from given iterable one by one in a loop. */
export class IterableProcessor<TItem = unknown> {
    currentIterable: AsyncIterableIterator<TItem> | undefined;
    lastItemInfo: unknown;
    lastItemError: unknown;
    lastSuccessTime: Date = new Date(0);

    constructor(
        private readonly iterableProvider: IterableProvider<TItem>,
        private readonly itemProcessor: ItemProcessor<TItem>,
        private readonly itemInfoProvider: InfoProvider<TItem>,
        private readonly clock: Clock,
        private readonly logger: Logger,
    ) {}

    async processItems(): Promise<void> {
        this.logger.logInformation('Starting to process items.');
        this.currentIterable = this.iterableProvider.iterate();

        for await (const item of this.currentIterable) {
            try {
                this.lastItemInfo = this.itemInfoProvider.getInfo(item);
                this.itemProcessor.processItem(item);

                this.lastItemError = undefined;
                this.lastSuccessTime = this.clock.getNowDate();
            } catch (error: unknown) {
                this.lastItemError = error;
                this.logger.logError('Failed to process {item} because of {error}.', {
                    item: this.lastItemInfo,
                    error,
                });
            }
        }

        this.currentIterable = undefined;
        this.logger.logInformation('Stopped.');
    }
}
