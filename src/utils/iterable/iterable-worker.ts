import { BackgroundWorker } from '../../bootstrap/background-worker';
import { IterableProcessor } from './iterable-processor';

/** Executes given iterable processor on the background of the app. */
export class IterableWorker implements BackgroundWorker {
    constructor(
        readonly name: string,
        private readonly processor: IterableProcessor,
    ) {}

    start(): void {
        if (this.processor.currentIterable) {
            throw new Error('This cannot be started because it is already running.');
        }

        void this.processor.processItems();
    }

    async stop(): Promise<void> {
        if (!this.processor.currentIterable?.return) {
            throw new Error('This cannot be stopped because it is NOT running or missing return.');
        }

        await this.processor.currentIterable.return();
        this.processor.currentIterable = undefined;
    }
}
