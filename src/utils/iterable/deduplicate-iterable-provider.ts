import { FixedQueue } from '../collections/fixed-queue';
import { Identifier, IterableProvider } from './interfaces';

/** Removes duplicates by ID from given iterable. */
export class DeduplicateIterableProvider<TItem> implements IterableProvider<TItem> {
    private readonly lastIds: FixedQueue<string>;

    constructor(
        private readonly innerProvider: IterableProvider<TItem>,
        private readonly identifier: Identifier<TItem>,
        numberOfLastItemsToCheck: number,
    ) {
        this.lastIds = new FixedQueue(numberOfLastItemsToCheck);
    }

    async *iterate(): AsyncIterableIterator<TItem> {
        for await (const item of this.innerProvider.iterate()) {
            const itemId = this.identifier.getId(item);

            if (!this.lastIds.has(itemId)) {
                this.lastIds.add(itemId);
                yield item;
            }
        }
    }
}
