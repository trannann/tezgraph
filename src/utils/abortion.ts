export function isAbortError(error: unknown): boolean {
    return error instanceof Error && error.name === 'AbortError';
}

export function addAbortListener(signal: AbortSignal, listener: () => void): void {
    signal.addEventListener('abort', listener);
}
