import { InjectionToken } from 'tsyringe';

import { ValueOrPromise } from '../reflection';

export enum HealthStatus {
    Degraded = 'Degraded',
    Healthy = 'Healthy',
    Unhealthy = 'Unhealthy',
}

export interface HealthCheckResult {
    readonly status: HealthStatus;
    readonly data: unknown;
    readonly evaluatedOn?: Date;
}

/** Common top level interface for health checks. */
export interface HealthCheck {
    readonly name: string;
    checkHealth(): ValueOrPromise<HealthCheckResult>;
}

export const healthChecksDIToken: InjectionToken<HealthCheck> = 'HealthChecks';
