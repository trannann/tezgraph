import express from 'express';
import { singleton } from 'tsyringe';

import { ContentType, HttpHeader, HttpStatusCode } from '../http-constants';
import { HttpRoute, HttpRouteProvider } from '../http-route-provider';
import { safeJsonStringify } from '../safe-json-stringifier';
import { HealthProvider } from './health-provider';

@singleton()
export class HealthHttpHandler implements HttpRouteProvider {
    constructor(private readonly healthProvider: HealthProvider) {}

    get routes(): readonly HttpRoute[] {
        return [
            { path: '/health', handler: this.getHealthStatus.bind(this) },
            { path: '/check', handler: this.getHealthCheck.bind(this) },
        ];
    }

    async getHealthStatus(response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        response.setHeader(HttpHeader.ContentType, ContentType.Json);
        response.send(safeJsonStringify(report, { indent: 2 }));
    }

    async getHealthCheck(response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        if (!report.isHealthy) {
            response.sendStatus(HttpStatusCode.InternalServerError);
        } else {
            response.send('OK');
        }
    }

    async checkIsHealthy(): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        if (!report.isHealthy) {
            throw new Error('Health check error.');
        }
    }
}
