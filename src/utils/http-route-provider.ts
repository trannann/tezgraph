import express from 'express';
import { InjectionToken } from 'tsyringe';

import { ValueOrPromise } from './reflection';

export interface HttpRoute {
    readonly path: string;
    handler(response: express.Response, request: express.Request): ValueOrPromise<void>;
}

export interface HttpRouteProvider {
    readonly routes: readonly HttpRoute[];
}

export const httpRouteProviderDIToken: InjectionToken<HttpRouteProvider> = 'HttpRouteProvider';
