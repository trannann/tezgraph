import { singleton } from 'tsyringe';

/** Usable mainly for delays for given amount of milliseconds.  */
@singleton()
export class SleepHelper {
    async sleep(millis: number): Promise<void> {
        return new Promise(resolve => {
            setTimeout(resolve, millis);
        });
    }
}
