import { singleton } from 'tsyringe';

import { BackgroundWorker } from '../../bootstrap/background-worker';
import { injectLogger, Logger } from '../logging';
import { MetricsContainer } from '../metrics/metrics-container';
import { VersionProvider } from './version-provider';

export const UNKNOWN_LABEL = '<unknown>';

@singleton()
export class VersionMetricsWorker implements BackgroundWorker {
    name = 'VersionMetrics';

    constructor(
        private readonly versionProvider: VersionProvider,
        private readonly metrics: MetricsContainer,
        @injectLogger(VersionMetricsWorker) private readonly logger: Logger,
    ) {}

    start(): void {
        const version = this.versionProvider.version;
        if (!version.git_sha || !version.tag) {
            this.logger.logWarning('Unable to read version while collecting build info metrics.');
        }

        this.metrics.buildInfoCounter.inc({
            git_sha: version.git_sha ?? UNKNOWN_LABEL,
            tag: version.tag ?? UNKNOWN_LABEL,
        });
    }

    stop(): void {
        // Nothing to do.
    }
}
