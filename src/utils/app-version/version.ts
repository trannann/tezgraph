import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class Version {
    @Field(() => String, {
        nullable: true,
        description: 'The name of the application.',
    })
    readonly name!: string | null;

    @Field(() => String, {
        nullable: true,
        description: 'The git commit hash.',
    })
    readonly git_sha!: string | null;

    @Field(() => String, {
        nullable: true,
        description: 'The version of the application.',
    })
    readonly tag!: string | null;

    @Field(() => String, {
        nullable: true,
        description: 'The URL to the release notes.',
    })
    readonly release_notes!: string | null;
}
