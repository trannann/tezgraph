import { Counter, Metric, Registry } from 'prom-client';
import { singleton } from 'tsyringe';

import { getEnumValues } from '../reflection';

export enum BuildInfoLabel {
    GitSha = 'git_sha',
    Tag = 'tag',
}

export enum GraphQLQueryMethodLabel {
    Method = 'method',
    Service = 'service',
}

export enum GraphQLErrorCounterLabel {
    Type = 'type',
    Field = 'field',
    Service = 'service',
}

const doNotRegisterGlobally: Registry[] = [];

export type MetricsRecord<TKey extends string = string> = Record<TKey, Metric<string>>;

/** Container with Prometheus metric objects. */
@singleton()
export class MetricsContainer implements MetricsRecord<keyof MetricsContainer> {
    readonly buildInfoCounter = new Counter({
        name: 'tezgraph_build_info',
        help: 'TezGraph build info: git commit hash and git tag name',
        labelNames: getEnumValues(BuildInfoLabel),
        registers: doNotRegisterGlobally,
    });

    readonly graphQLQueryMethodCounter = new Counter({
        name: 'graphql_query_method_total',
        help: 'TezGraph query calls',
        labelNames: getEnumValues(GraphQLQueryMethodLabel),
        registers: doNotRegisterGlobally,
    });

    readonly graphQLErrorCounter = new Counter({
        name: 'graphql_resolver_error_total',
        help: 'TezGraph resolvers errors',
        labelNames: getEnumValues(GraphQLErrorCounterLabel),
        registers: doNotRegisterGlobally,
    });
}
