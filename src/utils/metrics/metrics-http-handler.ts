import express from 'express';
import { Registry } from 'prom-client';
import { singleton } from 'tsyringe';

import { HttpHeader } from '../http-constants';
import { HttpRoute, HttpRouteProvider } from '../http-route-provider';

/** Exposes Prometheus metrics as a web page at particular HTTP path. */
@singleton()
export class MetricsHttpHandler implements HttpRouteProvider {
    constructor(private readonly prometheusRegistry: Registry) {}

    get routes(): readonly HttpRoute[] {
        return [{ path: '/metrics', handler: this.getMetrics.bind(this) }];
    }

    async getMetrics(response: express.Response): Promise<void> {
        const metrics = await this.prometheusRegistry.metrics();

        response.setHeader(HttpHeader.ContentType, this.prometheusRegistry.contentType);
        response.send(metrics);
    }
}
