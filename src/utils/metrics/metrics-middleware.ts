import { singleton } from 'tsyringe';
import { MiddlewareInterface, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../bootstrap/resolver-context';
import { MetricsContainer } from './metrics-container';

export const SERVICE_NAME = 'indexer';

@singleton()
export class MetricsMiddleware implements MiddlewareInterface<ResolverContext> {
    constructor(private readonly metrics: MetricsContainer) {}

    async use(resolverData: ResolverData<ResolverContext>, next: () => Promise<unknown>): Promise<unknown> {
        const typeName = resolverData.info.parentType.name;
        const fieldName = resolverData.info.fieldName;

        if (['Query', 'Mutation'].includes(typeName)) {
            this.metrics.graphQLQueryMethodCounter.inc({
                method: fieldName,
                service: SERVICE_NAME,
            });
        }

        try {
            return await next();
        } catch (error: unknown) {
            this.metrics.graphQLErrorCounter.inc({
                type: typeName,
                field: fieldName,
                service: SERVICE_NAME,
            });
            throw error;
        }
    }
}
