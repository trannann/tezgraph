import { injectWithTransform, singleton } from 'tsyringe';
import Transform from 'tsyringe/dist/typings/types/transform';

import { Logger } from './logger';
import { LoggerCategory, LoggerFactory } from './logger-factory';

@singleton()
class LoggerTransformer implements Transform<LoggerFactory, Logger> {
    transform(loggerFactory: LoggerFactory, category: LoggerCategory): Logger {
        return loggerFactory.getLogger(category);
    }
}

/** Used on constructor parameter for dependency injection of a logger for the specified category. */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type, @typescript-eslint/explicit-module-boundary-types
export function injectLogger(category: LoggerCategory) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call
    return injectWithTransform(LoggerFactory, LoggerTransformer, category);
}
