import { singleton } from 'tsyringe';

import { SleepHelper } from './time/sleep-helper';

@singleton()
export class RetryHelper {
    constructor(private readonly sleepHelper: SleepHelper) {}

    async execute<TResponse>(
        func: () => Promise<TResponse>,
        retryDelaysMillis: readonly number[],
    ): Promise<TResponse> {
        try {
            return await func();
        } catch (error: unknown) {
            if (retryDelaysMillis[0] !== undefined) {
                await this.sleepHelper.sleep(retryDelaysMillis[0]);
                return await this.execute(func, retryDelaysMillis.slice(1));
            }
            throw error;
        }
    }
}
