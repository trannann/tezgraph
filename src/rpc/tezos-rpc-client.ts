import { BlockHeaderResponse, BlockResponse, RpcClient } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EnvConfig } from '../utils/configuration/env-config';
import { injectLogger, Logger } from '../utils/logging';
import { RetryHelper } from '../utils/retry-helper';

@singleton()
export class TezosRpcClient {
    constructor(
        private readonly rpcClient: RpcClient,
        private readonly retryHelper: RetryHelper,
        private readonly envConfig: EnvConfig,
        @injectLogger(TezosRpcClient) private readonly logger: Logger,
    ) {}

    async getBlock(hashOrLevel: string | number): Promise<BlockResponse> {
        return this.retry(async () => {
            this.logger.logDebug('Calling getBlock({hashOrLevel}).', { hashOrLevel });
            const block = await this.rpcClient.getBlock({ block: hashOrLevel.toString() });

            this.logger.logDebug('Received block with {hash}.', { block, hash: block.hash });
            return block;
        });
    }

    async getHeadBlockHeader(): Promise<BlockHeaderResponse> {
        return this.retry(async () => {
            this.logger.logDebug('Calling getHeadBlockHeader().');
            const blockHeader = await this.rpcClient.getBlockHeader();

            this.logger.logDebug('Received block header with {hash}.', { blockHeader, hash: blockHeader.hash });
            return blockHeader;
        });
    }

    private async retry<TData>(getData: () => Promise<TData>): Promise<TData> {
        return this.retryHelper.execute(getData, this.envConfig.rpcRetryDelaysMillis);
    }
}
