import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { ActivateAccountNotification } from '../../entity/subscriptions/activate-account-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';

type RpcActivateAccount = rpc.OperationContentsActivateAccount | rpc.OperationContentsAndResultActivateAccount;

@singleton()
export class ActivateAccountConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: RpcActivateAccount, baseProperties: BaseOperationProperties): ActivateAccountNotification {
        return {
            ...baseProperties,
            kind: OperationKind.activate_account,
            pkh: rpcOperation.pkh,
            secret: rpcOperation.secret,
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }
}
