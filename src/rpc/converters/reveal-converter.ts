import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { RevealNotification } from '../../entity/subscriptions/reveal-notification';
import { convertBigInt, convertMutez } from './common/big-int-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';
import { RevealResultConverter } from './operation-results/reveal-result-converter';

type RpcReveal = rpc.OperationContentsReveal | rpc.OperationContentsAndResultReveal;

@singleton()
export class RevealConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: RevealResultConverter,
    ) {}

    convert(rpcOperation: RpcReveal, baseProperties: BaseOperationProperties): RevealNotification {
        return {
            ...baseProperties,
            kind: OperationKind.reveal,
            source: rpcOperation.source,
            fee: convertMutez(rpcOperation.fee),
            counter: convertBigInt(rpcOperation.counter),
            gas_limit: convertBigInt(rpcOperation.gas_limit),
            storage_limit: convertBigInt(rpcOperation.storage_limit),
            public_key: rpcOperation.public_key,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        };
    }
}
