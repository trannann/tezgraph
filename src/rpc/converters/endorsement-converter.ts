import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EndorsementNotification } from '../../entity/subscriptions/endorsement-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { BalanceUpdateConverter } from './common/balance-update-converter';
import { BaseOperationProperties } from './operation-converter';

export type RpcEndorsement = rpc.OperationContentsEndorsement | rpc.OperationContentsAndResultEndorsement;

@singleton()
export class EndorsementConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcOperation: RpcEndorsement, baseProperties: BaseOperationProperties): EndorsementNotification {
        return {
            ...baseProperties,
            kind: OperationKind.endorsement,
            level: rpcOperation.level,
            metadata: 'metadata' in rpcOperation
                ? {
                    balance_updates: this.balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates),
                    delegate: rpcOperation.metadata.delegate,
                    slots: rpcOperation.metadata.slots,
                }
                : undefined,
        };
    }
}
