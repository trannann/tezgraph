import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BallotNotification, BallotVote } from '../../entity/subscriptions/ballot-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { BaseOperationProperties } from './operation-converter';

type RpcBallot = rpc.OperationContentsBallot | rpc.OperationContentsAndResultBallot;

@singleton()
export class BallotConverter {
    convert(rpcOperation: RpcBallot, baseProperties: BaseOperationProperties): BallotNotification {
        return {
            ...baseProperties,
            kind: OperationKind.ballot,
            source: rpcOperation.source,
            period: rpcOperation.period,
            proposal: rpcOperation.proposal,
            ballot: BallotVote[rpcOperation.ballot],
        };
    }
}
