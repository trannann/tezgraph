import { errorToString } from '../../../utils/conversion';
import { Nullish } from '../../../utils/reflection';

/* Dedicated functions for nullable b/c we don't trust external source which is Tezos RPC. */

export function convertBigInt(rawValue: string): bigint {
    return convert(rawValue, 'big int', ensureNonNullish);
}

export function convertNullishBigInt(rawValue: Nullish<string>): bigint | undefined {
    return convert(rawValue, 'nullish big int', v => v);
}

export function convertMutez(rawValue: string): bigint {
    return convert(rawValue, 'mutez', v => ensureNonNullish(ensurePositive(v)));
}

export function convertNullishMutez(rawValue: Nullish<string>): bigint | undefined {
    return convert(rawValue, 'nullish mutez', v => ensurePositive(v));
}

function convert<TResult>(
    rawValue: Nullish<string>,
    valueDescription: string,
    transform: (v: bigint | undefined) => TResult,
): TResult {
    try {
        // Beware: BigInt(empty/white-space string) returns zero.
        const int = rawValue?.trim() ? BigInt(rawValue) : undefined;
        return transform(int);
    } catch (error: unknown) {
        throw new Error(`Received invalid ${valueDescription} ${JSON.stringify(rawValue)} from RPC. ${errorToString(error)}`);
    }
}

function ensureNonNullish(value: bigint | undefined): bigint {
    if (value === undefined) {
        throw new Error(`Value can't be nullish nor empty string.`);
    }
    return value;
}

function ensurePositive(value: bigint | undefined): bigint | undefined {
    if (value !== undefined && value < BigInt('0')) {
        throw new Error('Value must positive or zero.');
    }
    return value;
}
