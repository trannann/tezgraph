import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BalanceUpdate, BalanceUpdateCategory, BalanceUpdateKind } from '../../../entity/subscriptions/balance-update';
import { Nullish } from '../../../utils/reflection';
import { convertBigInt } from './big-int-converter';

export type RpcBalanceUpdate = rpc.OperationMetadataBalanceUpdates | rpc.OperationBalanceUpdatesItem;

@singleton()
export class BalanceUpdateConverter {
    convertNullish(rpcUpdates: Nullish<RpcBalanceUpdate[]>): readonly BalanceUpdate[] | undefined {
        return rpcUpdates ? this.convert(rpcUpdates) : undefined;
    }

    convert(rpcUpdates: RpcBalanceUpdate[]): readonly BalanceUpdate[] {
        return rpcUpdates.map(u => ({
            kind: BalanceUpdateKind[u.kind],
            category: u.category ? BalanceUpdateCategory[u.category] : undefined,
            contract: u.contract,
            delegate: u.delegate,
            cycle: u.cycle,
            change: convertBigInt(u.change),
        }));
    }
}
