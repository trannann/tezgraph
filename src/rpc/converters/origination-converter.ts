import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { OriginationNotification } from '../../entity/subscriptions/origination-notification';
import { convertBigInt, convertMutez } from './common/big-int-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';
import { OriginationResultConverter } from './operation-results/origination-result-converter';

type RpcOrigination = rpc.OperationContentsOrigination | rpc.OperationContentsAndResultOrigination;

@singleton()
export class OriginationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: OriginationResultConverter,
    ) {}

    convert(rpcOperation: RpcOrigination, baseProperties: BaseOperationProperties): OriginationNotification {
        return {
            ...baseProperties,
            kind: OperationKind.origination,
            source: rpcOperation.source,
            fee: convertMutez(rpcOperation.fee),
            counter: convertBigInt(rpcOperation.counter),
            gas_limit: convertBigInt(rpcOperation.gas_limit),
            storage_limit: convertBigInt(rpcOperation.storage_limit),
            balance: convertMutez(rpcOperation.balance),
            delegate: rpcOperation.delegate,
            script: rpcOperation.script,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        };
    }
}
