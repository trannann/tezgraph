import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { ProposalsNotification } from '../../entity/subscriptions/proposals-notification';
import { BaseOperationProperties } from './operation-converter';

type RpcProposals = rpc.OperationContentsProposals | rpc.OperationContentsAndResultProposals;

@singleton()
export class ProposalsConverter {
    convert(rpcOperation: RpcProposals, baseProperties: BaseOperationProperties): ProposalsNotification {
        return {
            ...baseProperties,
            kind: OperationKind.proposals,
            source: rpcOperation.source,
            period: rpcOperation.period,
            proposals: rpcOperation.proposals,
        };
    }
}
