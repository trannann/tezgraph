import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OriginationResult } from '../../../entity/subscriptions/origination-notification';
import { BalanceUpdateConverter } from '../common/balance-update-converter';
import { convertNullishBigInt } from '../common/big-int-converter';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class OriginationResultConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcResult: rpc.OperationResultOrigination): OriginationResult {
        return {
            ...getBaseOperationResultProperties(OriginationResult, rpcResult),
            balance_updates: this.balanceUpdateConverter.convertNullish(rpcResult.balance_updates),
            originated_contracts: rpcResult.originated_contracts,
            storage_size: convertNullishBigInt(rpcResult.storage_size),
            paid_storage_size_diff: convertNullishBigInt(rpcResult.paid_storage_size_diff),
        };
    }
}
