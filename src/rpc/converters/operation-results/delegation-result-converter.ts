import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationResult } from '../../../entity/subscriptions/delegation-notification';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class DelegationResultConverter {
    convert(rpcResult: rpc.OperationResultDelegation): DelegationResult {
        return getBaseOperationResultProperties(DelegationResult, rpcResult);
    }
}
