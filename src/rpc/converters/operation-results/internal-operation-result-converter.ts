import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    InternalOperationResult,
    InternalOperationResultKind,
    OperationResult,
} from '../../../entity/subscriptions/operation-result';
import { convertNullishMutez } from '../common/big-int-converter';
import { DelegationResultConverter } from './delegation-result-converter';
import { OriginationResultConverter } from './origination-result-converter';
import { RevealResultConverter } from './reveal-result-converter';
import { TransactionResultConverter } from './transaction-result-converter';

@singleton()
export class InternalOperationResultConverter {
    constructor(
        private readonly delegationResultConverter: DelegationResultConverter,
        private readonly originationResultConverter: OriginationResultConverter,
        private readonly revealResultConverter: RevealResultConverter,
        private readonly transactionResultConverter: TransactionResultConverter,
    ) {}

    convert(rpcResult: rpc.InternalOperationResult): InternalOperationResult {
        return {
            kind: InternalOperationResultKind[rpcResult.kind],
            source: rpcResult.source,
            nonce: rpcResult.nonce,
            amount: convertNullishMutez(rpcResult.amount),
            destination: rpcResult.destination,
            parameters: rpcResult.parameters,
            public_key: rpcResult.public_key,
            balance: convertNullishMutez(rpcResult.balance),
            delegate: rpcResult.delegate,
            script: rpcResult.script,
            result: this.convertResult(rpcResult),
        };
    }

    private convertResult(rpcResult: rpc.InternalOperationResult): OperationResult {
        switch (rpcResult.kind) {
            case rpc.OpKind.TRANSACTION:
                return this.transactionResultConverter.convert(rpcResult.result);
            case rpc.OpKind.REVEAL:
                return this.revealResultConverter.convert(rpcResult.result);
            case rpc.OpKind.DELEGATION:
                return this.delegationResultConverter.convert(rpcResult.result);
            case rpc.OpKind.ORIGINATION:
                return this.originationResultConverter.convert(rpcResult.result);
        }
    }
}
