import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { RevealResult } from '../../../entity/subscriptions/reveal-notification';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class RevealResultConverter {
    convert(rpcResult: rpc.OperationResultReveal): RevealResult {
        return getBaseOperationResultProperties(RevealResult, rpcResult);
    }
}
