import { ApolloError } from 'apollo-server-express';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { singleton } from 'tsyringe';
import * as typeGraphQL from 'type-graphql';

import { EnvConfig } from '../utils/configuration/env-config';
import { injectLogger, Logger } from '../utils/logging';
import { UuidGenerator } from '../utils/uuid-generator';
import { ResolverContext } from './resolver-context';

const userErrorTypes = [
    ApolloError,
    typeGraphQL.ArgumentValidationError,
    typeGraphQL.ForbiddenError,
    typeGraphQL.UnauthorizedError,
];

@singleton()
export class GraphQLErrorHandler {
    constructor(
        private readonly uuidGenerator: UuidGenerator,
        private readonly envConfig: EnvConfig,
        @injectLogger(GraphQLErrorHandler) private readonly logger: Logger,
    ) {}

    handle(error: GraphQLError, context: ResolverContext | undefined): GraphQLFormattedError {
        const requestId = context?.requestId;

        if (userErrorTypes.some(t => error.originalError instanceof t)) {
            this.logger.logWarning('User error occurred for {requestId}. Actual {error}', { error, requestId });
            return error;
        }

        const errorId = this.uuidGenerator.generate();
        const message = `An internal server error occured. Please provide error ID '${errorId}' if you contact our helpdesk.`;
        this.logger.logError('An internal server error with {errorId} occurred for {requestId}. Actual {error}', { errorId, error, requestId });

        if (this.envConfig.enableDebug) {
            return {
                ...error,
                message: `${message} (debug-only): requestId {requestId} and {error} ${error.message}`,
            };
        }

        return { message };
    }
}
