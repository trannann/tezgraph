import { InjectionToken } from 'tsyringe';
import { ClassType, MiddlewareInterface } from 'type-graphql';

import { ResolverContext } from './resolver-context';

export type Middleware = ClassType<MiddlewareInterface<ResolverContext>>;

export const graphQLMiddlewaresDIToken: InjectionToken<Middleware> = 'GraphQLMiddlewares';
