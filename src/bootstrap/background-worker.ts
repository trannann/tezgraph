import { InjectionToken } from 'tsyringe';

import { ValueOrPromise } from '../utils/reflection';

/** Arbitrary logic which is started and stopped together with the app. */
export interface BackgroundWorker {
    readonly name: string;
    start(): ValueOrPromise<void>;
    stop(): ValueOrPromise<void>;
}

export const backgroundWorkersDIToken: InjectionToken<BackgroundWorker> = 'BackgroundWorkers';
