/* istanbul ignore file */

import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from './scalars';
import { RelayEdge, RelayConnection } from "./relay";

@ObjectType()
export class Endorsement {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    batch_position!: number;

    @Field(() => String, { description: 'The kind of operation.' })
    kind!: string;

    @Field(() => scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @Field(() => Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @Field(() => scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @Field(() => Int)
    id!: number;

    @Field(() => scalars.Address, { nullable: true, description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.' })
    source?: string | null;

    @Field(() => scalars.Address, { description: 'An Implicit account to which an account has delegated their baking and endorsement rights.' })
    delegate!: string;

    @Field(() => String, { nullable: true })
    slots?: string | null;
}

@ObjectType()
export class EndorsementEdge extends RelayEdge(Endorsement) { }

@ObjectType()
export class EndorsementConnection extends RelayConnection(EndorsementEdge) { }

@ObjectType()
export class EndorsementSQLResults extends Endorsement {
    @Field(() => String)
    cursor!: string;
}