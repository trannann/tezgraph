/* istanbul ignore file */

import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from './scalars';
import { RelayEdge, RelayConnection } from "./relay";

@ObjectType()
export class Transaction {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    batch_position?: number;

    @Field(() => scalars.Address, { description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.' })
    source!: string;

    @Field(() => String, { description: 'The kind of operation.' })
    kind!: string;

    @Field(() => scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @Field(() => Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @Field(() => scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @Field(() => scalars.Mutez, { description: 'The cost of an operation in mutez.' })
    fee!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    counter!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    gas_limit!: bigint;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    storage_limit?: bigint | null;

    @Field(() => scalars.Mutez, { nullable: true, description: 'The amount of tz transfered.' })
    amount?: bigint | null;

    @Field(() => String, { nullable: true })
    parameters?: string | null;

    @Field(() => String, { nullable: true })
    entrypoint?: string | null;

    @Field(() => Int)
    id!: number;

    @Field(() => scalars.Address, { nullable: true, description: 'The address of an originated account or an implicit account.' })
    destination?: string | null;

    @Field(() => scalars.Address, { nullable: true, description: 'The address of an originated account or an implicit account.' })
    contract_address?: string | null;

    @Field(() => scalars.PositiveBigNumber, { description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas!: bigint;
}

@ObjectType()
export class TransactionEdge extends RelayEdge(Transaction) { }

@ObjectType()
export class TransactionConnection extends RelayConnection(TransactionEdge) { }

@ObjectType()
export class TransactionSQLResults extends Transaction {
    @Field(() => String)
    cursor!: string;
}