import { Field, Int, ObjectType } from 'type-graphql';

import { graphQLDescriptions } from './graphql-descriptions';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.seed_nonce_revelation),
})
export class SeedNonceRevelationNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.seed_nonce_revelation),
    })
    readonly kind = OperationKind.seed_nonce_revelation;

    @Field(() => Int)
    readonly level!: number;

    @Field(() => String)
    readonly nonce!: string;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | undefined;
}
