import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({ description: graphQLDescriptions.getRpc(['$block_header.alpha.full_header']) })
export class DoubleBakingEvidenceBlockHeader {
    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly proto!: number;

    @Field(() => scalars.BlockHash)
    readonly predecessor!: string;

    @Field(() => scalars.DateTime)
    readonly timestamp!: Date;

    @Field(() => Int)
    readonly validation_pass!: number;

    @Field(() => scalars.OperationHash, { nullable: true })
    readonly operations_hash: string | undefined;

    @Field(() => [String])
    readonly fitness!: string[];

    @Field(() => scalars.ContextHash)
    readonly context!: string;

    @Field(() => Int)
    readonly priority!: number;

    @Field(() => String, { nullable: true })
    readonly proof_of_work_nonce: string | undefined;

    @Field(() => scalars.NonceHash, { nullable: true })
    readonly seed_nonce_hash?: string | undefined;

    @Field(() => scalars.Signature)
    readonly signature!: string;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.double_baking_evidence),
})
export class DoubleBakingEvidenceNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.double_baking_evidence),
    })
    readonly kind = OperationKind.double_baking_evidence;

    @Field(() => DoubleBakingEvidenceBlockHeader)
    readonly bh1!: DoubleBakingEvidenceBlockHeader;

    @Field(() => DoubleBakingEvidenceBlockHeader)
    readonly bh2!: DoubleBakingEvidenceBlockHeader;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | undefined;
}
