import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { OperationKind, OperationNotification } from './operation-notification';

export enum BallotVote {
    nay = 'nay',
    pass = 'pass',
    yay = 'yay',
}

registerEnumType(BallotVote, {
    name: 'BallotVote',
    description: 'The ballot vote.',
});

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.ballot),
})
export class BallotNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.ballot),
    })
    readonly kind = OperationKind.ballot;

    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly period!: number;

    @Field(() => scalars.ProtocolHash)
    readonly proposal!: string;

    @Field(() => BallotVote)
    readonly ballot!: BallotVote;
}
