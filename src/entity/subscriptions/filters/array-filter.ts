import { Field, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { DefaultConstructor, isNullish, NonNullishValue, Nullish } from '../../../utils/reflection';
import { graphQLDescriptions } from '../graphql-descriptions';
import { Filter } from './filter';

export interface ArrayFilter<TItemScalar> extends Filter<readonly TItemScalar[]> {
    includes: Nullish<TItemScalar>;
    notIncludes: Nullish<TItemScalar>;
}

export function createArrayFilterClass<TItemScalar extends NonNullishValue>(
    itemScalarType: ReturnTypeFuncValue,
): DefaultConstructor<ArrayFilter<TItemScalar>> {
    @InputType({
        isAbstract: true,
        description: 'Filters for checking equality of any of the property values.',
    })
    class ArrayFilterClass implements Filter<readonly TItemScalar[]> {
        @Field(() => itemScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('include', { propertyPlural: true }),
        })
        includes: Nullish<TItemScalar>;

        @Field(() => itemScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('do NOT include', { propertyPlural: true }),
        })
        notIncludes: Nullish<TItemScalar>;

        passes(value: readonly TItemScalar[]): boolean {
            // Using isNullish() instead of ! operator b/c filters can be empty string/zero/false.
            return (isNullish(this.includes) || value.includes(this.includes))
                && (isNullish(this.notIncludes) || !value.includes(this.notIncludes));
        }
    }
    return ArrayFilterClass;
}
