import { Field, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { DefaultConstructor, isNullish, NonNullishValue, Nullish } from '../../../utils/reflection';
import { graphQLDescriptions } from '../graphql-descriptions';
import { Filter } from './filter';

export interface EqualityFilter<TScalar> extends Filter<TScalar> {
    equalTo: Nullish<TScalar>;
    notEqualTo: Nullish<TScalar>;
    in: Nullish<TScalar[]>;
    notIn: Nullish<TScalar[]>;
}

export function createEqualityFilterClass<TScalar extends NonNullishValue>(
    scalarType: ReturnTypeFuncValue,
): DefaultConstructor<EqualityFilter<TScalar>> {
    @InputType({ isAbstract: true })
    class EqualityFilterClass implements EqualityFilter<TScalar> {
        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('equals to'),
        })
        equalTo: Nullish<TScalar>;

        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('does NOT equal to'),
        })
        notEqualTo: Nullish<TScalar>;

        @Field(() => [scalarType], {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is contained in', { filterPlural: true }),
        })
        in: Nullish<TScalar[]>;

        @Field(() => [String], {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is NOT contained in', { filterPlural: true }),
        })
        notIn: Nullish<TScalar[]>;

        passes(value: TScalar): boolean {
            // Using isNullish() instead of ! operator b/c filters can be empty string/zero/false.
            return (isNullish(this.equalTo) || this.equalTo === value)
                && (isNullish(this.notEqualTo) || this.notEqualTo !== value)
                && (!this.in || this.in.includes(value))
                && (!this.notIn || !this.notIn.includes(value));
        }
    }
    return EqualityFilterClass;
}
