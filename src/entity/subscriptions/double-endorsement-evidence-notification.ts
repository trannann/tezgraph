import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

export enum InlinedEndorsementKind {
    endorsement = 'endorsement',
}

registerEnumType(InlinedEndorsementKind, {
    name: 'InlinedEndorsementKindEnum',
    description: 'Inlined endorsement kind.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.endorsement.contents']) })
export class InlinedEndorsementContents {
    @Field(() => InlinedEndorsementKind)
    readonly kind!: InlinedEndorsementKind;

    @Field(() => Int)
    readonly level!: number;
}

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.endorsement']) })
export class InlinedEndorsement {
    @Field(() => scalars.BlockHash)
    readonly branch!: string;

    @Field(() => InlinedEndorsementContents)
    readonly operations!: InlinedEndorsementContents;

    @Field(() => scalars.Signature, { nullable: true })
    readonly signature: string | undefined;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.double_endorsement_evidence),
})
export class DoubleEndorsementEvidenceNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.double_endorsement_evidence),
    })
    readonly kind = OperationKind.double_endorsement_evidence;

    @Field(() => InlinedEndorsement)
    readonly op1!: InlinedEndorsement;

    @Field(() => InlinedEndorsement)
    readonly op2!: InlinedEndorsement;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | undefined;
}
