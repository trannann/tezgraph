import { Field, ObjectType } from 'type-graphql';

import { BalanceUpdate } from './balance-update';
import { graphQLDescriptions } from './graphql-descriptions';
import { InternalOperationResult } from './operation-result';

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadata {
    @Field(() => [BalanceUpdate])
    readonly balance_updates!: readonly BalanceUpdate[];
}

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadataWithInternalResults extends AbstractOperationMetadata {
    @Field(() => [InternalOperationResult], { nullable: true })
    readonly internal_operation_results: readonly InternalOperationResult[] | undefined;
}

@ObjectType({
    description: `${graphQLDescriptions.getRpc([graphQLDescriptions.rpcOperation, 'metadata'], { trailingPeriod: false })}`
        + ' of simple operations where there are only `balance_updates`.',
})
export class SimpleOperationMetadata extends AbstractOperationMetadata {}
