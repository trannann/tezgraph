import { singleton } from 'tsyringe';

import { InfoProvider } from '../../../utils/iterable/interfaces';
import { BlockNotification } from '../block-notification';

/** Collects generic diagnostic info about a block. */
@singleton()
export class BlockInfoProvider implements InfoProvider<BlockNotification> {
    getInfo(block: BlockNotification): unknown {
        return {
            hash: block.hash,
            level: block.header.level,
            timestamp: block.header.timestamp,
            operationCount: block.operations.length,
        };
    }
}
