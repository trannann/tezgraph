import { singleton } from 'tsyringe';

import { InfoProvider } from '../../../utils/iterable/interfaces';
import { MempoolOperationGroup } from '../operation-notification';
import { getSignature } from './mempool-operation-group-identifier';

/** Collects generic diagnostic info about a mempool operation group. */
@singleton()
export class MempoolOperationGroupInfoProvider implements InfoProvider<MempoolOperationGroup> {
    getInfo(group: MempoolOperationGroup): unknown {
        return {
            signature: getSignature(group),
            operationCount: group.length,
        };
    }
}
