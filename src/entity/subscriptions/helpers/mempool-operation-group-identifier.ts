import { singleton } from 'tsyringe';

import { Identifier } from '../../../utils/iterable/interfaces';
import { isWhiteSpace } from '../../../utils/string-manipulation';
import { MempoolOperationGroup } from '../operation-notification';

export const numberOfLastItemsToCheck = 3000;

/** Collects generic diagnostic info about a mempool operation group. */
@singleton()
export class MempoolOperationGroupIdentifier implements Identifier<MempoolOperationGroup> {
    getId(group: MempoolOperationGroup): string {
        return getSignature(group);
    }
}

export function getSignature(group: MempoolOperationGroup): string {
    if (!group[0]) {
        throw new Error('Unexpectedly, there are no operations in the mempool operation group.');
    }

    const signature = group[0].info.signature;
    if (isWhiteSpace(signature)) {
        throw new Error('Unexpectedly, there is no signature on the first operation in the mempool operation group.');
    }
    return signature;
}
