import { singleton } from 'tsyringe';

import { Identifier } from '../../../utils/iterable/interfaces';

/** Minimal interface in order to support a regular block, a block header and a RPC block too. */
interface GenericBlock {
    readonly hash: string;
}

export const numberOfLastItemsToCheck = 100;

/** Gets hash as an ID of a block. */
@singleton()
export class BlockIdentifier implements Identifier<GenericBlock> {
    getId(block: GenericBlock): string {
        return block.hash;
    }
}
