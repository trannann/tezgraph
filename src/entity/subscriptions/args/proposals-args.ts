import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { AddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { NullableProtocolHashArrayFilter } from '../filters/hash-filters';
import { ProposalsNotification } from '../proposals-notification';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class ProposalsSpecificFilter implements Filter<ProposalsNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableProtocolHashArrayFilter, { nullable: true })
    readonly proposals: Nullish<NullableProtocolHashArrayFilter>;

    passes(operation: ProposalsNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.proposals || this.proposals.passes(operation.proposals));
    }
}

export const ProposalsFilter = createOperationFilterClass(ProposalsSpecificFilter);
export const ProposalsArgs = createOperationArgsClass(ProposalsFilter);
