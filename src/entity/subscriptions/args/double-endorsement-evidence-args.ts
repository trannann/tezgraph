import { Field, InputType } from 'type-graphql';

import { isNotNullish, Nullish } from '../../../utils/reflection';
import { DoubleEndorsementEvidenceNotification } from '../double-endorsement-evidence-notification';
import { NullableAddressArrayFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class DoubleEndorsementEvidenceSpecificFilter implements Filter<DoubleEndorsementEvidenceNotification> {
    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressArrayFilter>;

    passes(operation: DoubleEndorsementEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata?.balance_updates.map(u => u.delegate).filter(isNotNullish));
    }
}

export const DoubleEndorsementEvidenceFilter = createOperationFilterClass(DoubleEndorsementEvidenceSpecificFilter);
export const DoubleEndorsementEvidenceArgs = createOperationArgsClass(DoubleEndorsementEvidenceFilter);
