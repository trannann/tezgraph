import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { EndorsementNotification } from '../endorsement-notification';
import { NullableAddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class EndorsementSpecificFilter implements Filter<EndorsementNotification> {
    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    passes(operation: EndorsementNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata?.delegate);
    }
}

export const EndorsementFilter = createOperationFilterClass(EndorsementSpecificFilter);
export const EndorsementArgs = createOperationArgsClass(EndorsementFilter);
