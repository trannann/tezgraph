import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { MoneyOperationNotification, MoneyOperationResult } from './money-operation-notification';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { OperationResult } from './operation-result';
import { ScriptedContracts } from './scripted-contracts';

@ObjectType({
    implements: OperationResult,
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.origination),
})
export class OriginationResult extends MoneyOperationResult {}

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.origination) })
export class OriginationMetadata extends AbstractOperationMetadataWithInternalResults {
    @Field(() => OriginationResult)
    readonly operation_result!: OriginationResult;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.origination),
})
export class OriginationNotification extends MoneyOperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.origination),
    })
    readonly kind = OperationKind.origination;

    @Field(() => scalars.Mutez)
    readonly balance!: bigint;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @Field(() => ScriptedContracts, { nullable: true })
    readonly script: ScriptedContracts | undefined;

    @Field(() => OriginationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: OriginationMetadata | undefined;
}
