import { MichelsonV1Expression } from '@taquito/rpc';
import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';

@ObjectType({ description: graphQLDescriptions.getRpc(['$contract.big_map_diff']) })
export class BigMapDiff {
    @Field(() => String)
    readonly key_hash!: string;

    @Field(() => scalars.MichelsonJson)
    readonly key!: MichelsonV1Expression;

    @Field(() => scalars.MichelsonJson, { nullable: true })
    readonly value: MichelsonV1Expression | undefined;
}
