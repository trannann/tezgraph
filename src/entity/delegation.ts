/* istanbul ignore file */

import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from './scalars';
import { RelayEdge, RelayConnection } from "./relay";

@ObjectType()
export class Delegation {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    batch_position?: number;

    @Field(() => scalars.Address, { description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.' })
    source!: string;

    @Field(() => String, { description: 'The kind of operation.' })
    kind!: string;

    @Field(() => scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @Field(() => Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @Field(() => scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @Field(() => scalars.Mutez, { description: 'The cost of an operation in mutez.' })
    fee!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    counter!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    gas_limit!: bigint;

    @Field(() => Int)
    id!: number;

    @Field(() => scalars.Address, { description: 'An Implicit account to which an account has delegated their baking and endorsement rights.' })
    delegate!: string;
}

@ObjectType()
export class DelegationEdge extends RelayEdge(Delegation) { }

@ObjectType()
export class DelegationConnection extends RelayConnection(DelegationEdge) { }

@ObjectType()
export class DelegationSQLResults extends Delegation {
    @Field(() => String)
    cursor!: string;
}