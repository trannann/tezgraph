import { PrismaClient } from '@prisma/client';
import { singleton } from 'tsyringe';

import { BackgroundWorker } from '../../../bootstrap/background-worker';
import { injectLogger, Logger } from '../../../utils/logging';
import DatabaseHealthCheck from './database-health-check';

export const prisma = new PrismaClient();

@singleton()
export class PrismaDatabaseWorker implements BackgroundWorker {
    readonly name = 'PrismaDatabase';

    constructor(
        private readonly prismaClient: PrismaClient,
        private readonly databaseHealthCheck: DatabaseHealthCheck,
        @injectLogger(PrismaDatabaseWorker) private readonly logger: Logger,
    ) {}

    async start(): Promise<void> {
        try {
            this.databaseHealthCheck.setupMonitoring();

            /*
             * Retry mechanism blocked by Prisma
             * https://gitlab.com/tezgraph/tezgraph/-/issues/128#note_469904876
             */
            await this.prismaClient.$connect();
            this.logger.logInformation('Database connected.');
            this.databaseHealthCheck.setConnected();
        } catch (error: unknown) {
            this.logger.logWarning('Unable to connect to database due to {error}.', { error });
            this.databaseHealthCheck.setUnableToConnect();
        }
    }

    async stop(): Promise<void> {
        try {
            await this.prismaClient.$disconnect();
        } catch (error: unknown) {
            this.logger.logWarning('Unable to disconnect from database due to {error}.', { error });
        }
    }
}
