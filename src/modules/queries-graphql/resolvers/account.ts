import { Resolver, Query, Args, FieldResolver, Root, Ctx } from 'type-graphql';
import { performance } from 'perf_hooks';

import ConnectionUtils from '../repositories/connection.utils';
import { injectable } from 'tsyringe';
import { Account, AccountOperationsArgs, AccountArgs } from '../../../entity/account';
import AccountRepository from '../repositories/account-repository';
import { injectLogger, Logger } from '../../../utils/logging';
import { OriginationConnection } from '../../../entity/origination';
import { UserInputError } from 'apollo-server';
import { createOrderBy, createRelayPageData, queryResultsFound } from '../utils';
import { OrderByField, OrderByDirection } from '../repositories/order-by.utils';
import { RevealConnection } from '../../../entity/reveal';
import { OperationsConnection } from '../../../entity/operations';
import { TransactionConnection } from '../../../entity/transaction';
import { DelegationConnection } from '../../../entity/delegation';
import { EndorsementConnection } from '../../../entity/endorsement';
import { ResolverContext } from '../../../bootstrap/resolver-context';

@injectable()
@Resolver(() => Account)
export class AccountResolver {
    constructor(
        private readonly accountRepository: AccountRepository,
        private readonly connectionUtils: ConnectionUtils,
        @injectLogger(AccountResolver) private readonly logger: Logger,
    ) { }

    @Query(() => Account)
    async account(
        @Args() { address }: AccountArgs,
        @Ctx() { requestId }: ResolverContext,
    ) {
        this.logger.logInformation(`Account Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            const [activated, first_seen, public_key] = await Promise.all([
                this.accountRepository.getActivated(address, requestId),
                this.accountRepository.getFirstSeen(address, requestId),
                this.accountRepository.getRevealedPublicKey(address, requestId),
            ]);

            // Account Response Body
            const account: Account = {
                address,
                activated,
                first_seen,
                public_key,
            };

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Account Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });
            return account;
        } catch (err) {
            this.logger.logError(`Account Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => OriginationConnection)
    async originations(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs
    ) {
        this.logger.logInformation(`Originations Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(
                    `Argument Error - before/after: Cannot use both "before" and "after" arguments.`
                    + ` Please use "before" to paginate backwards and "after" to paginate forwards.`,
                );
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const relayPageData = createRelayPageData(first, last, before, after);

            const originations = await this.accountRepository.getOriginations(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData);
            queryResultsFound(originations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Originations Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('origination', originations, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Originations Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => RevealConnection)
    async reveals(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs) {
        this.logger.logInformation(`Reveals Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(
                    `Argument Error - before/after: Cannot use both "before" and "after" arguments.`
                    + ` Please use "before" to paginate backwards and "after" to paginate forwards.`,
                );
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const relayPageData = createRelayPageData(first, last, before, after);

            const reveals = await this.accountRepository.getReveals(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData)
            queryResultsFound(reveals);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Reveals Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('reveal', reveals, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Reveals Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => OperationsConnection)
    async operations(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs) {
        this.logger.logInformation(`Operations Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }
            const relayPageData = createRelayPageData(first, last, before, after);

            const operations = await this.accountRepository.getOperations(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData)
            queryResultsFound(operations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Operations Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('operations', operations, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Operations Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => TransactionConnection)
    async transactions(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs) {
        this.logger.logInformation(`Transactions Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const relayPageData = createRelayPageData(first, last, before, after);

            const transactions = await this.accountRepository.getTransactions(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData);
            queryResultsFound(transactions);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Transactions Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('transaction', transactions, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Transactions Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => DelegationConnection)
    async delegations(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs) {
        this.logger.logInformation(`Delegations Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const relayPageData = createRelayPageData(first, last, before, after);

            const delegations = await this.accountRepository.getDelegations(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData);
            queryResultsFound(delegations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Delegations Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('delegation', delegations, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Delegations Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }

    @FieldResolver(() => EndorsementConnection)
    async endorsements(
        @Root() { address }: Account,
        @Ctx() { requestId }: ResolverContext,
        @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs) {
        this.logger.logInformation(`Endorsements Query started - { requestID: ${requestId} }`, { status: 'start', requestID: requestId });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const relayPageData = createRelayPageData(first, last, before, after);

            const endorsements = await this.accountRepository.getEndorsements(requestId, address, first, last, before, after, dateRange, orderBy, relayPageData);
            queryResultsFound(endorsements);
            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`
            this.logger.logInformation(`Endorsements Query ended - {duration: ${duration}, requestID: ${requestId}}`, { status: 'end', duration, requestID: requestId });

            return await this.connectionUtils.getConnection('endorsement', endorsements, relayPageData, address, requestId, orderBy, dateRange);
        } catch (err) {
            this.logger.logError(`Endorsements Query failed - {error: '${err}', requestID: ${requestId}}`, { status: 'error', error: err, requestID: requestId })
            return err
        }
    }
}
