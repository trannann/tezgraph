/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable max-lines */
/* eslint-disable max-params */
/* eslint-disable max-lines-per-function */
/* eslint-disable @typescript-eslint/init-declarations */
/* eslint-disable capitalized-comments */
import { PrismaClient } from '@prisma/client';
import { performance } from 'perf_hooks';
import { singleton } from 'tsyringe';

import { DelegationSQLResults } from '../../../entity/delegation';
import { EndorsementSQLResults } from '../../../entity/endorsement';
import { OperationsSQLResults } from '../../../entity/operations';
import { OriginationSQLResults } from '../../../entity/origination';
import { RevealSQLResults } from '../../../entity/reveal';
import { TransactionSQLResults } from '../../../entity/transaction';
import { injectLogger, Logger } from '../../../utils/logging';
import { RelayPageData } from '../utils';
import CursorUtils from './cursor.utils';
import { createDateRangeClause, DateRange, dateRangeValid, getDateRangeClause } from './date-range.utils';
import { setTakeLimit } from './limit.utils';
import { applyRelayOrder, createBeforeOrderBy, createOrderByClause, OrderBy } from './order-by.utils';

@singleton()
export default class AccountRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly cursorUtils: CursorUtils,
        @injectLogger(AccountRepository) private readonly logger: Logger,
    ) { }

    // First Seen Timestamp Query
    async getFirstSeen(pkh: string, requestID: string): Promise<Date | undefined> {
        this.logger.logInformation(`GetFirstSeen started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        const firstTransactionQuery = /* sql */ `
            SELECT timestamp 
            FROM get_transaction('${pkh}') 
            ORDER BY timestamp 
            LIMIT 1;
        `;
        const firstTransaction = await this.prisma.$queryRaw<{ timestamp: number }[]>(firstTransactionQuery);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetFirstSeen ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });

        if (!firstTransaction[0]) {
            return undefined;
        }

        return new Date(firstTransaction[0].timestamp);
    }

    // Activated Timestamp Query
    async getActivated(pkh: string, requestID: string): Promise<Date | undefined> {
        this.logger.logInformation(`GetActivated started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        const activatedQuery = /* sql */ `
            SELECT timestamp
            FROM block
            WHERE hash = (
                SELECT block_hash
                FROM operation
                WHERE hash = (
                    SELECT activated
                    FROM implicit
                    WHERE pkh = '${pkh}'
                )
            );
        `;
        const activated = await this.prisma.$queryRaw<{ timestamp: number }[]>(activatedQuery);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetActivated ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });

        if (!activated[0]) {
            return undefined;
        }

        return new Date(activated[0].timestamp);
    }

    // Activated Timestamp Query
    async getRevealedPublicKey(pkh: string, requestID: string): Promise<string | undefined> {
        this.logger.logInformation(`GetRevealedPublicKey started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        const revealedQuery = /* sql */ `
            SELECT public_key 
            FROM get_reveal('${pkh}')
            ORDER BY id DESC
            LIMIT 1;
        `;
        const revealed = await this.prisma.$queryRaw<{ public_key: string }[]>(revealedQuery);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        // eslint-disable-next-line max-len
        this.logger.logInformation(`GetRevealedPublicKey ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });

        if (!revealed[0]) {
            return undefined;
        }

        return revealed[0].public_key;
    }

    async getOriginations(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<OriginationSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);

        let originationsQuery: string;
        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            originationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'origination',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            originationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'origination',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            originationsQuery = /* sql */ `
                SELECT id, hash, op_id as batch_position, source, destination, delegate, 
                    kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, 
                    amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor 
                FROM get_origination('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }

        originationsQuery = applyRelayOrder(first, last, before, after, orderBy, originationsQuery);

        const originations = await this.prisma.$queryRaw<OriginationSQLResults[]>(originationsQuery);

        return originations.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getReveals(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<RevealSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);
        let revealsQuery: string;

        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            revealsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'reveal',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            revealsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'reveal',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            revealsQuery = /* sql */ `
                SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, 
                    storage_limit, public_key, amount, parameters, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor 
                FROM get_reveal('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }

        revealsQuery = applyRelayOrder(first, last, before, after, orderBy, revealsQuery);

        const reveals = await this.prisma.$queryRaw<RevealSQLResults[]>(revealsQuery);

        return reveals.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getOperations(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<OperationsSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);
        let operationsQuery: string;

        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            operationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'operations',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            operationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'operations',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            operationsQuery = /* sql */ ` 
                SELECT id, hash, op_id as batch_position, source, destination, delegate, slots, kind, timestamp, level, block, counter, gas_limit,
                    storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, 
                    CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_operations('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }

        operationsQuery = applyRelayOrder(first, last, before, after, orderBy, operationsQuery);

        const operations = await this.prisma.$queryRaw<OperationsSQLResults[]>(operationsQuery);

        return operations.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getTransactions(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<TransactionSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);
        let transactionsQuery: string;

        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            transactionsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'transaction',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            transactionsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'transaction',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            transactionsQuery = /* sql */ `
                SELECT id, hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, storage_limit, entrypoint, 
                    fee, amount, parameters, timestamp, contract_address, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor 
                FROM get_transaction('${accountPKH}') 
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }

        transactionsQuery = applyRelayOrder(first, last, before, after, orderBy, transactionsQuery);

        const transactions = await this.prisma.$queryRaw<TransactionSQLResults[]>(transactionsQuery);

        return transactions.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getDelegations(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<DelegationSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        let dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);
        let delegationsQuery: string;

        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            delegationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'delegation',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            delegationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'delegation',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            if (dateRange) {
                dateRangeValid(dateRange);
                dateRangeClause = getDateRangeClause(dateRange);
            }

            delegationsQuery = /* sql */ `
                SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, kind, timestamp, level, block, counter, gas_limit, 
                    storage_limit, public_key, amount, parameters, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor
                FROM get_delegation('${accountPKH}') 
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }

        delegationsQuery = applyRelayOrder(first, last, before, after, orderBy, delegationsQuery);
        const delegations = await this.prisma.$queryRaw<DelegationSQLResults[]>(delegationsQuery);

        return delegations.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getEndorsements(
        requestID: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<EndorsementSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        let dateRangeClause = createDateRangeClause(dateRange);
        const orderByClause = createOrderByClause(first, last, orderBy);
        let endorsementsQuery: string;

        if (before) {
            const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
            endorsementsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'endorsement',
                before,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                beforeOrderBy,
                dateRange,
            );
        } else if (after) {
            endorsementsQuery = await this.cursorUtils.cursorPaginationQueryBuilder(
                'endorsement',
                after,
                relayPageData,
                accountPKH,
                takeLimit,
                requestID,
                orderBy,
                dateRange,
            );
        } else {
            if (dateRange) {
                dateRangeValid(dateRange);
                dateRangeClause = getDateRangeClause(dateRange);
            }

            endorsementsQuery = /* sql */ `
                SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, slots, kind, timestamp, level, block, counter, gas_limit, 
                    storage_limit, public_key, amount, parameters, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor
                FROM get_endorsement('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
                LIMIT ${takeLimit}
            `;
        }
        endorsementsQuery = applyRelayOrder(first, last, before, after, orderBy, endorsementsQuery);

        const endorsements = await this.prisma.$queryRaw<EndorsementSQLResults[]>(endorsementsQuery);

        return endorsements.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }
}
