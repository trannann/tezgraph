/* eslint-disable @typescript-eslint/init-declarations */
/* eslint-disable max-len */
/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { PrismaClient } from '@prisma/client';
import { UserInputError } from 'apollo-server-express';
import { performance } from 'perf_hooks';
import { singleton } from 'tsyringe';
import { ClassType } from 'type-graphql/dist/interfaces/ClassType';

import { DelegationConnection, DelegationEdge } from '../../../entity/delegation';
import { EndorsementConnection, EndorsementEdge } from '../../../entity/endorsement';
import { OperationsConnection, OperationsEdge } from '../../../entity/operations';
import { OriginationConnection, OriginationEdge } from '../../../entity/origination';
import { PageInfo } from '../../../entity/pageInfo';
import { RevealConnection, RevealEdge } from '../../../entity/reveal';
import { TransactionConnection, TransactionEdge } from '../../../entity/transaction';
import { errorToString } from '../../../utils/conversion';
import { injectLogger, Logger } from '../../../utils/logging';
import { RelayPageData, createOrderBy, createRelayPageData } from '../utils';
import CursorUtils from './cursor.utils';
import { DateRange } from './date-range.utils';
import { OrderBy, OrderByField, OrderByDirection } from './order-by.utils';

@singleton()
export default class ConnectionUtils {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly cursorUtils: CursorUtils,
        @injectLogger(ConnectionUtils) private readonly logger: Logger,
    ) { }

    async getConnection(
        operation: string,
        results: { cursor: string }[],
        relayPageData: RelayPageData,
        pkh: string,
        requestID: string,
        orderBy?: OrderBy | undefined,
        dateRange?: DateRange | undefined,
    ): Promise<ClassType<TransactionConnection | DelegationConnection | OperationsConnection | OriginationConnection | RevealConnection | EndorsementConnection>> {
        this.logger.logInformation(`GetConnection started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        const startCursor = results[0]?.cursor ?? '';
        const endCursor = results[results.length - 1]?.cursor ?? '';
        if (!this.cursorUtils.isCursorValid(startCursor) || !this.cursorUtils.isCursorValid(endCursor)) {
            const error = `The start cursor (${startCursor}) and/or end cursor(${endCursor}) retrieved from the Account Query results in getConnection() are not in a valid cursor format.`;
            this.logger.logError(`GetConnection failed - {error: '${error}', requestID: ${requestID}}`, { status: 'error', error, requestID });
            throw new Error(error);
        }

        const hasNextPage = await this.getPageInfoBool(operation, endCursor, pkh, requestID, relayPageData, 'next', orderBy, dateRange);
        const hasPrevPage = await this.getPageInfoBool(operation, startCursor, pkh, requestID, relayPageData, 'prev', orderBy, dateRange);
        const operationEdges = this.edgesBuilder(results, operation) as TransactionEdge[] | DelegationEdge[] | OperationsEdge[] | OriginationEdge[] | RevealEdge[] | EndorsementEdge[];
        const pageInfo = this.pageInfoBuilder(startCursor, endCursor, hasNextPage, hasPrevPage);
        const connection = this.connectionBuilder(operation, operationEdges, pageInfo);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetConnection ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });

        return connection;
    }

    // eslint-disable-next-line max-params
    async getPageInfoBool(
        operation: string,
        cursor: string,
        pkh: string,
        requestID: string,
        relayPageData: RelayPageData,
        direction: string,
        orderBy?: OrderBy,
        dateRange?: DateRange,
    ): Promise<boolean> {
        const orderByObject: OrderBy = orderBy === undefined ? createOrderBy(OrderByField.ID, OrderByDirection.DESC) : orderBy;
        const results = await this.getPageInfoResults(operation, cursor, pkh, requestID, relayPageData, direction, orderByObject, dateRange);
        if (results.length !== 0) {
            return true;
        }
        return false;
    }

    // eslint-disable-next-line max-params
    async getPageInfoResults(
        operation: string,
        cursor: string,
        pkh: string,
        requestID: string,
        relayPageData: RelayPageData,
        direction: string,
        orderBy: OrderBy,
        dateRange?: DateRange | undefined,
    ): Promise<Record<string, unknown>[]> {
        this.logger.logInformation(`GetPageInfoResults started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        try {
            const pageInfoQuery = await this.getPageInfoQuery(operation, cursor, pkh, requestID, relayPageData, direction, orderBy, dateRange);
            const results = await this.executeQuery(pageInfoQuery, requestID);
            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`GetPageInfoResults ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });
            return results;
        } catch (err: unknown) {
            const errString = errorToString(err);
            this.logger.logError(`GetPageInfoResults failed - {error: '${errString}', requestID: ${requestID}}`, { status: 'error', error: err, requestID });
            throw new Error(errString);
        }
    }

    // eslint-disable-next-line max-params
    async getPageInfoQuery(
        operation: string,
        cursor: string,
        pkh: string,
        requestID: string,
        relayPageData: RelayPageData,
        direction: string,
        orderBy: OrderBy,
        dateRange?: DateRange | undefined,
    ): Promise<string> {
        const before: string | undefined = relayPageData.order === 'before' ? cursor : undefined;
        const after: string | undefined = relayPageData.order === ' after' ? cursor : undefined;
        const pageInfoRelayPageData = createRelayPageData(undefined, undefined, before, after);
        pageInfoRelayPageData.order = direction === 'next' ? 'after' : 'before';

        const relayPageAfterBool = relayPageData.order === 'after';

        const pageInfoQuery = await this.pageInfoNeighbourQueryBuilder(
            operation,
            cursor,
            pageInfoRelayPageData,
            pkh,
            requestID,
            relayPageAfterBool,
            orderBy,
            dateRange,
        );

        return pageInfoQuery;
    }

    edgesBuilder(operations: Record<string, unknown>[], operation: string): unknown[] {
        const operationEdgeType = this.getEdgeClass(operation);
        const operationEdgeArray: unknown[] = [];
        operations.forEach((op) => {
            const operationEdge = new operationEdgeType();
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            Object.assign(operationEdge, { cursor: `${op.hash}:${op.batch_position}`, node: op });
            operationEdgeArray.push(operationEdge);
        });
        return operationEdgeArray;
    }

    getEdgeClass(operation: string): ClassType<TransactionEdge | DelegationEdge | OperationsEdge | OriginationEdge | RevealEdge | EndorsementEdge> {
        switch (operation) {
            case 'transaction':
                return TransactionEdge;
            case 'delegation':
                return DelegationEdge;
            case 'operations':
                return OperationsEdge;
            case 'origination':
                return OriginationEdge;
            case 'reveal':
                return RevealEdge;
            case 'endorsement':
                return EndorsementEdge;
            default:
                throw new UserInputError('Operation Error: Operation not recognized.');
        }
    }

    pageInfoBuilder(
        startCursor: string,
        endCursor: string,
        hasNextPage: boolean,
        hasPrevPage: boolean,
    ): PageInfo {
        const pageInfo: PageInfo = {
            has_next_page: hasNextPage,
            has_previous_page: hasPrevPage,
            start_cursor: startCursor,
            end_cursor: endCursor,
        };
        return pageInfo;
    }

    connectionBuilder(
        operation: string,
        operationEdges: TransactionEdge[] | DelegationEdge[] | OperationsEdge[] | OriginationEdge[] | RevealEdge[] | EndorsementEdge[],
        pageInfo: PageInfo,
    ): ClassType<TransactionConnection | DelegationConnection | OperationsConnection | OriginationConnection | RevealConnection | EndorsementConnection> {
        const connectionOperationClass: ClassType<TransactionConnection | DelegationConnection | OperationsConnection | OriginationConnection | RevealConnection | EndorsementConnection> = this.getConnectionOperationClass(operation);
        const operationConnection = new connectionOperationClass();
        Object.assign(operationConnection, { edges: operationEdges, page_info: pageInfo });
        return operationConnection as unknown as ClassType<TransactionConnection | DelegationConnection | OperationsConnection | OriginationConnection | RevealConnection | EndorsementConnection>;
    }

    getConnectionOperationClass(operation: string): ClassType<TransactionConnection | DelegationConnection | OperationsConnection | OriginationConnection | RevealConnection | EndorsementConnection> {
        switch (operation) {
            case 'transaction': {
                return TransactionConnection;
            }
            case 'delegation': {
                return DelegationConnection;
            }
            case 'operations': {
                return OperationsConnection;
            }
            case 'origination': {
                return OriginationConnection;
            }
            case 'reveal': {
                return RevealConnection;
            }
            case 'endorsement': {
                return EndorsementConnection;
            }
            default:
                throw new UserInputError('Operation Error: Operation not recognized.');
        }
    }

    /* The parameter relayAfter is a boolean that tells us whether the original relayPageData.order was 'after' or not.*/
    /* The parameter cursor in this function refers to the start_cursor/end_cursor from PageInfo and is required to determine whether there is has_next_page/has_previous_page is true or not.*/
    // eslint-disable-next-line max-params
    async pageInfoNeighbourQueryBuilder(operation: string, cursor: string, relayPageData: RelayPageData, pkh: string, requestID: string, relayAfter: boolean, orderBy?: OrderBy | undefined, dateRange?: DateRange | undefined): Promise<string> {
        const cursorRecord = await this.cursorUtils.getCursorRecord(operation, pkh, cursor, requestID);
        const orderByClause = this.getOrderByClause(relayPageData, orderBy);
        const orderByWhereClause = this.getOrderByWhereClause(cursorRecord, relayPageData, relayAfter, orderBy);
        const selectStatement = `SELECT id, hash`;
        const query = this.cursorUtils.getCursorQuery(operation, pkh, orderByClause, selectStatement, dateRange, orderByWhereClause);
        return query;
    }

    getOrderByClause(relayPageData: RelayPageData, orderBy?: OrderBy): string {
        let orderByClause = `ORDER BY id DESC, hash ASC, op_id ASC`;

        if (orderBy) {
            let orderByDirection = orderBy.direction;

            if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'DESC') {
                orderByDirection = OrderByDirection.ASC;
            }
            orderByClause = `ORDER BY ${orderBy.field} ${orderByDirection}, hash ASC, op_id ASC`;
        }

        return orderByClause;
    }

    // eslint-disable-next-line complexity
    getOrderByWhereClause(cursorRecord: Record<string, any>[], relayPageData: RelayPageData, relayAfter: boolean, orderBy?: OrderBy | undefined): string {
        let orderByWhereClause = '';
        let direction = '<';

        if (orderBy) {
            const orderByDirection = orderBy.direction;
            direction = orderByDirection === 'DESC' ? '<' : '>';

            if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'DESC') {
                direction = '>';
            } else if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'ASC') {
                direction = '<';
            } else if (relayPageData.direction === 'last' && relayPageData.order === 'before' && orderBy.direction === 'ASC') {
                direction = '<';
            } else if (relayPageData.direction === undefined && relayPageData.order === 'before' && orderBy.direction === 'ASC') {
                direction = '<';
            } else if (relayPageData.direction === undefined && relayPageData.order === 'before' && orderBy.direction === 'DESC') {
                direction = '>';
            } else if (relayPageData.direction === 'last' && relayPageData.order === 'before' && orderBy.direction === 'DESC' && relayAfter) {
                direction = '>';
            } else if (relayPageData.direction === 'last' && relayPageData.order === 'before' && orderBy.direction === 'DESC' && !relayAfter) {
                direction = '>';
            }

            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            orderByWhereClause = `WHERE (((${orderBy.field} ${direction}= '${cursorRecord[0]?.[orderBy.field]}') AND (hash >= '${cursorRecord[0]?.hash}' AND op_id > '${cursorRecord[0]?.op_id}')) OR ((${orderBy.field} ${direction}= '${cursorRecord[0]?.[orderBy.field]}') AND (hash != '${cursorRecord[0]?.hash}')))`;
        }

        return orderByWhereClause;
    }

    async executeQuery(query: string, requestID: string): Promise<Record<string, unknown>[]> {
        try {
            const results: Record<string, unknown>[] = await this.prisma.$queryRaw(query);
            return results;
        } catch (err: unknown) {
            const errString = errorToString(err);
            this.logger.logError(`Execute Query failed - {error: '${errString}', requestID: ${requestID}}`, { status: 'error', error: err, requestID });
            throw new Error(errString);
        }
    }
}
