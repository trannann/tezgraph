/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable lines-around-comment */
/* eslint-disable @typescript-eslint/init-declarations */
/* eslint-disable no-param-reassign */
/* eslint-disable func-style */

import { UserInputError } from 'apollo-server';
import { format } from 'fecha';
import { InputType, Field } from 'type-graphql';

export const createDateRangeClause = function (dateRange: DateRange | undefined): string {
    if (dateRange) {
        dateRange = dateRangeFormatter(dateRange);
        dateRangeValid(dateRange);
        return getDateRangeClause(dateRange);
    }

    return '';
};

// Date Range Query
export const getDateRangeClause = function (dateRange: DateRange): string {
    let dateRangeClause: string;

    if (dateRange.lte && dateRange.gte) {
        dateRangeClause = `WHERE (timestamp >= '${dateRange.gte}' AND timestamp <= '${dateRange.lte}')`;
    } else if (dateRange.lte) {
        dateRangeClause = `WHERE (timestamp <= '${dateRange.lte}')`;
    } else if (dateRange.gte) {
        dateRangeClause = `WHERE (timestamp >= '${dateRange.gte}')`;
    } else {
        throw new Error('Argument "date" of type "dateArg?" must be in a valid format.');
    }

    return dateRangeClause;
};

// DateRange Argument Validator
export const dateRangeValid = function (dateObj: DateRange): boolean {
    if (dateObj.lte && dateObj.gte) {
        if (!(new Date(dateObj.gte) < new Date(dateObj.lte))) {
            throw new UserInputError('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
        }
    }

    return true;
};

export const dateRangeFormatter = function (dateRange: DateRange): DateRange {
    try {
        if (dateRange.gte) {
            dateRange.gte = dateRangeTimestampCleanUp(dateRange.gte);
            let isoDateTime = format(new Date(dateRange.gte), 'isoDateTime');
            isoDateTime = isoDateTime.substring(0, 19);
            dateRange.gte = isoDateTime;
        }

        if (dateRange.lte) {
            dateRange.lte = dateRangeTimestampCleanUp(dateRange.lte);
            let isoDateTime = format(new Date(dateRange.lte), 'isoDateTime');
            isoDateTime = isoDateTime.substring(0, 19);
            const dateEG = isoDateTime;
            dateRange.lte = dateEG;
        }
        return dateRange;
    } catch (err: unknown) {
        throw new UserInputError(`Argument Error - date_range:  ${err}`);
    }
};

const dateRangeTimestampCleanUp = function (timestamp: string): string {
    /*
     * This is used to handle the case in which the user only enters the year.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 01/01 of the year the user entered.
     */
    if (timestamp.length === 7 || timestamp.length === 4) {
        timestamp += '/01';
    }

    /*
     * This is used to handle the case in which the user only enters a date without time.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 00:00:00 of the date the user entered.
     */
    if (timestamp.length < 11) {
        timestamp = format(new Date(timestamp), 'shortDate');
    }

    switch (true) {
        case timestamp.includes('PM'):
            timestamp = timestamp.replace('PM', ' PM');
            break;
        case timestamp.includes('pm'):
            timestamp = timestamp.replace('pm', ' pm');
            break;
        case timestamp.includes('AM'):
            timestamp = timestamp.replace('AM', ' AM');
            break;
        case timestamp.includes('am'):
            timestamp = timestamp.replace('am', ' am');
            break;
    }

    switch (true) {
        case timestamp.includes('Z'):
            timestamp = timestamp.replace('Z', '');
            break;
    }
    return timestamp;
};

// DateRange Argument Fields
@InputType()
export class DateRange {
    @Field({ nullable: true })
    gte?: string;

    @Field({ nullable: true })
    lte?: string;
}
