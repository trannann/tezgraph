/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { PrismaClient } from '@prisma/client';
import { UserInputError } from 'apollo-server';
import { performance } from 'perf_hooks';
import { singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../utils/logging';
import { RelayPageData } from '../utils';
import { DateRange, getDateRangeClause } from './date-range.utils';
import { OrderBy, OrderByDirection } from './order-by.utils';

@singleton()
export default class CursorUtils {
    constructor(
        private readonly prisma: PrismaClient,
        @injectLogger(CursorUtils) private readonly logger: Logger,
    ) { }

    isCursorValid(cursor: string): boolean {
        const cursorSplit = cursor.split(':');
        if (!cursorSplit[0] || (!cursorSplit[0].startsWith('oo') && !cursorSplit[0].startsWith('op') && !cursorSplit[0].startsWith('on'))) {
            return false;
        }
        if (cursorSplit[0].length !== 51 || isNaN(parseInt(cursorSplit[1] ?? '', 10))) {
            return false;
        }
        return true;
    }

    /*
     * This function is called by the Account Resolver.
     * When a user uses the before/after argument, it returns a SQL query that paginaates the records according to the user's request.
     */
    // eslint-disable-next-line max-params
    async cursorPaginationQueryBuilder(
        operation: string,
        cursor: string,
        relayPageData: RelayPageData,
        pkh: string,
        takeLimit: number,
        requestID: string,
        orderBy: OrderBy,
        dateRange?: DateRange,
    ): Promise<string> {
        this.logger.logInformation(`CursorPaginationQueryBuilder started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        if (!this.isCursorValid(cursor)) {
            throw new UserInputError(
                'Argument "cursor" of type "string?" is in a invalid format. The valid cursor format is "operation_hash:operation_id".',
            );
        }
        const cursorRecord = await this.getCursorRecord(operation, pkh, cursor, requestID);
        const relayDirection = this.getRelayDirection(orderBy, relayPageData);
        const orderByDirection = this.getOrderByDirection(orderBy, relayPageData);
        const orderByClause = `ORDER BY ${orderBy.field} ${orderByDirection}, hash ASC, op_id ASC`;
        const orderByWhereClause
            = `WHERE (((${orderBy.field} ${relayDirection}= '${cursorRecord[0]?.[orderBy.field]}') 
            AND (hash >= '${cursorRecord[0]?.hash}' AND op_id > '${cursorRecord[0]?.op_id}')) 
            OR ((${orderBy.field} ${relayDirection}= '${cursorRecord[0]?.[orderBy.field]}') 
            AND (hash != '${cursorRecord[0]?.hash}')))`;
        const selectStatement = this.getCursorSelectStatement(operation);
        const query = this.getCursorQuery(operation, pkh, orderByClause, selectStatement, dateRange, orderByWhereClause, takeLimit);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        // eslint-disable-next-line max-len
        this.logger.logInformation(`CursorPaginationQueryBuilder ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });
        return query;
    }

    async getCursorRecord(operation: string, pkh: string, cursor: string, requestID: string): Promise<Record<string, unknown>[]> {
        this.logger.logInformation(`GetCursorRecord started - { requestID: ${requestID} }`, { status: 'start', requestID });
        const performanceStart = performance.now();
        const cursorSplit = cursor.split(':');
        const opHash = cursorSplit[0];
        const opID = cursorSplit[1];
        const cursorRecordQuery
            = `SELECT * FROM get_${operation}('${pkh}') WHERE hash = '${opHash}' and op_id = '${opID}' limit 1`;
        const cursorRecord = await this.prisma.$queryRaw<Record<string, unknown>[]>(cursorRecordQuery);
        if (cursorRecord.length === 0) {
            throw new UserInputError('Unknown Record: No results found for the provided cursor.');
        }
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        // eslint-disable-next-line max-len
        this.logger.logInformation(`GetCursorRecord ended - {duration: ${duration}, requestID: ${requestID}}`, { status: 'end', duration, requestID });
        return cursorRecord;
    }

    getRelayDirection(orderBy: OrderBy, relayPageData: RelayPageData): string {
        let relayDirection = orderBy.direction === 'DESC' ? '<' : '>';

        if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'DESC') {
            relayDirection = '>';
        } else if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'ASC') {
            relayDirection = '<';
        }

        return relayDirection;
    }

    getOrderByDirection(orderBy: OrderBy, relayPageData: RelayPageData): string {
        let orderByDirection = orderBy.direction;
        if (relayPageData.direction === 'last' && relayPageData.order === 'after' && orderBy.direction === 'DESC') {
            orderByDirection = OrderByDirection.ASC;
        } else if (relayPageData.direction === 'last' && relayPageData.order === 'after' && orderBy.direction === 'ASC') {
            orderByDirection = OrderByDirection.DESC;
        }

        return orderByDirection;
    }

    getCursorSelectStatement(operation: string): string {
        // eslint-disable-next-line no-warning-comments
        // TODO: split and move sqls to the repository methods
        switch (operation) {
            case 'transaction':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, storage_limit, entrypoint, fee, amount, parameters, timestamp, contract_address, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor `;
            case 'delegation':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'operations':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, slots, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee,  consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'reveal':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'origination':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'endorsement':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, slots, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, slots, contract_address, fee, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            default:
                throw new UserInputError('Operation Error: Cursor query operation not recognized.');
        }
    }

    // eslint-disable-next-line max-lines-per-function,  max-statements
    getCursorQuery(
        operation: string,
        pkh: string,
        orderByClause: string,
        selectStatement: string,
        dateRange?: DateRange | undefined,
        orderByWhereClause?: string | undefined,
        takeLimit?: number | undefined,
    ): string {
        // eslint-disable-next-line @typescript-eslint/init-declarations
        let whereClause: string | undefined;
        // eslint-disable-next-line @typescript-eslint/init-declarations
        let dateRangeWhereClause: string | undefined;

        if (dateRange) {
            dateRangeWhereClause = getDateRangeClause(dateRange);
        }

        if (dateRangeWhereClause && orderByWhereClause) {
            whereClause = `${dateRangeWhereClause} AND ${orderByWhereClause.slice(6)}`;
        } else {
            whereClause = dateRangeWhereClause ?? orderByWhereClause;
        }

        if (whereClause === undefined) {
            whereClause = '';
        }

        return `${selectStatement} 
        FROM get_${operation}('${pkh}') ${whereClause} ${orderByClause} 
        LIMIT ${takeLimit !== undefined ? takeLimit : '1'}`;
    }
}
