/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-unnecessary-condition */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
/* eslint-disable func-style */
/* eslint-disable no-param-reassign */
/* eslint-disable complexity */
/* eslint-disable multiline-comment-style */
/* eslint-disable capitalized-comments */

import { UserInputError } from 'apollo-server';
import { Field, InputType, registerEnumType } from 'type-graphql';

import { createOrderBy } from '../utils';

// OrderBy Direction Enums
export enum OrderByDirection {
    ASC = 'ASC',
    DESC = 'DESC',
}

registerEnumType(OrderByDirection, {
    name: 'OrderByDirection',
    description: 'The available options for the order_by.direction field value.',
});

// OrderBy Field Enums
export enum OrderByField {
    TIMESTAMP = 'timestamp',
    // KIND = "kind", - only for account.operations
    HASH = 'hash',
    LEVEL = 'level',
    BLOCK = 'block',
    ID = 'id',
}

registerEnumType(OrderByField, {
    name: 'OrderByField',
    description: 'The available options for the order_by.field field value.',
});

@InputType()
export class OrderBy {
    @Field(() => OrderByField, { nullable: true })
    field!: OrderByField;

    @Field(() => OrderByDirection, { nullable: true })
    direction!: OrderByDirection;
}

export const createBeforeOrderBy = function (_first: number | undefined, last: number | undefined, before: string, orderBy: OrderBy): OrderBy {
    if (!orderBy.field || !orderBy.direction) {
        throw new UserInputError('Argument Error - order_by: Unable to process order_by argument. Missing order_by.field and/or order_by.direction.');
    }

    const beforeOrderBy = createOrderBy(orderBy.field, orderBy.direction);

    if (last && orderBy && before && orderBy.direction === 'DESC') {
        beforeOrderBy.direction = OrderByDirection.ASC;
    }

    if (last && orderBy && before && orderBy.direction === 'ASC') {
        beforeOrderBy.direction = OrderByDirection.DESC;
    }

    return beforeOrderBy;
};

export const createOrderByClause = (_first: number | undefined, last: number | undefined, orderBy: OrderBy | undefined): string => {
    const field = orderBy?.field ?? 'id';
    const direction = orderBy?.direction
        ? last
            ? reverseDirection(orderBy.direction)
            : orderBy.direction
        : OrderByDirection.DESC;
    return `ORDER BY ${field} ${direction} NULLS LAST, hash ASC, op_id ASC`;
};

function reverseDirection(direction: OrderByDirection): OrderByDirection {
    return direction === OrderByDirection.ASC ? OrderByDirection.DESC : OrderByDirection.ASC;
}

export const applyRelayOrder = function (first: number | undefined, last: number | undefined, before: string | undefined, after: string | undefined, orderBy: OrderBy, operationQuery: string): string {
    if (first && orderBy && !after && orderBy.direction !== 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && !before && !after && orderBy.direction === 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && before && orderBy.direction === 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && before && orderBy.direction === 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    if (last && !before && !after && orderBy && orderBy.direction === 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    if (last && !before && after && orderBy && orderBy.direction === 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && !before && after && orderBy && orderBy.direction === 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    return operationQuery;
};
