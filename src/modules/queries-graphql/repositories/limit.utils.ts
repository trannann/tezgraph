/* eslint-disable @typescript-eslint/strict-boolean-expressions */
/* eslint-disable func-style */

import { UserInputError } from 'apollo-server';

// Server Default Record Limit
export const pageSizeLimit: number = process.env.PAGE_SIZE_LIMIT ? parseInt(process.env.PAGE_SIZE_LIMIT, 10) : 200;

export const setTakeLimit = function (first: number | undefined, last: number | undefined): number {
    let takeLimit = pageSizeLimit;

    if (first && last) {
        throw new Error(`Argument Error: Cannot use both "first" and "last" arguments together.`);
    } else if (first) {
        takeLimit = first;
    } else if (last) {
        takeLimit = last;
    }

    limitValid(takeLimit);
    return takeLimit;
};

// Limit Validator
export const limitValid = function (limit: number | undefined): boolean {
    if (limit && limit > pageSizeLimit) {
        throw new UserInputError(`Argument "limit" of type "Float?" cannot be greater than ${pageSizeLimit}.`);
    }
    return true;
};
