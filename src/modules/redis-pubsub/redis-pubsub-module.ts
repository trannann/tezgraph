import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';
import { DependencyContainer } from 'tsyringe';

import { externalPubSubDIToken } from '../../utils/pubsub/external-pub-sub';
import { Module, ModuleName } from '../module';
import { onlyOnePubSubModuleEnabledDependency, RequireOneOfModulesDependency } from '../module-helpers';
import { RedisPubSubEnvConfig } from './redis-pubsub-env-config';
import { deserialize, serialize } from './redis-pubsub-serializer';

export const redisPubSubModule: Module = {
    name: ModuleName.RedisPubSub,

    dependencies: [
        new RequireOneOfModulesDependency(
            [ModuleName.TezosMonitor, ModuleName.SubscriptionsSubscriber],
            { reason: 'there is some producer or subscriber of notifications' },
        ),
        onlyOnePubSubModuleEnabledDependency,
    ],

    initialize(diContainer: DependencyContainer): void {
        diContainer.register(externalPubSubDIToken, {
            useFactory: c => {
                const envConfig = c.resolve(RedisPubSubEnvConfig);
                return new RedisPubSub({
                    // Needs differences instances to write and read.
                    publisher: new Redis(envConfig.redisConnectionString),
                    subscriber: new Redis(envConfig.redisConnectionString),
                    serializer: serialize,
                    deserializer: deserialize,
                });
            },
        });
    },
};
