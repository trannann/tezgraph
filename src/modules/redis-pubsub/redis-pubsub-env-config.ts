import { singleton } from 'tsyringe';

import { EnvConfigProvider } from '../../utils/configuration/env-config-provider';

export enum Names {
    RedisConnectionString = 'REDIS_CONNECTION_STRING',
}

@singleton()
export class RedisPubSubEnvConfig {
    readonly redisConnectionString: string;

    constructor(env: EnvConfigProvider) {
        this.redisConnectionString = env.getString(Names.RedisConnectionString);
    }
}
