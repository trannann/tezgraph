import { isAbortError } from '../../../utils/abortion';
import { ComponentHealthState } from '../../../utils/health/component-health-state';
import { HealthStatus } from '../../../utils/health/health-check';
import { IterableProvider } from '../../../utils/iterable/interfaces';
import { Logger } from '../../../utils/logging';
import { MonitorDelayHelper } from './monitor-delay-helper';

export const healthMessages = {
    healthy: 'Everything is fine.',
    temporaryFailure: 'Monitoring temporarily failed. It will reconnect.',
    persistentFailure: 'Monitoring failed multiple times. Now trying with the last delay indefinitely.',
} as const;

/**
 * Retries Tezos monitor connection on an end or an error because it's quite unstable.
 * This makes it infinite until return() on the iterator or underlying abort signal is aborted.
 * Retries are delayed according to config and the last delay also fails the related health check.
 */
export class AbstractRetryMonitorProvider<TItem> implements IterableProvider<TItem> {
    constructor(
        private readonly innerProvider: IterableProvider<TItem>,
        private readonly healthState: ComponentHealthState,
        private readonly delayHelper: MonitorDelayHelper,
        private readonly logger: Logger,
    ) {}

    async *iterate(): AsyncIterableIterator<TItem> {
        // It can be cancelled by calling return() on iterator.
        while (true) { // eslint-disable-line @typescript-eslint/no-unnecessary-condition
            try {
                for await (const item of this.innerProvider.iterate()) {
                    this.setHealthy();
                    yield item;
                }
                throw new Error('Connection ended which should not happen for a monitor.');
            } catch (error: unknown) {
                if (isAbortError(error)) {
                    this.logger.logInformation('Monitoring was aborted.');
                    return;
                }

                if (this.delayHelper.isInInfiniteLoop) {
                    this.setError(HealthStatus.Unhealthy, error, healthMessages.persistentFailure);
                } else {
                    this.setError(HealthStatus.Degraded, error, healthMessages.temporaryFailure);
                }

                await this.delayHelper.wait();
            }
        }
    }

    private setHealthy(): void {
        this.healthState.set(HealthStatus.Healthy, healthMessages.healthy);
        this.delayHelper.reset();
    }

    private setError(status: HealthStatus, error: unknown, reason: string): void {
        this.logger.logError(`${reason} {error}`, { error });
        this.healthState.set(status, { reason, error });
    }
}
