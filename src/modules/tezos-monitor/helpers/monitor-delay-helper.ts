import { singleton } from 'tsyringe';

import { guard } from '../../../utils/guard';
import { SleepHelper } from '../../../utils/time/sleep-helper';
import { TezosMonitorEnvConfig } from '../tezos-monitor-env-config';

@singleton()
export class MonitorDelayHelper {
    private isInInfiniteLoopFlag = false;
    private delays: number[] = [];

    constructor(
        private readonly envConfig: TezosMonitorEnvConfig,
        private readonly sleepHelper: SleepHelper,
    ) {
        this.reset();
    }

    get isInInfiniteLoop(): boolean {
        return this.isInInfiniteLoopFlag;
    }

    async wait(): Promise<void> {
        const delay = guard.notNullish(this.delays[0]);

        if (this.delays.length === 1) {
            this.isInInfiniteLoopFlag = true;
        } else {
            this.delays.shift();
        }

        await this.sleepHelper.sleep(delay);
    }

    reset(): void {
        this.delays = this.envConfig.rpcMonitorReconnectDelaysMillis.slice();
        this.isInInfiniteLoopFlag = false;
    }
}
