import { singleton } from 'tsyringe';

import { EnvConfigProvider, IntegerLimit } from '../../utils/configuration/env-config-provider';

export enum Names {
    RpcMonitorReconnectDelaysMillis = 'RPC_MONITOR_RECONNECT_DELAYS_MILLIS',
    TezosMonitorWarnChunkCount = 'TEZOS_MONITOR_WARN_CHUNK_COUNT',
    TezosMonitorFailChunkCount = 'TEZOS_MONITOR_FAIL_CHUNK_COUNT',
}

@singleton()
export class TezosMonitorEnvConfig {
    readonly rpcMonitorReconnectDelaysMillis: readonly number[];
    readonly tezosMonitorWarnChunkCount: number;
    readonly tezosMonitorFailChunkCount: number;

    constructor(env: EnvConfigProvider) {
        this.rpcMonitorReconnectDelaysMillis = env.getIntegerArray(Names.RpcMonitorReconnectDelaysMillis, IntegerLimit.PositiveOrZero);
        this.tezosMonitorWarnChunkCount = env.getInteger(Names.TezosMonitorWarnChunkCount, IntegerLimit.PositiveOrZero);
        this.tezosMonitorFailChunkCount = env.getInteger(Names.TezosMonitorFailChunkCount, IntegerLimit.PositiveOrZero);
    }
}
