import { DependencyContainer } from 'tsyringe';

import { BlockInfoProvider } from '../../entity/subscriptions/helpers/block-info-provider';
import { MempoolOperationGroupInfoProvider } from '../../entity/subscriptions/helpers/mempool-operation-group-info-provider';
import { healthChecksDIToken } from '../../utils/health/health-check';
import { registerIterableWorker } from '../../utils/iterable/iterable-worker-dependency-injection';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { BlockPubSubPublisher } from './block/pipeline/block-pubsub-publisher';
import { BlockMonitorHealth } from './block/pipeline/block-retry-monitor-provider';
import { FreezeBlockProvider } from './block/pipeline/freeze-block-provider';
import { ConvertMempoolProvider } from './mempool/pipeline/convert-mempool-provider';
import { MempoolPubSubPublisher } from './mempool/pipeline/mempool-pubsub-publisher';
import { MempoolMonitorHealth } from './mempool/pipeline/mempool-retry-monitor-provider';

export const tezosMonitorModule: Module = {
    name: ModuleName.TezosMonitor,

    dependencies: [new RequirePubSubDependency({ reason: 'notifications can be published to a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        // Block
        registerIterableWorker(diContainer, 'BlockPublisher', FreezeBlockProvider, BlockPubSubPublisher, BlockInfoProvider);
        diContainer.register(healthChecksDIToken, { useToken: BlockMonitorHealth });

        // Mempool
        registerIterableWorker(diContainer, 'MempoolPublisher', ConvertMempoolProvider, MempoolPubSubPublisher, MempoolOperationGroupInfoProvider);
        diContainer.register(healthChecksDIToken, { useToken: MempoolMonitorHealth });

        registerTezosRpcHealthWatcher(diContainer);
    },
};
