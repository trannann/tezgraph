import { inject, singleton } from 'tsyringe';

import { ComponentHealthState } from '../../../../utils/health/component-health-state';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../../utils/logging';
import { AbstractRetryMonitorProvider } from '../../helpers/abstract-retry-monitor-provider';
import { MonitorDelayHelper } from '../../helpers/monitor-delay-helper';
import { RpcMempoolOperationGroup } from '../rpc-mempool-operation-group';
import { DownloadMempoolProvider } from './download-mempool-provider';

/** Health state of mempool monitor. */
@singleton()
export class MempoolMonitorHealth extends ComponentHealthState {
    readonly name = 'MempoolMonitor';
}

/** See base class for more details. */
@singleton()
export class MempoolRetryMonitorProvider extends AbstractRetryMonitorProvider<RpcMempoolOperationGroup> {
    constructor(
        delayHelper: MonitorDelayHelper,
        healthState: MempoolMonitorHealth,
        @inject(DownloadMempoolProvider) innerProvider: IterableProvider<RpcMempoolOperationGroup>,
        @injectLogger(MempoolRetryMonitorProvider) logger: Logger,
    ) {
        super(innerProvider, healthState, delayHelper, logger);
    }
}
