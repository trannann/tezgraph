import { inject, singleton } from 'tsyringe';

import { numberOfLastItemsToCheck } from '../../../../entity/subscriptions/helpers/mempool-operation-group-identifier';
import { DeduplicateIterableProvider } from '../../../../utils/iterable/deduplicate-iterable-provider';
import { Identifier, IterableProvider } from '../../../../utils/iterable/interfaces';
import { RpcMempoolOperationGroup } from '../rpc-mempool-operation-group';
import { MempoolRetryMonitorProvider } from './mempool-retry-monitor-provider';

/** Gets ID. Used only here because MempoolOperationGroup (used on upper levels) has a different structure. */
@singleton()
export class RpcMempoolOperationGroupIdentifier implements Identifier<RpcMempoolOperationGroup> {
    getId(group: RpcMempoolOperationGroup): string {
        return group.signature;
    }
}

/** Removes duplicates when underlying monitor connection gets reset. */
@singleton()
export class DeduplicateMempoolProvider extends DeduplicateIterableProvider<RpcMempoolOperationGroup> {
    constructor(
        identifier: RpcMempoolOperationGroupIdentifier,
        @inject(MempoolRetryMonitorProvider) innerProvider: IterableProvider<RpcMempoolOperationGroup>,
    ) {
        super(innerProvider, identifier, numberOfLastItemsToCheck);
    }
}
