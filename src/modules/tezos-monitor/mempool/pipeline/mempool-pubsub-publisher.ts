import { singleton } from 'tsyringe';

import { getSignature } from '../../../../entity/subscriptions/helpers/mempool-operation-group-identifier';
import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../../utils/logging';
import { ExternalPubSub, externalTriggers } from '../../../../utils/pubsub/external-pub-sub';

/** Publishes GraphQL mempool operation groups to external PubSub. */
@singleton()
export class MempoolPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(MempoolPubSubPublisher) private readonly logger: Logger,
    ) {}

    processItem(mempoolGroup: MempoolOperationGroup): void {
        this.logger.logInformation('Publishing mempool operation group with {signature} and {operationCount}.', {
            signature: getSignature(mempoolGroup),
            operationCount: mempoolGroup.length,
        });
        this.externalPubSub.publish(externalTriggers.mempoolOperationGroups, mempoolGroup);
    }
}
