import { inject, singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { OperationGroupConverter } from '../../../../rpc/converters/operation-group-converter';
import { ConvertIterableProvider } from '../../../../utils/iterable/convert-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { RpcMempoolOperationGroup } from '../rpc-mempool-operation-group';
import { DeduplicateMempoolProvider } from './deduplicate-mempool-provider';

/** Converts a mempool operation group from RPC format to our GraphQL. */
@singleton()
export class ConvertMempoolProvider extends ConvertIterableProvider<RpcMempoolOperationGroup, MempoolOperationGroup> {
    constructor(
        private readonly operationGroupConverter: OperationGroupConverter,
        @inject(DeduplicateMempoolProvider) innerProvider: IterableProvider<RpcMempoolOperationGroup>,
    ) {
        super(innerProvider);
    }

    convertItem(rpcGroup: RpcMempoolOperationGroup): MempoolOperationGroup {
        return this.operationGroupConverter.convert(rpcGroup);
    }
}
