import * as rpc from '@taquito/rpc';

/** Data downloaded from Tezos node mempool monitor. */
export interface RpcMempoolOperationGroup {
    protocol: string;
    branch: string;
    signature: string;

    // Slightly adapted to make it compatible with block RPC -> common deserialization is easier.
    contents: (rpc.OperationContents | rpc.OperationContentsAndResult)[];
    chain_id: undefined;
    hash: undefined;
}
