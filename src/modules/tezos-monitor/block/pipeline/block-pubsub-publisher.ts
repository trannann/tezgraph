import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../../utils/logging';
import { ExternalPubSub, externalTriggers } from '../../../../utils/pubsub/external-pub-sub';

/** Publishes GraphQL blocks to external PubSub. */
@singleton()
export class BlockPubSubPublisher implements ItemProcessor<BlockNotification> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(BlockPubSubPublisher) private readonly logger: Logger,
    ) {}

    processItem(block: BlockNotification): void {
        this.logger.logInformation('Publishing block {hash} with {operationCount}.', {
            hash: block.hash,
            operationCount: block.operations.length,
        });
        this.externalPubSub.publish(externalTriggers.blocks, block);
    }
}
