import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { BlockConverter } from '../../../../rpc/converters/block-converter';
import { TezosRpcClient } from '../../../../rpc/tezos-rpc-client';
import { ConvertIterableProvider } from '../../../../utils/iterable/convert-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { RpcMonitorBlockHeader } from '../rpc-monitor-block-header';
import { DeduplicateBlocksProvider } from './deduplicate-blocks-provider';

/** Converts a block from RPC format to our GraphQL. */
@singleton()
export class ConvertBlocksProvider extends ConvertIterableProvider<RpcMonitorBlockHeader, BlockNotification> {
    constructor(
        private readonly rpcClient: TezosRpcClient,
        private readonly blockConverter: BlockConverter,
        @inject(DeduplicateBlocksProvider) innerProvider: IterableProvider<RpcMonitorBlockHeader>,
    ) {
        super(innerProvider);
    }

    async convertItem(monitorBlock: RpcMonitorBlockHeader): Promise<BlockNotification> {
        const rpcBlock = await this.rpcClient.getBlock(monitorBlock.hash);
        return this.blockConverter.convert(rpcBlock);
    }
}
