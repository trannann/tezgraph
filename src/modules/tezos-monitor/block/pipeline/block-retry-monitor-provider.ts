import { inject, singleton } from 'tsyringe';

import { ComponentHealthState } from '../../../../utils/health/component-health-state';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../../utils/logging';
import { AbstractRetryMonitorProvider } from '../../helpers/abstract-retry-monitor-provider';
import { MonitorDelayHelper } from '../../helpers/monitor-delay-helper';
import { RpcMonitorBlockHeader } from '../rpc-monitor-block-header';
import { DownloadBlocksProvider } from './download-blocks-provider';

/** Health state of block monitor. */
@singleton()
export class BlockMonitorHealth extends ComponentHealthState {
    readonly name = 'BlockTezosMonitor';
}

/** See base class for more details. */
@singleton()
export class BlockRetryMonitorProvider extends AbstractRetryMonitorProvider<RpcMonitorBlockHeader> {
    constructor(
        delayHelper: MonitorDelayHelper,
        healthState: BlockMonitorHealth,
        @inject(DownloadBlocksProvider) innerProvider: IterableProvider<RpcMonitorBlockHeader>,
        @injectLogger(BlockRetryMonitorProvider) logger: Logger,
    ) {
        super(innerProvider, healthState, delayHelper, logger);
    }
}
