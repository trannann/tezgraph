import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { FreezeIterableProvider } from '../../../../utils/iterable/freeze-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { UnassignBlockOnOperationsProvider } from './unassign-block-on-operations-provider';

/** Deep-freezes blocks even for hacks (e.g. Object.assign()) if they are already read-only on TypeScript level. */
@singleton()
export class FreezeBlockProvider extends FreezeIterableProvider<BlockNotification> {
    constructor(@inject(UnassignBlockOnOperationsProvider) innerProvider: IterableProvider<BlockNotification>) {
        super(innerProvider);
    }
}
