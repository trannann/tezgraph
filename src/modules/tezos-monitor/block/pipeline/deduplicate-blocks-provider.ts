import { inject, singleton } from 'tsyringe';

import { BlockIdentifier, numberOfLastItemsToCheck } from '../../../../entity/subscriptions/helpers/block-identifier';
import { DeduplicateIterableProvider } from '../../../../utils/iterable/deduplicate-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { RpcMonitorBlockHeader } from '../rpc-monitor-block-header';
import { BlockRetryMonitorProvider } from './block-retry-monitor-provider';

/** Removes duplicates when underlying monitor connection gets reset. */
@singleton()
export class DeduplicateBlocksProvider extends DeduplicateIterableProvider<RpcMonitorBlockHeader> {
    constructor(
        identifier: BlockIdentifier,
        @inject(BlockRetryMonitorProvider) innerProvider: IterableProvider<RpcMonitorBlockHeader>,
    ) {
        super(innerProvider, identifier, numberOfLastItemsToCheck);
    }
}
