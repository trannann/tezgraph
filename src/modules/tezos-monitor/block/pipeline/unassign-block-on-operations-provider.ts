import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { ConvertIterableProvider } from '../../../../utils/iterable/convert-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { ConvertBlocksProvider } from './convert-block-provider';

@singleton()
export class UnassignBlockOnOperationsProvider extends ConvertIterableProvider<BlockNotification, BlockNotification> {
    constructor(@inject(ConvertBlocksProvider) innerProvider: IterableProvider<BlockNotification>) {
        super(innerProvider);
    }

    convertItem(block: BlockNotification): BlockNotification {
        return {
            ...block,
            operations: block.operations.map(operation => ({
                ...operation,
                block: undefined,
            })),
        };
    }
}
