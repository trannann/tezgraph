import { ResolverContext } from '../../../bootstrap/resolver-context';
import { Logger } from '../../../utils/logging';

export interface AbstractSubscriptionOptions {
    readonly args: unknown;
    readonly context: ResolverContext;
    readonly subscriptionName: string;
}

export interface AbstractSubscription {
    subscribe(options: AbstractSubscriptionOptions): AsyncIterator<unknown>;
}

export const loggerCategory = 'LoggingSubscription';

export abstract class AbstractLoggingSubscription {
    constructor(
        private readonly logger: Logger,
        private readonly innerSubscription: AbstractSubscription,
    ) {}

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    subscribe(options: AbstractSubscriptionOptions): AsyncIterableIterator<any> {
        this.logger.logInformation('Opening {subscription} for {requestId} with {args}.', {
            subscription: options.subscriptionName,
            requestId: options.context.requestId,
            args: options.args,
            rawQuery: options.context.connection?.query,
            rawQueryVariables: options.context.connection?.variables,
        });

        const innerIterable = this.innerSubscription.subscribe(options);

        const logSubscriptionClosing = (): void => {
            this.logger.logInformation('Closing {subscription} for {requestId}.', {
                subscription: options.subscriptionName,
                requestId: options.context.requestId,
            });
        };
        return {
            next: innerIterable.next.bind(innerIterable),

            async return(value: unknown): Promise<IteratorResult<unknown, unknown>> {
                logSubscriptionClosing();
                return await innerIterable.return?.(value) ?? { done: true, value };
            },
            async throw(error: unknown): Promise<IteratorResult<unknown, unknown>> {
                logSubscriptionClosing();
                return await innerIterable.throw?.(error) ?? { done: true, value: error };
            },
            [Symbol.asyncIterator](): AsyncIterableIterator<unknown> {
                return this;
            },
        };
    }
}
