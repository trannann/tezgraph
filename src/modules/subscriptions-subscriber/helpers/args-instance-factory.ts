/* istanbul ignore file */
import { ArgsDictionary, getMetadataStorage } from 'type-graphql';
import { ArgsParamMetadata } from 'type-graphql/dist/metadata/definitions';
import { convertArgsToInstance } from 'type-graphql/dist/resolvers/convert-args';

import { DefaultConstructor, NonNullishValue } from '../../../utils/reflection';

export function createArgsInstance<TArgs extends NonNullishValue>(argsType: DefaultConstructor<TArgs>, argsDictionary: ArgsDictionary): TArgs {
    const metadata = getMetadata(argsType);
    return convertArgsToInstance(metadata, argsDictionary) as TArgs;
}

// Class name isn't unique b/c it's generated using func -> use constructor as key.
const metadataCache = new Map<DefaultConstructor, ArgsParamMetadata>();

function getMetadata(argsType: DefaultConstructor): ArgsParamMetadata {
    let metadata = metadataCache.get(argsType);
    if (!metadata) {
        const storage = getMetadataStorage();
        metadata = storage.params.find(p => p.kind === 'args' && p.getType() === argsType) as ArgsParamMetadata;
        metadataCache.set(argsType, metadata);
    }
    return metadata;
}
