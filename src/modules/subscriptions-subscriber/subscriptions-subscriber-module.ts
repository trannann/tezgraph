import { DependencyContainer } from 'tsyringe';

import { graphQLResolversDIToken } from '../../bootstrap/graphql-resolver-type';
import { BlockInfoProvider } from '../../entity/subscriptions/helpers/block-info-provider';
import { MempoolOperationGroupInfoProvider } from '../../entity/subscriptions/helpers/mempool-operation-group-info-provider';
import { registerIterableWorker } from '../../utils/iterable/iterable-worker-dependency-injection';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { blockOperationsResolvers } from './block-subscription/block-operations-resolver';
import { BlockSubscriptionResolver } from './block-subscription/block-subscription-resolver';
import { operationSubscriptionResolvers } from './operation-subscription/operation-subscription-resolvers';
import { BlockPubSubPublisher } from './pubsub/block-pipeline/block-pubsub-publisher';
import { FreezeBlockProvider } from './pubsub/block-pipeline/freeze-block-provider';
import { FreezeMempoolProvider } from './pubsub/mempool-pipeline/freeze-mempool-provider';
import { MempoolPubSubPublisher } from './pubsub/mempool-pipeline/mempool-pubsub-pubsliher';

export const subscriptionsSubscriberModule: Module = {
    name: ModuleName.SubscriptionsSubscriber,

    dependencies: [new RequirePubSubDependency({ reason: 'subscriptions can be created from a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        for (const type of [BlockSubscriptionResolver, ...operationSubscriptionResolvers, ...blockOperationsResolvers]) {
            diContainer.registerInstance(graphQLResolversDIToken, type);
        }

        registerTezosRpcHealthWatcher(diContainer);

        registerIterableWorker(diContainer, 'BlockSubscriber', FreezeBlockProvider, BlockPubSubPublisher, BlockInfoProvider);
        registerIterableWorker(diContainer, 'MempoolSubscriber', FreezeMempoolProvider, MempoolPubSubPublisher, MempoolOperationGroupInfoProvider);
    },
};
