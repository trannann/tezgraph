/* istanbul ignore file */
import { Args, Authorized, Resolver, Root, Subscription } from 'type-graphql';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { SubscriptionArgs } from '../../../entity/subscriptions/args/subscription-args';
import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { nameof } from '../../../utils/reflection';
import { createArgsInstance } from '../helpers/args-instance-factory';
import { blockSubscriptionDIToken } from './block-subscription';

const subscriptionName = nameof<BlockSubscriptionResolver>('blockAdded');

@Resolver()
export class BlockSubscriptionResolver {
    @Authorized()
    @Subscription({
        subscribe: (_root, argsDictionary, context: ResolverContext) => {
            const args = createArgsInstance(SubscriptionArgs, argsDictionary);
            const subscription = context.container.resolve(blockSubscriptionDIToken);
            return subscription.subscribe({ args, context, subscriptionName });
        },
    })
    blockAdded(
        @Root() block: BlockNotification,
        @Args(() => SubscriptionArgs) _args: SubscriptionArgs, // eslint-disable-line @typescript-eslint/indent
    ): BlockNotification {
        return block;
    }
}
