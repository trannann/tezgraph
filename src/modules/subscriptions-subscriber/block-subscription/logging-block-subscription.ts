import { singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../utils/logging';
import { AbstractLoggingSubscription, loggerCategory } from '../helpers/abstract-logging-subscription';
import { BlockSubscription } from './block-subscription';
import { ReplayBlockSubscription } from './replay-block-subscription';

@singleton()
export class LoggingBlockSubscription extends AbstractLoggingSubscription implements BlockSubscription {
    constructor(
        innerSubscription: ReplayBlockSubscription,
        @injectLogger(loggerCategory) logger: Logger,
    ) {
        super(logger, innerSubscription);
    }
}
