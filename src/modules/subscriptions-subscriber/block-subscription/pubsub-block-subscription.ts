import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { SubscriberPubSub, subscriberTriggers } from '../pubsub/subscriber-pub-sub';
import { BlockSubscription, BlockSubscriptionOptions } from './block-subscription';

@singleton()
export class PubSubBlockSubscription implements BlockSubscription {
    constructor(private readonly pubSub: SubscriberPubSub) {}

    subscribe(_options: BlockSubscriptionOptions): AsyncIterableIterator<BlockNotification> {
        return this.pubSub.iterate([subscriberTriggers.blocks]);
    }
}
