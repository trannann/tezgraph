import { inject, singleton } from 'tsyringe';

import { FilterOptimizer } from '../../../entity/subscriptions/filters/filter-optimizer';
import { OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { filter } from '../../../utils/collections/async-iterable-utils';
import { OperationSubscription, SubscriptionOptions } from './operation-subscription';
import { ReplayOperationSubscription } from './replay-operation-subscription';

@singleton()
export class FilteringOperationSubscription implements OperationSubscription {
    constructor(
        @inject(ReplayOperationSubscription) private readonly innerSubscription: OperationSubscription,
        private readonly filterOptimizer: FilterOptimizer,
    ) {}

    subscribe<TOperation extends OperationNotification>(options: SubscriptionOptions<TOperation>): AsyncIterableIterator<TOperation> {
        const operationFilter = options.args.filter ? this.filterOptimizer.optimize(options.args.filter) : null;
        const iterable = this.innerSubscription.subscribe(options);

        return operationFilter
            ? filter(iterable, o => operationFilter.passes(o))
            : iterable;
    }
}
