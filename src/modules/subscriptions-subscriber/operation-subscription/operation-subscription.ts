import { InjectionToken } from 'tsyringe';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { OperationArgs } from '../../../entity/subscriptions/args/operation-args';
import { OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { DefaultConstructor } from '../../../utils/reflection';
import { LoggingOperationSubscription } from './logging-operation-subscription';

export interface SubscriptionOptions<TOperation extends OperationNotification> {
    readonly operationType: DefaultConstructor<TOperation>;
    readonly args: OperationArgs<TOperation>;
    readonly context: ResolverContext;
    readonly subscriptionName: string;
}

export interface OperationSubscription {
    subscribe<TOperation extends OperationNotification>(
        options: SubscriptionOptions<TOperation>,
    ): AsyncIterableIterator<TOperation>;
}

export const operationSubscriptionDIToken: InjectionToken<OperationSubscription> = LoggingOperationSubscription;
