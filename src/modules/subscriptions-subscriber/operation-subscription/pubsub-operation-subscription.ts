import { singleton } from 'tsyringe';

import { getOperationKind, OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { SubscriberPubSub, subscriberTriggers } from '../pubsub/subscriber-pub-sub';
import { OperationSubscription, SubscriptionOptions } from './operation-subscription';

@singleton()
export class PubSubOperationSubscription implements OperationSubscription {
    constructor(private readonly pubSub: SubscriberPubSub) {}

    subscribe<TOperation extends OperationNotification>(options: SubscriptionOptions<TOperation>): AsyncIterableIterator<TOperation> {
        const kind = getOperationKind(options.operationType);
        const triggers = [
            subscriberTriggers.blockOperations[kind],
            ...options.args.includeMempool ? [subscriberTriggers.mempoolOperations[kind]] : [],
        ];
        return this.pubSub.iterate<TOperation>(triggers);
    }
}
