/* istanbul ignore file */
import { ActivateAccountNotification } from '../../../entity/subscriptions/activate-account-notification';
import { ActivateAccountArgs } from '../../../entity/subscriptions/args/activate-account-args';
import { BallotArgs } from '../../../entity/subscriptions/args/ballot-args';
import { DelegationArgs } from '../../../entity/subscriptions/args/delegation-args';
import { DoubleBakingEvidenceArgs } from '../../../entity/subscriptions/args/double-baking-evidence-args';
import { DoubleEndorsementEvidenceArgs } from '../../../entity/subscriptions/args/double-endorsement-evidence-args';
import { EndorsementArgs } from '../../../entity/subscriptions/args/endorsement-args';
import { OriginationArgs } from '../../../entity/subscriptions/args/origination-args';
import { ProposalsArgs } from '../../../entity/subscriptions/args/proposals-args';
import { RevealArgs } from '../../../entity/subscriptions/args/reveal-args';
import { SeedNonceRevelationArgs } from '../../../entity/subscriptions/args/seed-nonce-revelation-args';
import { TransactionArgs } from '../../../entity/subscriptions/args/transaction-args';
import { BallotNotification } from '../../../entity/subscriptions/ballot-notification';
import { DelegationNotification } from '../../../entity/subscriptions/delegation-notification';
import { DoubleBakingEvidenceNotification } from '../../../entity/subscriptions/double-baking-evidence-notification';
import {
    DoubleEndorsementEvidenceNotification,
} from '../../../entity/subscriptions/double-endorsement-evidence-notification';
import { EndorsementNotification } from '../../../entity/subscriptions/endorsement-notification';
import { OriginationNotification } from '../../../entity/subscriptions/origination-notification';
import { ProposalsNotification } from '../../../entity/subscriptions/proposals-notification';
import { RevealNotification } from '../../../entity/subscriptions/reveal-notification';
import { SeedNonceRevelationNotification } from '../../../entity/subscriptions/seed-nonce-revelation-notification';
import { TransactionNotification } from '../../../entity/subscriptions/transaction-notification';
import { createOperationSubscriptionResolverClass } from './operation-subscription-resolver-factory';

const create = createOperationSubscriptionResolverClass;

/** GraphQL resolvers for all operation subscriptions. */
export const operationSubscriptionResolvers = [
    create(ActivateAccountNotification, ActivateAccountArgs),
    create(BallotNotification, BallotArgs),
    create(DelegationNotification, DelegationArgs),
    create(DoubleBakingEvidenceNotification, DoubleBakingEvidenceArgs),
    create(DoubleEndorsementEvidenceNotification, DoubleEndorsementEvidenceArgs),
    create(EndorsementNotification, EndorsementArgs),
    create(OriginationNotification, OriginationArgs),
    create(ProposalsNotification, ProposalsArgs),
    create(RevealNotification, RevealArgs),
    create(SeedNonceRevelationNotification, SeedNonceRevelationArgs),
    create(TransactionNotification, TransactionArgs),
];
