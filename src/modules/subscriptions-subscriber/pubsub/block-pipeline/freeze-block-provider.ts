import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { FreezeIterableProvider } from '../../../../utils/iterable/freeze-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { AssignBlockToOperationsProvider } from './assign-block-to-operations-provider';

/** Deep-freezes blocks even for hacks (e.g. Object.assign()) if they are already read-only on TypeScript level. */
@singleton()
export class FreezeBlockProvider extends FreezeIterableProvider<BlockNotification> {
    constructor(@inject(AssignBlockToOperationsProvider) innerProvider: IterableProvider<BlockNotification>) {
        super(innerProvider);
    }
}
