import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { BlockIdentifier, numberOfLastItemsToCheck } from '../../../../entity/subscriptions/helpers/block-identifier';
import { DeduplicateIterableProvider } from '../../../../utils/iterable/deduplicate-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { BlockPubSubProvider } from './block-pubsub-provider';

/** Removes duplicates when multiple Tezos monitors (in the fail-over deployment) push the same item to the external PubSub. */
@singleton()
export class DeduplicateBlocksProvider extends DeduplicateIterableProvider<BlockNotification> {
    constructor(
        identifier: BlockIdentifier,
        @inject(BlockPubSubProvider) innerProvider: IterableProvider<BlockNotification>,
    ) {
        super(innerProvider, identifier, numberOfLastItemsToCheck);
    }
}
