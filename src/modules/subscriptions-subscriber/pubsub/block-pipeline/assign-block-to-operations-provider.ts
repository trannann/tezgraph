import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../entity/subscriptions/operation-notification';
import { ConvertIterableProvider } from '../../../../utils/iterable/convert-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { DeduplicateBlocksProvider } from './deduplicate-blocks-provider';

@singleton()
export class AssignBlockToOperationsProvider extends ConvertIterableProvider<BlockNotification, BlockNotification> {
    constructor(@inject(DeduplicateBlocksProvider) innerProvider: IterableProvider<BlockNotification>) {
        super(innerProvider);
    }

    convertItem(block: BlockNotification): BlockNotification {
        const alignedOperations = Array<OperationNotification>();
        const alignedBlock: BlockNotification = {
            ...block,
            operations: alignedOperations,
        };

        for (const operation of block.operations) {
            alignedOperations.push({
                ...operation,
                block: alignedBlock,
            });
        }
        return alignedBlock;
    }
}
