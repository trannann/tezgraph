import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';

@singleton()
export class BlockPubSubPublisher implements ItemProcessor<BlockNotification> {
    constructor(private readonly subscriberPubSub: SubscriberPubSub) {}

    processItem(block: BlockNotification): void {
        this.subscriberPubSub.publish(subscriberTriggers.blocks, block);

        for (const operation of block.operations) {
            const trigger = subscriberTriggers.blockOperations[operation.kind];
            this.subscriberPubSub.publish(trigger, operation);
        }
    }
}
