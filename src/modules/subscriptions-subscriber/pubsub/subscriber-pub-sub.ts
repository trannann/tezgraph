import { PubSub } from 'graphql-subscriptions';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { asReadonly } from '../../../utils/conversion';
import { injectLogger, Logger } from '../../../utils/logging';
import { TypedPubSub } from '../../../utils/pubsub/typed-pub-sub';

export class PubSubTrigger<_TPayload> { /* eslint-disable-line @typescript-eslint/no-unused-vars */
    constructor(readonly name: string) {}
}

/** Strongly-typed triggers for particular payload. */
export const subscriberTriggers = {
    blocks: new PubSubTrigger<BlockNotification>('BLOCKS'),

    // Separate mempool b/c it's not included by default -> don't evaluate all consumers -> performance.
    blockOperations: getTriggerForEachKind('BLOCK_OPERATIONS'),
    mempoolOperations: getTriggerForEachKind('MEMPOOL_OPERATIONS'),
} as const;

function getTriggerForEachKind(namePrefix: string): Readonly<Record<OperationKind, PubSubTrigger<OperationNotification>>> {
    const result: Record<string, PubSubTrigger<OperationNotification>> = {};
    for (const kind of Object.keys(OperationKind)) {
        result[kind] = new PubSubTrigger<OperationNotification>(`${namePrefix}:${kind}`);
    }
    return asReadonly(result);
}

/** Strongly-typed PubSub. */
@singleton()
export class SubscriberPubSub extends TypedPubSub {
    constructor(@injectLogger(SubscriberPubSub) logger: Logger) {
        super(new PubSub(), logger);
    }
}
