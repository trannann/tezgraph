import { singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';

@singleton()
export class MempoolPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(private readonly subscriberPubSub: SubscriberPubSub) {}

    processItem(operationGroup: MempoolOperationGroup): void {
        for (const operation of operationGroup) {
            const trigger = subscriberTriggers.mempoolOperations[operation.kind];
            this.subscriberPubSub.publish(trigger, operation);
        }
    }
}
