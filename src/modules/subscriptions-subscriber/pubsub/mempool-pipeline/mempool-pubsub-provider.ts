import { singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { ExternalPubSub, externalTriggers } from '../../../../utils/pubsub/external-pub-sub';

@singleton()
export class MempoolPubSubProvider implements IterableProvider<MempoolOperationGroup> {
    constructor(private readonly externalPubSub: ExternalPubSub) {}

    iterate(): AsyncIterableIterator<MempoolOperationGroup> {
        return this.externalPubSub.iterate([externalTriggers.mempoolOperationGroups]);
    }
}
