import { inject, singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { FreezeIterableProvider } from '../../../../utils/iterable/freeze-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { DeduplicateMempoolProvider } from './deduplicate-mempool-provider';

/** Deep-freezes groups even for hacks (e.g. Object.assign()) if they are already read-only on TypeScript level. */
@singleton()
export class FreezeMempoolProvider extends FreezeIterableProvider<MempoolOperationGroup> {
    constructor(@inject(DeduplicateMempoolProvider) innerProvider: IterableProvider<MempoolOperationGroup>) {
        super(innerProvider);
    }
}
