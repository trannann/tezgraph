import { inject, singleton } from 'tsyringe';

import {
    MempoolOperationGroupIdentifier,
    numberOfLastItemsToCheck,
} from '../../../../entity/subscriptions/helpers/mempool-operation-group-identifier';
import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { DeduplicateIterableProvider } from '../../../../utils/iterable/deduplicate-iterable-provider';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { MempoolPubSubProvider } from './mempool-pubsub-provider';

/** Removes duplicates when multiple Tezos monitors (in the fail-over deployment) push the same item to the external PubSub. */
@singleton()
export class DeduplicateMempoolProvider extends DeduplicateIterableProvider<MempoolOperationGroup> {
    constructor(
        identifier: MempoolOperationGroupIdentifier,
        @inject(MempoolPubSubProvider) innerProvider: IterableProvider<MempoolOperationGroup>,
    ) {
        super(innerProvider, identifier, numberOfLastItemsToCheck);
    }
}
