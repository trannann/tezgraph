import { createLogger, format, LoggerOptions, transports } from 'winston';

const { combine, timestamp, simple } = format;

export interface Logger {
    debug(message: string): void;
    info(message: string, parameters: { status: string; uuid: string; duration?: string; query?: string }): void;
    warn(message: string): void;
    error(message: string, parameters: { status: string; uuid: string; type: string; error: any; query?: string }): void;

}

const loggerConfig: LoggerOptions = {
    format: combine(timestamp(), simple()),
    transports: [
        new transports.Console(),
    ],
};

export const logger: Logger = createLogger(loggerConfig);
