import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import { injectable, injectAll } from 'tsyringe';

import { ApolloServerFactory } from './bootstrap/apollo-server-factory';
import { BackgroundWorker, backgroundWorkersDIToken } from './bootstrap/background-worker';
import { EnvConfig } from './utils/configuration/env-config';
import { HealthHttpHandler } from './utils/health/health-http-handler';
import { HttpRouteProvider, httpRouteProviderDIToken } from './utils/http-route-provider';
import { injectLogger, Logger } from './utils/logging';

@injectable()
export class App {
    apolloServer: ApolloServer | undefined;
    httpServer: http.Server | undefined;

    constructor(
        private readonly envConfig: EnvConfig,
        private readonly apolloServerFactory: ApolloServerFactory,
        @injectAll(backgroundWorkersDIToken) private readonly backgroundWorkers: readonly BackgroundWorker[],
        @injectAll(httpRouteProviderDIToken) private readonly httpRouteProviders: readonly HttpRouteProvider[],
        private readonly healthHttpHandler: HealthHttpHandler,
        @injectLogger(App) private readonly logger: Logger,
    ) { }

    async start(): Promise<void> {
        if (!this.httpServer || !this.apolloServer) {
            await this.initialize();
        }

        await Promise.all(this.backgroundWorkers.map(async worker => {
            this.logWorker('Starting', worker);
            await worker.start();
            this.logWorker('Started', worker);
        }));

        this.logger.logInformation('Server starting at {url}.', { url: `http://${this.envConfig.host}:${this.envConfig.port}` });
        await new Promise<void>(resolve => {
            this.httpServer?.listen(this.envConfig.port, this.envConfig.host, resolve);
        });
        this.logger.logInformation('Server started.');
    }

    async stop(): Promise<void> {
        if (!this.httpServer || !this.apolloServer) {
            throw new Error('App is not initialized.');
        }

        await Promise.all(this.backgroundWorkers.map(async worker => {
            this.logWorker('Stopping', worker);
            await worker.stop();
            this.logWorker('Stopped', worker);
        }));

        this.logger.logInformation('Server stopping.');
        await this.apolloServer.stop();
        await new Promise(resolve => {
            this.httpServer?.close(resolve);
        });

        this.logger.logInformation('Server stopped.');
        this.logger.close();
    }

    async initialize(): Promise<void> {
        const expressApp = express();
        this.httpServer = http.createServer(expressApp);
        this.apolloServer = await this.apolloServerFactory.create();

        for (const route of this.httpRouteProviders.flatMap(p => p.routes)) {
            expressApp.get(route.path, (request, response) => route.handler(response, request));
        }

        this.apolloServer.applyMiddleware({
            app: expressApp,
            path: '/graphql',
            onHealthCheck: async () => this.healthHttpHandler.checkIsHealthy(),
        });
        this.apolloServer.installSubscriptionHandlers(this.httpServer);
    }

    private logWorker(verb: string, worker: BackgroundWorker): void {
        this.logger.logInformation(`${verb} {worker}.`, { worker: worker.name });
    }
}
