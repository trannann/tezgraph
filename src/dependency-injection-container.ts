import { RpcClient } from '@taquito/rpc';
import { AbortController, AbortSignal } from 'abort-controller';
import NodeCache from 'node-cache';
import { Registry } from 'prom-client';
import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { backgroundWorkersDIToken } from './bootstrap/background-worker';
import { graphQLMiddlewaresDIToken } from './bootstrap/graphql-middleware-type';
import { graphQLResolversDIToken } from './bootstrap/graphql-resolver-type';
import { memoryPubSubModule } from './modules/memory-pubsub/memory-pubsub-module';
import { CompositeModule, CompositeModuleName, compositeModulesDIToken, modulesDIToken } from './modules/module';
import { ModulesInitializer } from './modules/modules-initializer';
import { queriesGraphQLModule } from './modules/queries-graphql/queries-graphql-module';
import { redisPubSubModule } from './modules/redis-pubsub/redis-pubsub-module';
import { subscriptionsSubscriberModule } from './modules/subscriptions-subscriber/subscriptions-subscriber-module';
import { tezosMonitorModule } from './modules/tezos-monitor/tezos-monitor-module';
import { VersionGraphQLResolver } from './utils/app-version/version-graphql-resolver';
import { VersionMetricsWorker } from './utils/app-version/version-metrics-worker';
import { EnvConfig } from './utils/configuration/env-config';
import { processEnvDIToken } from './utils/configuration/env-config-provider';
import { diContainerDIToken, registerSingleton } from './utils/dependency-injection';
import { HealthHttpHandler } from './utils/health/health-http-handler';
import { httpRouteProviderDIToken } from './utils/http-route-provider';
import { createRootLogger, LoggerFactory } from './utils/logging';
import { MetricsHttpHandler } from './utils/metrics/metrics-http-handler';
import { MetricsMiddleware } from './utils/metrics/metrics-middleware';
import { MetricsWorker } from './utils/metrics/metrics-worker';
import { Clock } from './utils/time/clock';

export function createDIContainer(processEnv: NodeJS.ProcessEnv): DependencyContainer {
    const diContainer = globalDIContainer.createChildContainer();

    diContainer.registerInstance(processEnvDIToken, processEnv);
    diContainer.registerInstance(diContainerDIToken, diContainer);
    diContainer.registerInstance(NodeCache, new NodeCache({ checkperiod: 1 }));
    registerSingleton(diContainer, LoggerFactory, c => new LoggerFactory(createRootLogger(c.resolve(EnvConfig)), c.resolve(Clock)));
    registerSingleton(diContainer, RpcClient, c => new RpcClient(c.resolve(EnvConfig).tezosNodeUrl));

    registerAbortion(diContainer);
    registerVersionGraphQL(diContainer);
    registerMetrics(diContainer);
    registeHttpRouteProviders(diContainer);
    registerModules(diContainer);

    diContainer.resolve(ModulesInitializer).initializeConfiguredModules();
    return diContainer;
}

function registerAbortion(diContainer: DependencyContainer): void {
    const abortController = new AbortController();
    diContainer.registerInstance(AbortController, abortController);
    diContainer.registerInstance(AbortSignal, abortController.signal);
}

function registerVersionGraphQL(diContainer: DependencyContainer): void {
    diContainer.registerInstance(graphQLResolversDIToken, VersionGraphQLResolver);
    diContainer.register(backgroundWorkersDIToken, { useToken: VersionMetricsWorker });
}

function registerMetrics(diContainer: DependencyContainer): void {
    diContainer.register(backgroundWorkersDIToken, { useToken: MetricsWorker });
    diContainer.registerInstance(graphQLMiddlewaresDIToken, MetricsMiddleware);
    diContainer.registerInstance(Registry, new Registry());
}

function registeHttpRouteProviders(diContainer: DependencyContainer): void {
    diContainer.register(httpRouteProviderDIToken, { useToken: HealthHttpHandler });
    diContainer.register(httpRouteProviderDIToken, { useToken: MetricsHttpHandler });

    diContainer.registerInstance(httpRouteProviderDIToken, {
        routes: [{ path: '/', handler: (response): void => response.redirect('/graphql') }],
    });
}

function registerModules(diContainer: DependencyContainer): void {
    diContainer.registerInstance(modulesDIToken, queriesGraphQLModule);
    diContainer.registerInstance(modulesDIToken, subscriptionsSubscriberModule);
    diContainer.registerInstance(modulesDIToken, tezosMonitorModule);
    diContainer.registerInstance(modulesDIToken, memoryPubSubModule);
    diContainer.registerInstance(modulesDIToken, redisPubSubModule);

    diContainer.registerInstance(compositeModulesDIToken, CompositeModule.create(
        CompositeModuleName.SubscriptionsGraphQL,
        [subscriptionsSubscriberModule, memoryPubSubModule, tezosMonitorModule],
    ));
}
