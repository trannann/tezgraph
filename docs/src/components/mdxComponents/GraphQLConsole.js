import * as React from "react";
import GraphiQL from "graphiql";
import "graphiql/graphiql.min.css";
import styled from "@emotion/styled";
import { parse } from "graphql";
import { SubscriptionClient } from "subscriptions-transport-ws";
import { InMemoryCache, ApolloClient, gql } from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";

const GraphQLConsole = ({URL, defaultQuery, variables}) => {
  const maxSubscriptionMinutes = 10;
  let wsClient  = null;
  let apolloClient = null;
  let cancelSubscriptionTimeoutId = null;

  const graphQLFetcher = (graphQLParams) => {
    destroySubscription();
    if (hasSubscriptionOperation(graphQLParams)) {
      wsClient  = new SubscriptionClient(URL.replace("https://", "wss://"));
      apolloClient = new ApolloClient({
        uri: URL,
        link: new WebSocketLink(wsClient),
        cache: new InMemoryCache(),
      });

      return {
        subscribe: (observer) => {
          observer.next("Subscribed. Waiting for data...");

          cancelSubscriptionTimeoutId = setTimeout(() => {
            observer.next(`Subscription was automatically cancelled after ${maxSubscriptionMinutes} minutes in order not to overload demo servers.`);
            destroySubscription();
          }, maxSubscriptionMinutes * 60 * 1000)

          apolloClient
            .subscribe({
              query: gql(graphQLParams.query),
              variables: graphQLParams.variables,
            })
            .subscribe({
              next (data) {
                observer.next(data);
              },
              error (err) {
                observer.error(err);
              },
              complete() {
                observer.next("Subscription ended.");
              },
            });
        },
      };
    } else {
      return fetch(URL, {
        method: "post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(graphQLParams),
        credentials: "omit"
      }).then(response => response.json());
    }
  };

  function destroySubscription() {
    if (cancelSubscriptionTimeoutId) {
      clearTimeout(cancelSubscriptionTimeoutId);
      cancelSubscriptionTimeoutId = null;
    }
    if (apolloClient) {
      apolloClient.stop();
      apolloClient = null;
    }
    if (wsClient) {
      wsClient.unsubscribeAll();
      wsClient.close();
      wsClient = null;
    }
  }

  const [value, setValue] = React.useState(null);
  React.useEffect(() => {
    setTimeout(() => {
      setValue("something");
    }, 1000);
  }, []);

  return (
    <Wrapper>
      {value ? (
        <GraphiQL
          fetcher={graphQLFetcher}
          query={defaultQuery}
          variables={variables}
          storage={null}
        />
      ) : null}
    </Wrapper>
  );
};

const Wrapper = styled.div`
display: flex;
height: 50vh;
`

export default GraphQLConsole;

function hasSubscriptionOperation(graphQlParams) {
  const queryDoc = parse(graphQlParams.query);

  for (let definition of queryDoc.definitions) {
    if (definition.kind === "OperationDefinition") {
      const operation = definition.operation;
      if (operation === "subscription") {
        return true;
      }
    }
  }

  return false;
}

