import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TabListItem = styled.div`
  display: flex;
  padding: 0.5rem 0.75rem;
  min-width: 0;
  white-space: break-spaces;
  height:fit-content;
  color: grey;
  text-transform: capitalize;
  font-weight: 600;
  padding: 16px 30px;
  border-bottom: 0.5px solid #8080807a;
  padding-bottom: 10px;

  ${({ active }) => active && `
    background: white;
    color: black;
    border: solid #ccc;
    border-width: 1px 1px 0 1px;
    border-bottom-color: #6c63ff;
    border-bottom-width: 10px;
    padding-bottom: 0px;
  `}

`

class Tab extends Component {
  
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  }

  render() {
    const {
      onClick,
      props: {
        activeTab,
        label,
      },
    } = this;

    let isTabActive = false
    if(activeTab === label) {
      isTabActive = true
    }

    return (
      <TabListItem
        onClick={onClick}
        active={isTabActive}
      >
        {label}
      </TabListItem>
    );
  }
}

export default Tab;