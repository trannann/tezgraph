import React from "react";
import styled from '@emotion/styled';
// import { Tabs } from "@feuer/react-tabs";
import ReactHtmlParser from 'react-html-parser';
import {Tabs} from './Tabs'

const TabsComponent = ({ children }) => {
  const tabItems = ReactHtmlParser(children)

  return (
    <div>
   <Tabs> 
       {tabItems.map((tab, i) => {
         const {title, children} = tab.props
         const tabId = `tab${i+1}`
        return (
          <div label={title}>
          <Wrapper>{children}</Wrapper>
          </div>   
        )
       })}
   </Tabs> 
  </div>
  //   <Tabs
  //   tabsProps={{
  //     style: {
  //       textAlign: "left",
  //       display:"flex",
  //       flexDirection:"row",
  //       width: '200px',
  //     }
  //   }}
  //   activeTab={{
  //     id: "tab1"
  //   }}>
    //  <>
    //    {tabItems.map((tab, i) => {
    //      const {title, children} = tab.props
    //      const tabId = `tab${i+1}`
    //     return (
    //     <Tabs.Tab id={tabId} title={title} style={{display:"flex"}}>
    //        <Wrapper>{children}</Wrapper>
    //     </Tabs.Tab>
    //     )
    //    })}
    //  </>
  // </Tabs>
  );
};

const Wrapper = styled.div`
  display: flex;
  overflow: auto;
  padding: 2%;
`
const TabWrapper = styled.div`
  width: 10%;
`

export default TabsComponent;
