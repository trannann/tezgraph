import React from 'react';
import styled from '@emotion/styled'

import AvLogo from '../../images/logos/AV_logo.png';
import NomadicLabsLogo from '../../images/logos/nomadicLabs.png';
import EcadLogo from '../../images/logos/ecadLogo.png';

const Team = () => (
 <Section id="team" accent="secondary">
 <Container style={{ position: 'relative' }}>
   <h1>The Team</h1>
   <TeamGrid>
   <a href='https://www.ecadlabs.com/'>
     <EcadImage src={EcadLogo} alt="link" />
    </a>
    <a href='https://www.agile-ventures.com/'>
      <Image src={AvLogo} alt="link" />
    </a>
   <a href='https://www.nomadic-labs.com/'>
     <Image src={NomadicLabsLogo} alt="link" />
    </a>
   </TeamGrid>
 </Container>
</Section>
);

export const Container = styled.div`
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
  padding: 0 16px;

  @media (min-width: ${props => props.theme.screen.xs}) {
    max-width: 540px;
  }

  @media (min-width: ${props => props.theme.screen.sm}) {
    max-width: 720px;
  }

  @media (min-width: ${props => props.theme.screen.md}) {
    max-width: 960px;
  }

  @media (min-width: ${props => props.theme.screen.lg}) {
    max-width: 1200px;
  }

  ${props =>
    props.fluid &&
    `
    max-width: 1200px !important;
  `};
`;

export const Section = styled.section`
  padding: 5%;
  overflow: hidden;

  @media (max-width: ${props => props.theme.screen.md}) {
    padding: 96px 0;
  }

  ${props =>
    props.accent &&
    `background-color: ${
      props.accent === 'secondary'
        ? props.theme.color.white.dark
        : props.theme.color.primary
    }`};
`;

const Image = styled.img`
  width: 100%;
  height: auto;
  padding-top: 12%;
`

const EcadImage = styled(Image)`
  width: 50%;
`

const TeamGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 200px);
  grid-template-rows: min-content;
  grid-gap: 50px;
  justify-content: center;
  margin-top: 5%;

  @media (max-width: ${props => props.theme.screen.lg}) {
    justify-content: center;
  }

  @media (max-width: ${props => props.theme.screen.md}) {
    width: 100%;
    grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
  }

  @media (max-width: ${props => props.theme.screen.xs}) {
    grid-gap: 24px;
  }
`;

const Title = styled.p`
  margin-top: 16px;
  color: ${props => props.theme.color.black.regular};
`;


export default Team;
