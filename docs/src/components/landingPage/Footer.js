import React from 'react';
import styled from '@emotion/styled'
import { Link } from "gatsby"
import * as Icon from "react-feather";
 
const Footer = () => (
    <FooterWrapper>
    <StyledContainer>
      <Copyright>
        <span>
          Open Source MIT License
        </span>
      </Copyright>
      <SocialIcons>
      <IconWrapper>
        <Link to="https://gitlab.com/tezgraph/tezgraph"><Icon.Gitlab size={30} /></Link>
      </IconWrapper>
      <IconWrapper>
        <Link to="https://twitter.com/tezgraph"><Icon.Twitter size={30} /></Link>
      </IconWrapper>
      <IconWrapper>
        <Link to="https://gitlab.com/tezgraph/tezgraph/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D="><Icon.MessageSquare size={30} /></Link>
      </IconWrapper>
    </SocialIcons>
    </StyledContainer>
  </FooterWrapper>
);

export const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 0 16px;
  width: 65%;
  text-align: center;

  @media (min-width: ${props => props.theme.screen.xs}) {
    max-width: 540px;
  }

  @media (min-width: ${props => props.theme.screen.sm}) {
    max-width: 720px;
  }

  @media (min-width: ${props => props.theme.screen.md}) {
    max-width: 960px;
  }

  @media (min-width: ${props => props.theme.screen.lg}) {
    max-width: 1200px;
  }

  ${props =>
    props.fluid &&
    `
    max-width: 1200px !important;
  `};
`;

const SocialIcons = styled.div`
  display: flex;
  flex-direction: row;
  justify-items: flex-end;
`;

const IconWrapper = styled.div`
margin: 0 0.75em;
`

const FooterWrapper = styled.footer`
  background-color: white;
  padding: 32px 0;
  border-top: 1px solid rgb(230,236,241);
`;

const Copyright = styled.div`
  font-family: ${props => props.theme.fonts.secondary};
  ${props => props.theme.font_size.small};
  color: ${props => props.theme.color.black.regular};

  a {
    text-decoration: none;
    color: inherit;
  }
`;

const Art = styled.figure`
  display: flex;
  justify-content: center;
  margin: 0;
  margin-top: 48px;
`;

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: ${props => props.theme.screen.sm}) {
    flex-direction: column;
    text-align: center;
  }
`;

export default Footer;
