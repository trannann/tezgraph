import React from 'react';
import styled from '@emotion/styled'

import DeveloperActivity from '../../images/art/undraw_developer_activity.svg';
import CompletedTasks from '../../images/art/undraw_completed_tasks.svg';
import Navigator from '../../images/art/undraw_navigator.svg';

const About = () => (
    <Section id="about">
        <Container>
            <Grid>
                <TextWrapper>
                    <h2>Dev Benefits</h2>
                    <P>
                        <Ul>
                            <li>Indexed blockchain data at your fingertips</li>
                            <li>Simple, discoverable and efficient API</li>
                            <li>Compatible with major programming languages</li>
                            <li>Helps build richer end-user experiences</li>
                            <li>Faster development cycle and quicker adoption</li>
                            <li>Reliable</li>
                            <li>Easy to Deploy</li>
                        </Ul>
                    </P>
                </TextWrapper>
                <Art>
                    <Image src={DeveloperActivity} alt="link" />
                </Art>
            </Grid>
            <Grid inverse>
                <Art>
                    <Image src={CompletedTasks} alt="link" />
                </Art>
                <TextWrapper>
                    <h2>Features</h2>
                    <P>
                        <Ul>
                            <li>Near real-time notifications</li>
                            <li>Efficiently query accounts and manager operations</li>
                            <li>Use out of the box Indexer node with Public API</li>
                            <li>Set up Indexer node on premise</li>
                            <li>Continous integration and delivery</li>
                            <li>Monitoring</li>
                            <li>Fail over support</li>
                        </Ul>
                    </P>
                </TextWrapper>
            </Grid>
            <Grid>
                <TextWrapper>
                    <h2>Coming Next</h2>
                    <P>
                        <Ul>
                            <li>Edo protocol compatibility</li>
                            <li>Query validation and voting operations</li>
                            <li>Bigmap support</li>
                            <li>Layer-2 indexing</li>
                            <li>Taquito integration</li>
                            <li>Indexing smart contracts</li>
                            <li>More filtering options</li>
                        </Ul>
                    </P>
                </TextWrapper>
                <Art>
                    <Image src={Navigator} alt="link" />
                </Art>
            </Grid>
        </Container>
    </Section>
);

export const Container = styled.div`
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
  padding: 0 16px;

  @media (min-width: ${props => props.theme.screen.xs}) {
    max-width: 540px;
  }

  @media (min-width: ${props => props.theme.screen.sm}) {
    max-width: 720px;
  }

  @media (min-width: ${props => props.theme.screen.md}) {
    max-width: 960px;
  }

  @media (min-width: ${props => props.theme.screen.lg}) {
    max-width: 1200px;
  }

  ${props =>
        props.fluid &&
        `
    max-width: 1200px !important;
  `};
`;

export const Section = styled.section`
  padding: 128px 0;
  overflow: hidden;

  @media (max-width: ${props => props.theme.screen.md}) {
    padding: 96px 0;
  }

  ${props =>
        props.accent &&
        `background-color: ${props.accent === 'secondary'
            ? props.theme.color.white.dark
            : props.theme.color.primary
        }`};
`;


const Image = styled.img`
width: 100%;
height: auto;
`

const TextWrapper = styled.div`
text-align: justify;

`
const Ul = styled.ul`
text-align: justify;
list-style: none;
list-style-position: outside;
padding: 0;

.b {
  list-style-type: square;
}

li::before {
  content: "• ";
  color: ${props => props.theme.colors.link};
}
`

const Grid = styled.div`
display: grid;
grid-template-columns: 3fr 2fr;
grid-gap: 40px;
text-align: right;
align-items: center;
justify-items: center;
margin: 24px 0;
padding-bottom: 10%;

${props =>
        props.inverse &&
        `
  text-align: left;
  grid-template-columns: 2fr 3fr;
`}

h2 {
  margin-bottom: 16px;
}

@media (max-width: ${props => props.theme.screen.md}) {
  grid-template-columns: 1fr;
  text-align: left;
  margin-bottom: 96px;

  &:last-child {
    margin-bottom: 24px;
  }

  ${props =>
        props.inverse &&
        `
      ${Art} {
        order: 2;
      }
  `}
}
`;

const Art = styled.figure`
margin: 0;
max-width: 380px;
width: 100%;
`;

const P = styled.div`
  font-size: 20px;
`

export default About;
