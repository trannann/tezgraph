import React from 'react';
import { Link } from "gatsby"
import styled from '@emotion/styled'
import * as Icon from "react-feather";

const NavBar = () => (
  <Nav>
    <StyledContainer>
    <Link to={`/`}><Brand>TEZGRAPH</Brand></Link>
      <RightContainer>
        <NavItem><Link to={`/quickStart`}>Quick Start</Link></NavItem>
        <NavItem><Link to={`/overview`}>Docs</Link></NavItem>
        <NavItem><Link to={`https://gitlab.com/tezgraph/tezgraph`}><Icon.Gitlab size={30} /></Link></NavItem>
      </RightContainer>
    </StyledContainer>
  </Nav>
);
export default NavBar;

const Container = styled.div`
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
  padding: 0 16px;

  @media (min-width: ${props => props.theme.screen.xs}) {
    max-width: 540px;
  }

  @media (min-width: ${props => props.theme.screen.sm}) {
    max-width: 720px;
  }

  @media (min-width: ${props => props.theme.screen.md}) {
    max-width: 960px;
  }

  @media (min-width: ${props => props.theme.screen.lg}) {
    max-width: 1200px;
  }

  ${props =>
    props.fluid &&
    `
    max-width: 1200px !important;
  `};
`;

const Nav = styled.nav`
  padding: 16px 0;
  background-color: white;
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 1000;
  border-bottom: 1px solid rgb(230,236,241);
`;

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const RightContainer = styled.div`
  display: flex;
  flex-direction: row;
  list-style: none;
  `;

const NavItem = styled.li`
  margin: 0 0.75em;
  font-family: ${props => props.theme.fonts.secondary};
  ${props => props.theme.font_size.small};

  a {
    text-decoration: none;
    opacity: 0.7;
  }

  &.active {
    a {
      opacity: 1;
    }
  }
`;

const Brand = styled.div`
  font-family: ${props => props.theme.fonts.primary};
  ${props => props.theme.font_size.large};
`;

