---
title: "Overview"
metaTitle: "This is the overview tag of this page"
metaDescription: "This is the meta description"
---

# Introduction

TezGraph is a GraphQL API that provides access to historic and real-time data from the Tezos blockchain. TezGraph is opensource (MIT), offered as a public API, and packaged to operate privately.

TezGraph gives easy access to data to casual users and developers. The API is well documented and easy to explore using the [GraphQL playground console](https://mainnet.tezgraph.tez.ie/graphql).

# Why TezGraph?

1. The ability to query historical and real-time blockchain data using Tezos RPC directly is time-consuming. There are operational considerations for applications since it takes time to traverse the blockchain, and development considerations since more in-depth knowledge of the Tezos domain are required.

Building its own indexer solution by every wallet application or dApp developer is not feasible. Current indexer solutions require ramp-up time and need client tools to be a good fit for development environments.

2. Development solutions that provide effective and simple subscriptions and notifications for near real-time new blockchain events are not as developed and hence not adopted by applications. Events notification could trigger workflows and/or business rules based on the changes to enrich end-user experiences.

Information points that require significant effort:

- account's balance
- account operations within a period
- most recent real-time operations
- smart contract execution
- paged results of the above

3. Existing Tezos indexing offerings are typically available as a SaaS and operated by a single entity. Centralized indexer infrastructure puts the risk on builders. It is possible to self-host an indexer, but the undertaking to self-host an incumbent indexer is non-trivial and introduces cost.

# Benefits

### Developer Benefits

- **Allows to provide richer end-user experiences**
- **Faster development cycle and quicker adoption due to:**
    - **Indexed blockchain data at your fingertips**
    - **Simplicity.** Straightforward API, easy to use technology, and out of the box indexed data
    - **Compatibility.** Easy to use with various technologies and applications, GraphQL has mature client libraries readily available for all major programming languages, including OCaml/Reason, Haskell, Rust, etc.
    - **Discoverability.** GraphQL makes exploration and discovery of the API and underlying data far easier than REST or RPC based APIs.
- **API efficiency** due to GraphQL's incremental delivery for state-less queries. It serves only the data properties that the client asks for. Furthermore, it supports @defer and @stream directives.

### Operational benefits

- **Reliability**
    - Co-developed by two teams
    - Public nodes are run by two independent teams (ECAD Labs and Agile Ventures), giving users of public nodes failover options
- **Easy to deploy**
    - TezGraph is easily deploy-able to cloud or on-premise infrastructure

# Features

## Functional

- Near real-time notifications on newly created head blocks, account/smart contract operations, validation and voting operations
- Ability to efficiently query information about accounts and operations (translations, delegations,  originations, reveals, and endorsements).
- Ability to use out of the box Indexer node with Public API
- Ability to set up Indexer node on premise

## Non-functional

- **Operational**:
    - GitLab CI/CD
    - Easy to deploy in a maximum of 4 steps
    - Automatic Indexer data dumps with each release and for specific branches
- **Design**:
    - Consistent terminology between Query API and Subscription API
    - Extendable design
- **Monitoring**: operations and performance
- **Failover and high availability**: AV and ECAD setup their own Indexer nodes

# Coming next

- Edo protocol compatibility
- Query validation and voting operations
- Bitmap support
- Layer-2 Indexing
- Taquito integration
- More filtering options