---
title: "Query API"
metaTitle: "This is the Query API guide tag of this page"
metaDescription: "This is the meta description"
---

### BASIC EXAMPLE

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
query {
  account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
    transactions(first: 2) {
      edges {
        cursor
        node {
          hash
          batch_position
        }
      }
    }
  }
}
</content>
```
In this example, we are querying for the `hash` and `batch_position` of the two latest (first) transaction records for the implicit account with the public key hash of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`.

The `account()` query takes in an `address` argument of type Address. This address argument refers to the public key hash of an implicit or originated account. 

Inside the account query, we are querying for transaction objects using `transactions()`. `transactions()`  can also take in arguments such as `first`, `last`, `date_range`, `order_by`, `before`, and `after`. 

By default, the TezGraph Query API returns all records in descending chronological order. This means that the first record in the list would be the latest, and the last record in the list would be the oldest. This sorting order may be modified with the argument `order_by`, which will be explained in the Advanced Example.

Note: In the case of batched operations, the timestamp may not be a unique value. However, all of the operations are indexed with an ID in chronological order. Therefore, assuming that the query uses the default sorting, the records returned will still be in descending chronological order.

Here we are using the argument `first: 2`. Since we are using the default sort order, which is in descending chronological order, it will return the 2 latest transaction records from the account. 

Inside `transactions()`, we are requesting an `edge` object. Edges consist of two fields, cursor, and node.  A `node` field is an object that consists of the operation's fields. Inside the node, the object is where we declare the operation fields that we want to see in the results. The `cursor` field is used for pagination, which will be explained in the Pagination Example.

Results:

### ADVANCED EXAMPLE

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
query {
  account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
    transactions(
      date_range: { lte: "2019-06-14T16:22:03Z", gte: "2019-06-13T16:22:03Z" },
      order_by: {field: ID, direction: ASC},
      first: 2
    ) {
      edges {
        cursor
        node {
          hash
          batch_position
          timestamp
          id
        }
      }
    }
  }
}
</content>
```

In this example, we are querying for the `hash`, `batch_position`, `timestamp`, and `id` of two transaction records with the smallest ID values for the implicit account with the public key hash `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. 

Here we are using the following arguments:

- `date_range: { lte: "2019-06-14T16:22:03Z", gte: "2019-06-13T16:22:03Z" }` - This argument will only return the records with a timestamp less than or equal to `2019-06-14T16:22:03Z` and greater than or equal to `2019-06-13T16:22:03Z`.
- `first: 2` - Since we are sorting the records by ID in ascending order, this argument will return the 2 records with the smallest ID value.
- `order_by: {field: ID, direction: ASC}` - This argument will sort the records by ID in ascending order, returning records with the oldest ID value first.

Note: In the case of batched operations, the timestamp may not have a unique value. However, all operations are assigned an incremental ID in chronological order first and then in the batch position order. Therefore, assuming that the query uses the default sorting, the records returned will still be in descending chronological order.


### PAGINATION EXAMPLE

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
query {
  account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
    transactions(
      first: 2
      after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0"
    ) {
      edges {
        cursor
        node {
          hash
          batch_position
          timestamp
          id
        }
      }
    }
  }
}
</content>
```

In this example, we are querying for the `hash`, `batch_position`, `timestamp`, and `id` of the two latest transaction records after the record with the cursor of `opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0` for the implicit account with the public key hash of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. 

The `cursor` field in the `edges` object returns a string made up of the operation's `hash` and `batch_position`. This unique string value is used for pagination. 


Using the cursor of a record, we can query for the records before or after the record the cursor references. 

To do this, we use the arguments `before` or `after`, along with the arguments `first` or `last`.

- Pagination only works with one of each arguments.

The following pagination queries will succeed:

- `first: 2, after: opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0`
- `last: 20, before: opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0`

The following pagination queries will fail:

- `first: 100, last: 100, after: opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0`
- `last: 100, before: "opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0, after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0`

In this pagination example, we are querying for the first two records after the first record in the Basic Example Results. We do this by using the following arguments:

- `after: opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0` - With this argument, the results will only return records after the record this cursor references.
- `first: 2` - Since we are using the default sort order, which is in descending chronological order, it will return the two latest transaction records after the record that the above cursor references.

In the following results, you can see that the first record is now listed second from the Basic Example Results.
