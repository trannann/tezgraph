---
title: "Subscription API"
metaTitle: "This is the Subscription API guide tag of this page"
metaDescription: "This is the meta description"
---

## Operation Subscription
 
In this example, we subscribe to all transactions in newly baked blocks. We retrieve some of their fields and also info about the block.

Similarly there are subscriptions for all operation kinds supported on Tezos.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded {
    kind
    source
    destination
    amount
    info {
      signature
    }
    block {
      hash
      header {
        level
      }
    }
  }
}
</content>
```

## Filtering

If you want to subscribe only to a particular subset of operations according to your criteria, you can leverage the `filter`.

The example demonstrates subscriptions to new transactions from all addresses except *tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS* which have `amount` greater than *7* mutez.

```gql url=https://tezgraph-ci.tezoslive.io/graphql
<content> 
subscription($excludedSource: Address, $minAmount: Mutez) {
  transactionAdded(
    filter: {
      source: { notEqualTo: $excludedSource }
      amount: { greaterThan: $minAmount }
    }
  ) {
    source
    destination
    amount
  }
}
</content> 
<variables>
{
  "excludedSource": "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS",
  "minAmount": "7"
}
</variables>
```

## Advanced Filtering

If you want to specify more advanced filters with alternative conditions then you can leverage `and` and `or` properties.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded(
    filter: {
      or: [
        { source: { equalTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" } }
        {
          source: { notEqualTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" }
          amount: { greaterThan: "7" }
        }
      ]
    }
  ) {
    source
    destination
    amount
  }
}
</content>
```

## Mempool

If you want to be notified about operations when they are put to a mempool before it gets baked into a block, then specify `includeMempool` flag.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded(includeMempool: true) {
    source
    destination
    amount
  }
}
</content>
```

## Block Subscription

You can also subscribe to entire new blocks and get operations which can be filtered too.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  blockAdded {
    hash
    header {
      level
    }
    reveals(filter: { source: { notEqualTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" } }) {
      fee
      public_key
    }
  }
}
</content>
```
