import fs from 'fs';

export async function delay(millis: number): Promise<void> {
    return new Promise(resolve => {
        setTimeout(resolve, millis);
    });
}

export async function waitUntil(checkState: () => void): Promise<void> {
    const maxWaitMillis = 3_000;
    const waitIntervalMillis = 100;

    let waitCount = 0;
    let lastError = '';

    while (waitCount++ <= (maxWaitMillis / waitIntervalMillis)) {
        await delay(waitIntervalMillis);
        try {
            checkState();
            return;
        } catch (error) {
            lastError = error.toString();
        }
    }
    throw new Error(`Condition wasn't fulfilled within ${maxWaitMillis} ms. ${lastError}`);
}

export function readFileText(filePath: string): string {
    return fs.readFileSync(filePath).toString();
}

export async function waitUntilFileContains(filePath: string, expected: string[]): Promise<void> {
    await waitUntil(() => {
        const contents = fs.readFileSync(filePath).toString();
        for (const str of expected) {
            if (!contents.includes(str)) {
                throw new Error(`Expected file ${filePath} to contain:\n\n\t${str}\n\nbut its content is:\n\n\t${contents}`);
            }
        }
    });
}
