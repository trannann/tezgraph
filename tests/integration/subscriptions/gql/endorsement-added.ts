import { gql } from 'apollo-server';

export const endorsementAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $delegate: NullableAddressFilter
) {
    endorsementAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            delegate: $delegate
        }
    ) {
        kind,
        level,
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            },
            delegate,
            slots
        }
    }
}
`;
