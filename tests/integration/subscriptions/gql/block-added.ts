import { gql } from 'apollo-server';

export const blockAdded = gql`
subscription(
    # Common parameters.
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $delegate: NullableAddressFilter,
    $originated_contract: NullableAddressArrayFilter,
    $status: NullableOperationResultStatusFilter
) {
    blockAdded {
        hash,
        protocol,
        chain_id,
        header {
            level,
            proto,
            predecessor,
            timestamp,
            signature
        },
        originations(filter: {
            # Common parameters.
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            delegate: $delegate,
            originated_contract: $originated_contract,
            status: $status
        }) {
            kind,
            source,
            fee
        }
    }
}
`;
