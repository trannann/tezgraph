import { gql } from 'apollo-server';

export const revealAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $status: NullableOperationResultStatusFilter
) {
    revealAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            status: $status
        }
    ) {
        origin,
        block {
            hash
        },
        info {
            protocol,
            chain_id,
            hash,
            branch,
            signature
        },
        kind,
        source,
        fee,
        counter,
        gas_limit,
        storage_limit,
        public_key,
        metadata {
            balance_updates {
                kind,
                delegate
            },
            internal_operation_results {
                kind,
                source,
                nonce,
                amount,
                destination
            },
            operation_result {
                status,
                consumed_gas,
                consumed_milligas,
                errors {
                    kind,
                    id
                }
            }
        }
    }
}
`;
