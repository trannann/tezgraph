import { gql } from 'apollo-server';

export const doubleEndorsementEvidenceAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $delegate: NullableAddressArrayFilter
) {
    doubleEndorsementEvidenceAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            delegate: $delegate
        }
    ) {
        kind,
        op1 {
            branch,
                operations {
                    kind,
                    level
                },
            signature
        },
        op2 {
            branch,
            operations {
                kind,
                level
            },
            signature
        },
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            }
        }
    }
}
`;
