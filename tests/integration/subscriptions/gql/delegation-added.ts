import { gql } from 'apollo-server';

export const delegationAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $delegate: NullableAddressFilter,
    $status: NullableOperationResultStatusFilter
) {
    delegationAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            delegate: $delegate,
            status: $status
        }
    ) {
        kind,
        fee,
        counter,
        gas_limit,
        storage_limit,
        delegate,
        metadata {
            balance_updates {
                kind,
                delegate
            },
            internal_operation_results {
                kind,
                source,
                nonce,
                amount,
                destination
            },
            operation_result {
                status,
                consumed_gas,
                consumed_milligas,
                errors {
                    kind,
                    id
                }
            }
        }
    }
}
`;
