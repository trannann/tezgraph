import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import fs from 'fs';
import { DocumentNode } from 'graphql';
import { PassThrough } from 'stream';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { DependencyContainer } from 'tsyringe';
import ws from 'ws';

import { App } from '../../../src/app';
import { createDIContainer } from '../../../src/dependency-injection-container';
import { CompositeModuleName } from '../../../src/modules/module';
import { tezosRpcPaths } from '../../../src/rpc/tezos-rpc-paths';
import { Names } from '../../../src/utils/configuration/env-config';
import { Logger, LoggerFactory } from '../../../src/utils/logging';
import { activateAccountAdded } from './gql/activate-account-added';
import { ballotAdded } from './gql/ballot-added';
import { blockAdded } from './gql/block-added';
import { delegationAdded } from './gql/delegation-added';
import { doubleBakingEvidenceAdded } from './gql/double-baking-evidence-added';
import { doubleEndorsementEvidenceAdded } from './gql/double-endorsement-evidence-added';
import { endorsementAdded } from './gql/endorsement-added';
import { originationAdded } from './gql/origination-added';
import { proposalsAdded } from './gql/proposals-added';
import { revealAdded } from './gql/reveal-added';
import { seedNonceRevelationAdded } from './gql/seed-nonce-revelation-added';
import { transactionAdded } from './gql/transaction-added';
import { delay, waitUntil } from './helpers';
import { rpcMocks } from './rpc-mocks';

const testEndpoint = 'ws://localhost:3000/graphql';

describe('Apollo GraphQL Subscriptions', () => {
    let app: App;
    let wsClient: SubscriptionClient;
    let apolloClient: ApolloClient<NormalizedCacheObject>;
    let blockMonitorStream: PassThrough;
    let mempoolMonitorStream: PassThrough;
    let logger: Logger;
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = createDIContainer({
            ...process.env,
            [Names.Modules]: CompositeModuleName.SubscriptionsGraphQL,
            [Names.Port]: '3000',
        });
    });

    beforeEach(async () => {
        logger = diContainer.resolve(LoggerFactory).getLogger('TestRunner');
        logger.logInformation('Testing with {environmentVariables}.', { environmentVariables: process.env });

        app = diContainer.resolve(App);

        // Mock RPC responses
        blockMonitorStream = new PassThrough();
        mempoolMonitorStream = new PassThrough();
        rpcMocks.setup({
            urlPath: tezosRpcPaths.blockMonitor,
            responseBody: blockMonitorStream,
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.block('BLBiBqzhMU1z5VpcTxHZc1wwwvU92LE4YMU96vTfiQSSbVHCqss'),
            responseBody: readTestDataFile('block.json'),
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.mempoolMonitor,
            responseBody: mempoolMonitorStream,
        });

        await app.start();

        // Initialize websockets test client
        wsClient = new SubscriptionClient(
            testEndpoint,
            { reconnect: true },
            ws,
        );

        apolloClient = new ApolloClient({
            link: new WebSocketLink(wsClient),
            cache: new InMemoryCache(),
        });
    });

    it('should fetch RPC monitor head and corresponding block, subscribe to Apollo websockets and get corresponding notifications', async () => {
        const endorsementFilter = {
            delegate: {
                in: ['tz1e7uhpwmiKp8Yd2KwFwmVhoT31L478KUe3', 'tz1VxS7ff4YnZRs8b4mMP4WaMVpoQjuo1rjf'],
                notIn: ['tz1NRTQeqcuwybgrZfJavBY3of83u8uLpFBj', 'tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3'],
                notEqualTo: 'tz1TEZtYnuLiZLdA6c7JysAUJcHMrogu4Cpr',
            },
        };

        const originationFilter = {
            delegate: { isNull: true },
            originated_contract: { isNull: false, includes: 'tz1X6MxebHJ4DLu6VAcEaCcqn6JoPg6tqrWW' },
        };

        const transactionFilter = {
            source: {
                equalTo: 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7',
                notEqualTo: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                in: ['tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7', 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'],
            },
            destination: { notIn: ['tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7'] },
            status: { notEqualTo: 'applied', notIn: ['backtracked', 'skipped'] },
            amount: { greaterThan: 49400000 },
        };

        const revealFromMempoolFilter = {
            includeMempool: true,
            status: { isNull: true }, // Filters out reveals from block.
        };

        const subscriptionTests = [
            setupSubscriptionToTest('block', blockAdded, originationFilter),
            setupSubscriptionToTest('activate-account', activateAccountAdded),
            setupSubscriptionToTest('ballot', ballotAdded),
            setupSubscriptionToTest('delegation', delegationAdded),
            setupSubscriptionToTest('double-baking-evidence', doubleBakingEvidenceAdded),
            setupSubscriptionToTest('double-endorsement-evidence', doubleEndorsementEvidenceAdded),
            setupSubscriptionToTest('endorsement', endorsementAdded),
            setupSubscriptionToTest('endorsement-filtered', endorsementAdded, endorsementFilter),
            setupSubscriptionToTest('origination', originationAdded),
            setupSubscriptionToTest('origination-filtered', originationAdded, originationFilter),
            setupSubscriptionToTest('proposals', proposalsAdded),
            setupSubscriptionToTest('reveal', revealAdded),
            setupSubscriptionToTest('reveal-from-mempool', revealAdded, revealFromMempoolFilter),
            setupSubscriptionToTest('seed-nonce-revelation', seedNonceRevelationAdded),
            setupSubscriptionToTest('transaction', transactionAdded),
            setupSubscriptionToTest('transaction-filtered', transactionAdded, transactionFilter),
        ];

        // Let the app with workers start before simulating new monitor block header chunk.
        await delay(500);

        // Tezos monitor is listening to passed stream, waiting for chunk push.
        blockMonitorStream.push(readTestDataFile('block-monitor.json'));
        mempoolMonitorStream.push(readTestDataFile('mempool-monitor.json'));

        // Wait for app to receive, process and publish all notifications. Then assert.
        await Promise.all(subscriptionTests.map(async test => waitUntil(
            () => {
                try {
                    expect(test.receivedNotifications).toHaveLength(test.expectedNotifications.length);
                    expect(test.receivedNotifications).toEqual(test.expectedNotifications);
                } catch (error: unknown) {
                    throw new Error(`Test '${test.name}' didn't receive data as expected. ${(error as Error).toString()}`);
                }
            },
        )));

        logger.logInformation('Unsubscribing');
        subscriptionTests.forEach(t => t.subscription.unsubscribe());
    });

    afterEach(() => {
        logger.logInformation('Stopping client');
        blockMonitorStream.destroy();
        mempoolMonitorStream.destroy();
        apolloClient.stop();
        wsClient.unsubscribeAll();
        app.stop();
        rpcMocks.cleanAll();
    });

    function setupSubscriptionToTest(
        fileNameWithExpected: string,
        query: DocumentNode,
        filter?: Record<string, unknown>,
    ) {
        const expectedNotifications = JSON.parse(readTestDataFile(`expected/${fileNameWithExpected}-notifications.json`)) as unknown[];
        const receivedNotifications: unknown[] = [];

        const subscription = apolloClient
            .subscribe({
                query,
                variables: filter,
            })
            .subscribe({
                next: d => receivedNotifications.push(d),
            });

        return { name: fileNameWithExpected, subscription, receivedNotifications, expectedNotifications };
    }
});

function readTestDataFile(fileName: string): string {
    return fs.readFileSync(`./tests/integration/subscriptions/test-data/${fileName}`).toString();
}
