import nock, { Body } from 'nock';

import { Names } from '../../../src/utils/configuration/env-config';

interface RpcMock {
    urlPath: string;
    responseBody: Body;
}

export const rpcMocks = {
    setup: (mock: RpcMock) => {
        nock(process.env[Names.TezosNode]!)
            .persist()
            .get(mock.urlPath)
            .delayConnection(50)
            .delayBody(50)
            .reply(200, mock.responseBody);
    },

    cleanAll: () => {
        nock.abortPendingRequests();
        nock.cleanAll();
    },
};
