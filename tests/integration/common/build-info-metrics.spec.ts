import supertest from 'supertest';

import { App } from '../../../src/app';
import { createDIContainer } from '../../../src/dependency-injection-container';
import { VersionVariables } from '../../../src/utils/app-version/version-provider';

describe('Prometheus "tezgraph_build_info" metric', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;

    beforeAll(async () => {
        const diContainer = createDIContainer({
            ...process.env,
            [VersionVariables.GitSha]: 'ee323ca7eaab5e317f54c66c4478cdbe8ffd5b73',
            [VersionVariables.GitTag]: 'v0.8.0-beta.1',
        });
        app = diContainer.resolve(App);
        await app.start();
        request = supertest(app.httpServer);
    });

    afterAll(async () => {
        await app.stop();
    });

    it('should expose metric value after bootstrap', async () => {
        const res = await request.get('/metrics');

        expect(res.status).toBe(200);
        expect(res.text).toInclude('tezgraph_build_info{git_sha="ee323ca7eaab5e317f54c66c4478cdbe8ffd5b73",tag="v0.8.0-beta.1"} 1');
    });
});
