/* eslint-disable capitalized-comments */
import { Registry } from 'prom-client';
import supertest from 'supertest';
import { Query, Resolver } from 'type-graphql';

import { App } from '../../../src/app';
import { graphQLResolversDIToken } from '../../../src/bootstrap/graphql-resolver-type';
import { createDIContainer } from '../../../src/dependency-injection-container';

describe('Prometheus metrics endpoints', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;
    let promClientRegistry: Registry;

    beforeAll(async () => {
        @Resolver()
        class TestResolver {
            @Query()
            firstQuery(): boolean {
                return true;
            }

            @Query()
            secondQuery(): boolean {
                return true;
            }

            @Query()
            errorQuery(): boolean {
                throw new Error('Test error');
            }
        }

        const diContainer = createDIContainer(process.env);
        diContainer.registerInstance(graphQLResolversDIToken, TestResolver);

        app = diContainer.resolve(App);
        promClientRegistry = diContainer.resolve(Registry);
        await app.start();
        request = supertest(app.httpServer);
    });

    beforeEach(() => {
        promClientRegistry.resetMetrics();
    });

    afterAll(async () => {
        await app.stop();
    });

    const sendFirstQueryGraphQLRequest = async () => {
        const res = await request.post('/graphql').send({
            query: /* graphql */ `
                query {
                    firstQuery
                }
            `,
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeUndefined();
        expect(res.body.data.firstQuery).toEqual(true);
    };
    const sendSecondQueryGraphQLRequest = async () => {
        const res = await request.post('/graphql').send({
            query: /* graphql */ `
                query {
                    secondQuery
                }
            `,
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeUndefined();
        expect(res.body.data.secondQuery).toEqual(true);
    };
    const sendErrorQueryGraphQLRequest = async () => {
        const res = await request.post('/graphql').send({
            query: /* graphql */ `
                query {
                    errorQuery
                }
            `,
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeDefined();
    };

    describe('graphql_query_method_total', () => {
        it('should correctly return metric for single graphql call', async () => {
            await sendFirstQueryGraphQLRequest();

            const res = await request.get('/metrics');

            expect(res.status).toBe(200);
            expect(res.text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 1');
        });

        it('should correctly count subsequent graphql calls', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendFirstQueryGraphQLRequest();

            const res = await request.get('/metrics');

            expect(res.status).toBe(200);
            expect(res.text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 2');
        });

        it('should correctly return metric for different graphql calls', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendSecondQueryGraphQLRequest();
            await sendSecondQueryGraphQLRequest();

            const res = await request.get('/metrics');

            expect(res.status).toBe(200);
            expect(res.text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 1');
            expect(res.text).toInclude('graphql_query_method_total{method="secondQuery",service="indexer"} 2');
        });
    });

    describe('graphql_resolver_error_total', () => {
        it('should correctly return error metric value when graphql calls failed', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendErrorQueryGraphQLRequest();
            await sendErrorQueryGraphQLRequest();

            const res = await request.get('/metrics');

            expect(res.status).toBe(200);
            expect(res.text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 1');
            expect(res.text).toInclude('graphql_query_method_total{method="errorQuery",service="indexer"} 2');
            expect(res.text).toInclude('graphql_resolver_error_total{type="Query",field="errorQuery",service="indexer"} 2');
        });
    });
});
