import supertest from 'supertest';

import { App } from '../../../src/app';
import { createDIContainer } from '../../../src/dependency-injection-container';

describe('Redirection from "/" to "/graphql"', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;

    beforeAll(async () => {
        app = createDIContainer(process.env).resolve(App);
        await app.start();
        request = supertest(app.httpServer);
    });

    it('should redirect to "/graphql" on "/"', async () => {
        await request.get('/')
            .expect(302)
            .expect('Location', '/graphql');
    });

    afterAll(async () => {
        await app.stop();
    });
});
