import { gql } from 'apollo-server';
import { ApolloServer } from 'apollo-server-express';
import { createTestClient } from 'apollo-server-testing';
import { DocumentNode } from 'graphql';

import { ApolloServerFactory } from '../../../../src/bootstrap/apollo-server-factory';
import { createDIContainer } from '../../../../src/dependency-injection-container';
import { ModuleName } from '../../../../src/modules/module';
import { DateRange } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { Names } from '../../../../src/utils/configuration/env-config';

export const ID = 'ID';
export const ASC = 'ASC';

export function buildTestQuery(fieldResolver: string) {
    return gql`
        query AccountQuery(
            $address: Address!
            $first: Int
            $last: Int
            $before: Cursor
            $after: Cursor
            $order_by: OrderBy
            $date_range: DateRange
        ) {
            account(address: $address) {
                ${fieldResolver}(
                    first: $first
                    last: $last
                    before: $before
                    after: $after
                    order_by: $order_by
                    date_range: $date_range
                ) {
                    edges {
                        node {
                            hash
                            batch_position
                            timestamp
                            id
                        }
                    }
                }
            }
        }`;
}

export interface TestQueryOptionsVariables {
    address: string;
    first?: number;
    last?: number;
    before?: string;
    after?: string;
    order_by?: { field: string; direction: string };
    date_range?: DateRange;
}

export interface TestQueryOptions {
    query: DocumentNode;
    variables: TestQueryOptionsVariables;
    errorPolicy: string;
}

interface TestUtils {
    executeTestQuery(operation: string, variables?: TestQueryOptionsVariables, customQuery?: DocumentNode): Promise<any>;
}

export function createTestGraphQLClientUtils() {
    const diContainer = createDIContainer({
        ...process.env,
        [Names.Modules]: ModuleName.QueriesGraphQL,
    });
    const apolloServerFactory = diContainer.resolve(ApolloServerFactory);
    let executeQuery: (query: any) => Promise<any>;
    let apolloServer: ApolloServer;
    const testUtils = {} as TestUtils;

    beforeAll(async () => {
        apolloServer = await apolloServerFactory.create();
        const testClient = createTestClient(apolloServer);
        executeQuery = testClient.query.bind(testClient);

        async function executeTestQuery(operation: string, variables: TestQueryOptionsVariables, customQuery?: DocumentNode) {
            const queryResults = await executeQuery({
                query: customQuery ? customQuery : buildTestQuery(operation),
                variables: {
                    ...variables,
                },
                errorPolicy: 'all',
            });
            return queryResults;
        }

        testUtils.executeTestQuery = executeTestQuery;
    });

    afterAll(async () => {
        await apolloServer.stop();
    });

    return testUtils;
}
