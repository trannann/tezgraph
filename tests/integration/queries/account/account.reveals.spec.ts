/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { createTestGraphQLClientUtils } from './account.utils';

describe('Account Resolvers', () => {
    const testUtils = createTestGraphQLClientUtils();

    it('should return an account.reveals record with all of the reveal fields', async () => {
        const testQuery = gql`
        query AccountQuery(
            $address: Address!
            $last: Int
        ) {
            account(address: $address) {
                address
                    first_seen
                    activated
                    public_key
                    reveals (
                        last: $last
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source
                                kind
                                timestamp
                                level
                                block
                                fee
                                counter
                                gas_limit
                                public_key
                                id
                            }
                        }
                    }
                }
            }
        `;

        const res = await testUtils.executeTestQuery('reveals', {
            address: 'tz1a3Gs9whDXUAVzKSbJ3abYaEUxhQm1wdb1',
            last: 1,
        }, testQuery);
        expect(res.data).toMatchSnapshot();
    });
});
