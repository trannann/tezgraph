import type { Config } from '@jest/types';

import defaultConfig from './base.config';

const config: Config.InitialOptions = {
    ...defaultConfig,
    testMatch: ['<rootDir>/tests/integration/common/**/*.spec.ts'],
};

export default config;
