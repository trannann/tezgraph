import express from 'express';
import { Registry } from 'prom-client';
import { instance, mock, spy, verify, when } from 'ts-mockito';

import { HttpHeader } from '../../../../src/utils/http-constants';
import { MetricsHttpHandler } from '../../../../src/utils/metrics/metrics-http-handler';

describe(MetricsHttpHandler.name, () => {
    let target: MetricsHttpHandler;
    let prometheusRegistry: Registry;
    let response: express.Response;

    beforeEach(() => {
        prometheusRegistry = mock(Registry);
        target = new MetricsHttpHandler(instance(prometheusRegistry));

        response = mock<express.Response>();
    });

    it('should define correct routes', () => {
        const targetSpy = spy(target);

        expect(target.routes).toHaveLength(1);
        expect(target.routes[0]?.path).toBe('/metrics');

        target.routes[0]?.handler(response, null!);
        verify(targetSpy.getMetrics(response));
    });

    it('should send metrics to HTTP response', async () => {
        when(prometheusRegistry.metrics()).thenResolve('lol omg');
        when(prometheusRegistry.contentType).thenReturn('funny/memes');

        await target.getMetrics(instance(response));

        verify(response.setHeader(HttpHeader.ContentType, 'funny/memes'));
        verify(response.send('lol omg'));
    });
});
