import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, verify } from 'ts-mockito';
import { NextFn, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../../../src/bootstrap/resolver-context';
import { GraphQLErrorCounterLabel, GraphQLQueryMethodLabel, MetricsContainer } from '../../../../src/utils/metrics/metrics-container';
import { MetricsMiddleware, SERVICE_NAME } from '../../../../src/utils/metrics/metrics-middleware';
import { expectToThrowAsync } from '../../mocks';

describe(MetricsMiddleware.name, () => {
    let target: MetricsMiddleware;
    let graphQLQueryMethodCounter: Counter<GraphQLQueryMethodLabel>;
    let graphQLErrorCounter: Counter<GraphQLErrorCounterLabel>;
    let metrics: MetricsContainer;

    let resolverData: ResolverData<ResolverContext>;
    let next: NextFn;

    const act = async () => target.use(resolverData, next);

    beforeEach(() => {
        graphQLQueryMethodCounter = mock(Counter);
        graphQLErrorCounter = mock(Counter);
        metrics = {
            graphQLQueryMethodCounter: instance(graphQLQueryMethodCounter),
            graphQLErrorCounter: instance(graphQLErrorCounter),
        } as MetricsContainer;
        target = new MetricsMiddleware(metrics);

        resolverData = {
            info: {
                fieldName: 'transactions',
                parentType: { name: 'Explosion' },
            },
        } as ResolverData<ResolverContext>;
        next = async () => Promise.resolve('resultData');
    });

    it('should not increment query counter if not query', async () => {
        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLQueryMethodCounter.inc(anything())).never();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it.each(['Query', 'Mutation'])('should increment query counter if %s', async (type) => {
        resolverData.info.parentType.name = type;

        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLQueryMethodCounter.inc(anything())).once();
        verify(graphQLQueryMethodCounter.inc(deepEqual({ method: 'transactions', service: SERVICE_NAME }))).once();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should increment error counter if next throws', async () => {
        const nextError = new Error('oups');
        next = async () => Promise.reject(nextError);

        const error = await expectToThrowAsync(act);

        expect(error).toBe(nextError);
        verify(graphQLErrorCounter.inc(anything())).once();
        verify(graphQLErrorCounter.inc(deepEqual({ type: 'Explosion', field: 'transactions', service: SERVICE_NAME }))).once();
    });
});
