import { instance, mock, verify, when } from 'ts-mockito';

import { Lazy } from '../../../src/utils/lazy';

interface FooFactory {
    execute(): string;
}

describe(Lazy.name, () => {
    let target: Lazy<string>;
    let factory: FooFactory;

    beforeEach(() => {
        factory = mock<FooFactory>();
        target = new Lazy(() => instance(factory).execute());

        when(factory.execute()).thenReturn('foo');
    });

    it('should return value from factory and cache it', () => {
        for (let i = 0; i < 5; i++) {
            expect(target.value).toBe('foo');
        }

        verify(factory.execute()).once();
    });

    it('should not execute factory when only constructed', () => {
        verify(factory.execute()).never();
    });
});
