import { instance, mock, verify, when } from 'ts-mockito';

import { EnvConfig } from '../../../../src/utils/configuration/env-config';
import { CachedHealthCheck } from '../../../../src/utils/health/cached-health-check';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../../../../src/utils/health/health-check';
import { nameof } from '../../../../src/utils/reflection';
import { getRandomDate, TestClock } from '../../mocks/test-clock';

describe(CachedHealthCheck.name, () => {
    let target: CachedHealthCheck;
    let innerCheck: HealthCheck;
    let envConfig: EnvConfig;
    let clock: TestClock;

    beforeEach(() => {
        innerCheck = mock<HealthCheck>();
        envConfig = { healthStatusCacheTtlSeconds: 12 } as EnvConfig;
        clock = new TestClock();
        target = new CachedHealthCheck(instance(innerCheck), envConfig, clock);
    });

    describe(nameof<CachedHealthCheck>('name'), () => {
        it('should expose value from inner check', () => {
            when(innerCheck.name).thenReturn('Foo');

            expect(target.name).toBe('Foo');
        });
    });

    describe(nameof<CachedHealthCheck>('checkHealth'), () => {
        it('should return fresh result from inner check if nothing cached', async () => {
            const innerResult = mockResult();
            when(innerCheck.checkHealth()).thenReturn(Promise.resolve(innerResult));

            const result = await target.checkHealth();

            expect(result).toEqual({
                ...innerResult,
                evaluatedOn: clock.nowDate,
            });
        });

        it('should return cached result if not expired', async () => {
            when(innerCheck.checkHealth()).thenReturn(Promise.resolve(mockResult()));
            const initialResult = await target.checkHealth();
            clock.tick((envConfig.healthStatusCacheTtlSeconds - 1) * 1_000);

            const result = await target.checkHealth();

            expect(result).toBe(initialResult);
            verify(innerCheck.checkHealth()).once();
        });

        it('should return fresh result from inner check if cached result expired', async () => {
            const secondResult = mockResult();
            when(innerCheck.checkHealth())
                .thenReturn(Promise.resolve(mockResult()))
                .thenReturn(Promise.resolve(secondResult));
            await target.checkHealth();
            clock.tick((envConfig.healthStatusCacheTtlSeconds + 1) * 1_000);

            const result = await target.checkHealth();

            expect(result).toEqual({
                ...secondResult,
                evaluatedOn: clock.nowDate,
            });
            verify(innerCheck.checkHealth()).twice();
        });
    });

    function mockResult(): HealthCheckResult {
        return {
            status: HealthStatus.Degraded,
            data: { value: Math.random() },
            evaluatedOn: getRandomDate(),
        };
    }
});
