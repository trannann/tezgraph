import express from 'express';
import { instance, mock, spy, verify, when } from 'ts-mockito';

import { HealthHttpHandler } from '../../../../src/utils/health/health-http-handler';
import { HealthProvider, HealthReport } from '../../../../src/utils/health/health-provider';
import { ContentType, HttpHeader, HttpStatusCode } from '../../../../src/utils/http-constants';
import { nameof } from '../../../../src/utils/reflection';

describe(HealthHttpHandler.name, () => {
    let target: HealthHttpHandler;
    let healthProvider: HealthProvider;
    let response: express.Response;

    beforeEach(() => {
        healthProvider = mock(HealthProvider);
        response = mock<express.Response>();
        target = new HealthHttpHandler(instance(healthProvider));
    });

    describe(nameof<HealthHttpHandler>('routes'), () => {
        it('should define correct routes', () => {
            const targetSpy = spy(target);
            expect(target.routes).toHaveLength(2);

            expect(target.routes[0]?.path).toBe('/health');
            target.routes[0]?.handler(response, null!);
            verify(targetSpy.getHealthStatus(response));

            expect(target.routes[1]?.path).toBe('/check');
            target.routes[1]?.handler(response, null!);
            verify(targetSpy.getHealthCheck(response));
        });
    });

    describe('http handlers', () => {
        afterEach(() => {
            verify(healthProvider.generateHealthReport()).once();
        });

        describe(nameof<HealthHttpHandler>('getHealthStatus'), () => {
            it(`should call generateHealthReport and send response with status`, async () => {
                const report = setupHealthReport(true);

                await target.getHealthStatus(instance(response));

                verify(response.setHeader(HttpHeader.ContentType, ContentType.Json)).once();
                verify(response.send(JSON.stringify(report, null, 2))).once();
            });
        });

        describe(nameof<HealthHttpHandler>('getHealthCheck'), () => {
            it(`should call generateHealthReport and return "OK" with code 200`, async () => {
                setupHealthReport(true);

                await target.getHealthCheck(instance(response));

                verify(response.send('OK')).once();
            });

            it(`should call generateHealthReport and return "failed" with code 500`, async () => {
                setupHealthReport(false);

                await target.getHealthCheck(instance(response));

                verify(response.sendStatus(HttpStatusCode.InternalServerError)).once();
            });
        });

        describe(nameof<HealthHttpHandler>('checkIsHealthy'), () => {
            it(`should call health provider for IsHealthy and not throw error`, async () => {
                setupHealthReport(true);

                await target.checkIsHealthy();
            });

            it(`should call health provider for IsHealthy and throw error`, async () => {
                setupHealthReport(false);

                const act = async () => target.checkIsHealthy();

                await expect(act()).rejects.toThrow();
            });
        });
    });

    function setupHealthReport(isHealthy: boolean) {
        const report = { isHealthy, evaluatedOn: new Date() } as HealthReport;
        when(healthProvider.generateHealthReport()).thenResolve(report);
        return report;
    }
});
