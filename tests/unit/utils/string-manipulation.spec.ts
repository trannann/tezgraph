import { isWhiteSpace, joinQuoted, removeRequiredSuffix } from '../../../src/utils/string-manipulation';
import { expectToThrow } from '../mocks';

describe('String manipulation utils', () => {
    describe(removeRequiredSuffix.name, () => {
        it('should remove suffix correctly', () => {
            const result = removeRequiredSuffix('Hello world', ' world');

            expect(result).toBe('Hello');
        });

        it(`should throw if input doesn't end with the suffix`, () => {
            const error = expectToThrow(() => removeRequiredSuffix('Hello world', 'not suffix'));

            expect(error.message).toIncludeMultiple(['"Hello world"', '"not suffix"']);
        });
    });

    describe(isWhiteSpace.name, () => {
        it.each([
            ['undefined', true, undefined],
            ['null', true, null],
            ['empty', true, ''],
            ['spaces', true, '  '],
            ['tabs', true, '\t'],
            ['text', false, 'abc'],
            ['text with white-spaces', false, 'a b\tc'],
        ])('should take %s and return %s', (_desc, expected, input) => {
            expect(isWhiteSpace(input)).toBe(expected);
        });
    });

    describe(joinQuoted.name, () => {
        it('should quote and join strings', () => {
            const str = joinQuoted(['a', '', 'b']);

            expect(str).toBe(`'a', '', 'b'`);
        });
    });
});
