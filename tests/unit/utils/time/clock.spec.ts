import { nameof } from '../../../../src/utils/reflection';
import { Clock } from '../../../../src/utils/time/clock';

describe(Clock.name, () => {
    const target = new Clock();

    describe(nameof<Clock>('getNowDate'), () => {
        it('should get current Date', () => {
            const startTimestamp = Date.now();

            const date = target.getNowDate();

            expect(date.getTime()).toBeGreaterThanOrEqual(startTimestamp);
            expect(date.getTime()).toBeLessThanOrEqual(Date.now());
        });
    });
});
