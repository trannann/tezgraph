import { instance, mock, when } from 'ts-mockito';

import { Version } from '../../../../src/utils/app-version/version';
import { VersionGraphQLResolver } from '../../../../src/utils/app-version/version-graphql-resolver';
import { VersionProvider } from '../../../../src/utils/app-version/version-provider';

describe(VersionGraphQLResolver.name, () => {
    let target: VersionGraphQLResolver;
    let versionProvider: VersionProvider;

    beforeEach(() => {
        versionProvider = mock(VersionProvider);
        target = new VersionGraphQLResolver(instance(versionProvider));
    });

    it('should return version from provider', () => {
        const testVersion = {} as Version;
        when(versionProvider.version).thenReturn(testVersion);

        const version = target.version();

        expect(version).toBe(testVersion);
    });
});
