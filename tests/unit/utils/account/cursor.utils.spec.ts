/* eslint-disable max-lines */
import { PrismaClient } from '@prisma/client';
import { mock } from 'ts-mockito';

import CursorUtils from '../../../../src/modules/queries-graphql/repositories/cursor.utils';
import { DateRange } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { OrderBy, OrderByField, OrderByDirection } from '../../../../src/modules/queries-graphql/repositories/order-by.utils';
import { RelayPageData } from '../../../../src/modules/queries-graphql/utils';
import { Logger } from '../../../../src/utils/logging';

describe('Account Cursor Utils', () => {
    const prisma = mock<PrismaClient>();
    const logger = mock<Logger>();
    const cursorUtils: CursorUtils = new CursorUtils(prisma, logger);
    it('should return a "<"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'before',
        };

        const res = cursorUtils.getRelayDirection(orderBy, relayPageData);
        expect(res).toMatchSnapshot();
    });

    it('should return a ">"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'first',
            order: 'before',
        };

        const res = cursorUtils.getRelayDirection(orderBy, relayPageData);
        expect(res).toMatchSnapshot();
    });

    it('should return "DESC"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'before',
        };

        const res = cursorUtils.getOrderByDirection(orderBy, relayPageData);
        expect(res).toMatchSnapshot();
    });

    it('should return "ASC"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'after',
        };

        const res = cursorUtils.getOrderByDirection(orderBy, relayPageData);
        expect(res).toMatchSnapshot();
    });

    it('should return a full SQL query', () => {
        const operation = 'transaction';
        const pkh = 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS';
        const selectStatement = `SELECT id`;

        const orderByClause1 = 'ORDER BY id DESC, hash ASC, op_id ASC';
        const dateRange1 = undefined;
        // eslint-disable-next-line max-len
        const orderByWhereClause1 = `WHERE (((id <= '25134187') AND (hash >= 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1' AND op_id > '0')) OR ((id <= '25134187') AND (hash != 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1')))`;
        const res1 = cursorUtils.getCursorQuery(operation, pkh, orderByClause1, selectStatement, dateRange1, orderByWhereClause1);
        expect(res1).toMatchSnapshot();

        const orderByClause2 = 'ORDER BY id ASC, hash ASC, op_id ASC';
        const dateRange2: DateRange = { gte: '2020-07-17T19:46:24', lte: '2020-07-17T19:50:24' };
        // eslint-disable-next-line max-len
        const orderByWhereClause2 = `WHERE (((id <= '25210415') AND (hash >= 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S' AND op_id > '0')) OR ((id <= '25210415') AND (hash != 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S')))`;
        const res2 = cursorUtils.getCursorQuery(operation, pkh, orderByClause2, selectStatement, dateRange2, orderByWhereClause2);
        expect(res2).toMatchSnapshot();
    });
});
