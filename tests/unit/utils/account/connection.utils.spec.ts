/* eslint-disable max-lines */
import { PrismaClient } from '@prisma/client';
import { mock } from 'ts-mockito';

import { PageInfo } from '../../../../src/entity/pageInfo';
import { TransactionEdge } from '../../../../src/entity/transaction';
import ConnectionUtils from '../../../../src/modules/queries-graphql/repositories/connection.utils';
import CursorUtils from '../../../../src/modules/queries-graphql/repositories/cursor.utils';
import { OrderBy, OrderByField, OrderByDirection } from '../../../../src/modules/queries-graphql/repositories/order-by.utils';
import { createRelayPageData, RelayPageData } from '../../../../src/modules/queries-graphql/utils';
import { Logger } from '../../../../src/utils/logging';

describe('Account Connection Utils', () => {
    const prisma = mock<PrismaClient>();
    const logger = mock<Logger>();
    const cursorUtils: CursorUtils = new CursorUtils(prisma, logger);
    const connectionUtils: ConnectionUtils = new ConnectionUtils(prisma, cursorUtils, logger);

    it('should return a PageInfo object with a start_cursor, end_cursor, has_next_page, and !has_previous_page', () => {
        const res = connectionUtils.pageInfoBuilder(
            'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
            'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
            true,
            false,
        );
        expect(res).toMatchSnapshot();
    });

    it('should return an RelayEdge array with 2 edges in the array each contain a cursor string and a node object', () => {
        const operations = [
            {
                id: 1427150,
                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                batch_position: 1,
                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 102031,
                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                counter: '19869',
                gas_limit: '0',
                storage_limit: '0',
                entrypoint: null,
                fee: 0,
                amount: 1000000,
                parameters: null,
                timestamp: '2018-09-13T16:12:50.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            },
            {
                id: 1425714,
                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                batch_position: 0,
                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 101957,
                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                counter: '59234',
                gas_limit: '200',
                storage_limit: '0',
                entrypoint: null,
                fee: 10,
                amount: 14,
                parameters: null,
                timestamp: '2018-09-13T14:51:20.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            },
        ];
        const res = connectionUtils.edgesBuilder(operations, 'transaction');
        expect(res).toMatchSnapshot();
    });

    it('should return an RelayConnection(transaction) object with edges and page_info', () => {
        const operationEdges: TransactionEdge[] = [
            {
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                node:
                {
                    id: 1427150,
                    hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                    batch_position: 1,
                    source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                    destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                    kind: 'transaction',
                    level: 102031,
                    block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                    counter: 19869 as unknown as bigint,
                    gas_limit: 0 as unknown as bigint,
                    storage_limit: 0 as unknown as bigint,
                    entrypoint: null,
                    fee: 0 as unknown as bigint,
                    amount: 1000000 as unknown as bigint,
                    parameters: null,
                    timestamp: '2018-09-13T16:12:50.000Z' as unknown as Date,
                    contract_address: null,
                    consumed_milligas: 100000 as unknown as bigint,
                },
            },
            {
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                node:
                {
                    id: 1425714,
                    hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                    batch_position: 0,
                    source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                    destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                    kind: 'transaction',
                    level: 101957,
                    block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                    counter: 59234 as unknown as bigint,
                    gas_limit: 200 as unknown as bigint,
                    storage_limit: 0 as unknown as bigint,
                    entrypoint: null,
                    fee: 10 as unknown as bigint,
                    amount: 14 as unknown as bigint,
                    parameters: null,
                    timestamp: '2018-09-13T14:51:20.000Z' as unknown as Date,
                    contract_address: null,
                    consumed_milligas: 100000 as unknown as bigint,
                },
            },
        ];
        const pageInfo: PageInfo = {
            start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            has_next_page: true,
            has_previous_page: false,
        };
        const res = connectionUtils.connectionBuilder('transaction', operationEdges, pageInfo);
        expect(res).toMatchSnapshot();
    });

    it('should return an error due to invalid cursor format in getConnection()', async () => {
        const operations = [
            {
                id: 1427150,
                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                batch_position: 1,
                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 102031,
                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                counter: '19869',
                gas_limit: '0',
                storage_limit: '0',
                entrypoint: null,
                fee: 0,
                amount: 1000000,
                parameters: null,
                timestamp: '2018-09-13T16:12:50.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:AA',
            },
            {
                id: 1425714,
                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                batch_position: 0,
                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 101957,
                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                counter: '59234',
                gas_limit: '200',
                storage_limit: '0',
                entrypoint: null,
                fee: 10,
                amount: 14,
                parameters: null,
                timestamp: '2018-09-13T14:51:20.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6j:0',
            },
        ];
        const pageInfoRelayPageData = createRelayPageData(undefined, undefined, 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENRdio34:0', undefined);
        let error;
        try {
            await connectionUtils.getConnection('transaction', operations, pageInfoRelayPageData, 'pkh', 'uuid', undefined, undefined);
        } catch (err) {
            error = err;
        }
        // eslint-disable-next-line max-len
        expect(error).toEqual(new Error('The start cursor (oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:AA) and/or end cursor(opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6j:0) retrieved from the Account Query results in getConnection() are not in a valid cursor format.'));
    });

    it('should return a SQL order by clause string', () => {
        const relayPageData1: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const orderBy1: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };
        const res1 = connectionUtils.getOrderByClause(relayPageData1, orderBy1);
        expect(res1).toMatchSnapshot();

        const relayPageData2: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const orderBy2: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.ASC,
        };
        const res2 = connectionUtils.getOrderByClause(relayPageData2, orderBy2);
        expect(res2).toMatchSnapshot();
    });

    it('should return a SQL where clause string that reflects order by data', () => {
        const cursorRecord1 = [
            {
                kind: 'transaction',
                id: 25134187,
                level: 1041412,
                timestamp: '2020-07-15T20:57:27+00:00',
                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                fee: 1284,
                counter: '119008',
                gas_limit: '10307',
                storage_limit: '0',
                op_id: 0,
                public_key: null,
                amount: 188397246,
                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                parameters: '{ "prim": "Unit" }',
                slots: null,
                entrypoint: 'default',
                contract_address: null,
                delegate: null,
                consumed_milligas: 10207000,
            },
        ];
        const relayPageData1: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const relayAfter1 = true;
        const orderBy1: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };
        const res1 = connectionUtils.getOrderByWhereClause(cursorRecord1, relayPageData1, relayAfter1, orderBy1);
        expect(res1).toMatchSnapshot();

        const cursorRecord2 = [
            {
                kind: 'transaction',
                id: 25210415,
                level: 1044207,
                timestamp: '2020-07-17T19:46:24+00:00',
                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                fee: 1420,
                counter: '2156606',
                gas_limit: '10600',
                storage_limit: '300',
                op_id: 0,
                public_key: null,
                amount: 192000000,
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                parameters: '{ "prim": "Unit" }',
                slots: null,
                entrypoint: 'default',
                contract_address: null,
                delegate: null,
                consumed_milligas: 10207000,
            },
        ];
        const relayPageData2: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const relayAfter2 = false;
        const orderBy2: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.ASC,
        };
        const res2 = connectionUtils.getOrderByWhereClause(cursorRecord2, relayPageData2, relayAfter2, orderBy2);
        expect(res2).toMatchSnapshot();
    });
});
