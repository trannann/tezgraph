import { LoggerFactory, RootLogger } from '../../../../src/utils/logging';
import { nameof } from '../../../../src/utils/reflection';
import { Clock } from '../../../../src/utils/time/clock';

class MyService {}

describe(LoggerFactory.name, () => {
    let target: LoggerFactory;
    let rootLogger: RootLogger;
    let clock: Clock;

    beforeEach(() => {
        rootLogger = {} as RootLogger;
        clock = {} as Clock;
        target = new LoggerFactory(rootLogger, clock);
    });

    const categories = [
        { type: 'string', value: 'FooBar', expected: 'FooBar' },
        { type: 'constructor', value: MyService, expected: 'MyService' },
    ];

    categories.forEach(category => {
        it(`${nameof<LoggerFactory>('getLogger')}(${category.type}) should create logger correctly`, () => {
            const logger: any = target.getLogger(category.value);

            expect(logger.category).toBe(category.expected);
            expect(logger.rootLogger).toBe(rootLogger);
            expect(logger.clock).toBe(clock);
        });
    });
});
