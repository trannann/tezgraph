import { instance, mock, spy, when } from 'ts-mockito';

import { toArray, toAsyncIterable } from '../../../../src/utils/collections/async-iterable-utils';
import { ConvertIterableProvider } from '../../../../src/utils/iterable/convert-iterable-provider';
import { IterableProvider } from '../../../../src/utils/iterable/interfaces';
import { ValueOrPromise } from '../../../../src/utils/reflection';

class TargetProvider extends ConvertIterableProvider<number, string> {
    convertItem(_item: number): ValueOrPromise<string> {
        throw new Error('Method not implemented.');
    }
}

describe(ConvertIterableProvider.name, () => {
    let target: TargetProvider;
    let innerProvider: IterableProvider<number>;
    let targetSpy: TargetProvider;

    beforeEach(() => {
        innerProvider = mock<IterableProvider<number>>();
        target = new TargetProvider(instance(innerProvider));
        targetSpy = spy(target);
    });

    it('should convert items', async () => {
        when(innerProvider.iterate()).thenReturn(toAsyncIterable([1, 2]));
        when(targetSpy.convertItem(1)).thenReturn('a');
        when(targetSpy.convertItem(2)).thenReturn(Promise.resolve('b'));

        const result = await toArray(target.iterate());

        expect(result).toEqual(['a', 'b']);
    });
});
