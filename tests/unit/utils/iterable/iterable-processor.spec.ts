import { anything, instance, mock, verify, when } from 'ts-mockito';

import { toAsyncIterable } from '../../../../src/utils/collections/async-iterable-utils';
import { ItemProcessor, IterableProvider } from '../../../../src/utils/iterable/interfaces';
import { IterableProcessor } from '../../../../src/utils/iterable/iterable-processor';
import { LogLevel } from '../../../../src/utils/logging';
import { TestLogger } from '../../mocks';
import { TestClock } from '../../mocks/test-clock';

describe(IterableProcessor.name, () => {
    let target: IterableProcessor<number>;
    let iterableProvider: IterableProvider<number>;
    let itemProcessor: ItemProcessor<number>;
    let clock: TestClock;
    let logger: TestLogger;

    beforeEach(() => {
        iterableProvider = mock<IterableProvider<number>>();
        itemProcessor = mock<ItemProcessor<number>>();
        const itemInfoProvider = { getInfo: (x: number) => `info-${x}` };
        clock = new TestClock();
        logger = new TestLogger();
        target = new IterableProcessor(instance(iterableProvider), instance(itemProcessor), itemInfoProvider, clock, logger);

        when(iterableProvider.iterate()).thenReturn(toAsyncIterable([1, 2]));
    });

    it('should process items', async () => {
        const promise = target.processItems();
        expect(target.currentIterable).toBeTruthy();
        await promise;

        verify(itemProcessor.processItem(anything())).times(2);
        verify(itemProcessor.processItem(1)).calledBefore(itemProcessor.processItem(2));

        expect(target.lastItemInfo).toBe('info-2');
        expect(target.lastSuccessTime).toBe(clock.nowDate);
        expect(target.lastItemError).toBeUndefined();
        expect(target.currentIterable).toBeUndefined();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify(LogLevel.Information).verifyMessageIncludesAll('Start');
        logger.logged(1).verify(LogLevel.Information).verifyMessageIncludesAll('Stop');
    });

    it('should keep processing items if some item processing failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(1)).thenThrow(error);

        await target.processItems();

        verify(itemProcessor.processItem(2)).once();
        expect(target.lastItemError).toBeUndefined();
        logger.loggedSingle(LogLevel.Error).verifyData({ item: 'info-1', error });
    });

    it('should expose error if last item failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(2)).thenThrow(error);

        await target.processItems();

        expect(target.lastItemError).toBe(error);
    });
});
