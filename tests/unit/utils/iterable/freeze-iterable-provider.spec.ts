import { FreezeIterableProvider } from '../../../../src/utils/iterable/freeze-iterable-provider';
import { expectToThrow } from '../../mocks';

interface Foo {
    value: string;
}

describe(FreezeIterableProvider.name, () => {
    let target: FreezeIterableProvider<Foo>;

    beforeEach(() => {
        target = new FreezeIterableProvider(null!);
    });

    it('should freeze iterated items', () => {
        const foo = { value: 'aa' };

        const converted = target.convertItem(foo);

        expect(converted).toBe(foo);
        expectToThrow(() => {
            foo.value = 'bb';
        });
    });
});
