import { instance, mock, when } from 'ts-mockito';

import { toArray, toAsyncIterable } from '../../../../src/utils/collections/async-iterable-utils';
import { DeduplicateIterableProvider } from '../../../../src/utils/iterable/deduplicate-iterable-provider';
import { Identifier, IterableProvider } from '../../../../src/utils/iterable/interfaces';

interface Foo {
    id: string;
    value: string;
}

describe(DeduplicateIterableProvider.name, () => {
    let target: IterableProvider<Foo>;
    let innerProvider: IterableProvider<Foo>;

    beforeEach(() => {
        innerProvider = mock<IterableProvider<Foo>>();
        const identifier: Identifier<Foo> = { getId: (x: Foo) => x.id };
        target = new DeduplicateIterableProvider(instance(innerProvider), identifier, 100);
    });

    it('should deduplicate items', async () => {
        const items: Foo[] = [
            { id: '1', value: 'val.1.1' },
            { id: '2', value: 'val.2.1' },
            { id: '1', value: 'val.1.2' },
            { id: '2', value: 'val.2.2' },
            { id: '3', value: 'val.3' },
            { id: '2', value: 'val.2.3' },
        ];
        when(innerProvider.iterate()).thenReturn(toAsyncIterable(items));

        const result = await toArray(target.iterate());

        expect(result).toHaveLength(3);
        expect(result[0]).toBe(items[0]);
        expect(result[1]).toBe(items[1]);
        expect(result[2]).toBe(items[4]);
    });
});
