import { ApolloError, UserInputError } from 'apollo-server';
import { GraphQLError } from 'graphql';
import { instance, mock, verify, when } from 'ts-mockito';
import { ArgumentValidationError, ForbiddenError, UnauthorizedError } from 'type-graphql';

import { GraphQLErrorHandler } from '../../../src/bootstrap/graphql-error-handler';
import { ResolverContext } from '../../../src/bootstrap/resolver-context';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { LogLevel } from '../../../src/utils/logging';
import { Writable } from '../../../src/utils/reflection';
import { UuidGenerator } from '../../../src/utils/uuid-generator';
import { TestLogger } from '../mocks';

describe(GraphQLErrorHandler.name, () => {
    describe.each([
        'req',
        undefined,
    ])('with requestId = %s', (requestId) => {
        let target: GraphQLErrorHandler;
        let uuidGenerator: UuidGenerator;
        let envConfig: Writable<EnvConfig>;
        let logger: TestLogger;

        const act = (error: GraphQLError) => target.handle(error, { requestId } as ResolverContext);

        beforeEach(() => {
            uuidGenerator = mock(UuidGenerator);
            envConfig = { enableDebug: false } as EnvConfig;
            logger = new TestLogger();
            target = new GraphQLErrorHandler(instance(uuidGenerator), envConfig, logger);

            when(uuidGenerator.generate()).thenReturn('uuu');
        });

        it('should log error with generated ID', () => {
            const inputError = getGraphQLError(new Error('wtf'));

            const resultError = act(inputError);

            expect(resultError.message).toContain('uuu');
            expect(resultError.message).not.toContain('omg');
            expect(resultError.message).not.toContain('wtf');
            expect(resultError).toContainAllKeys(['message']);
            logger.loggedSingle().verify(LogLevel.Error, { error: inputError, errorId: 'uuu', requestId });
        });

        it('should return original error if debug', () => {
            envConfig.enableDebug = true;
            const inputError = getGraphQLError(new Error('wtf'));

            const resultError = act(inputError);

            expect(resultError.message).toIncludeMultiple(['uuu', 'omg']);
            expect(resultError.path).toEqual(['err-path']);
            logger.loggedSingle().verify(LogLevel.Error, { error: inputError, errorId: 'uuu', requestId });
        });

        it.each([
            ['ApolloError', new ApolloError('wtf')],
            ['inherited from ApolloError', new UserInputError('wtf')],
            ['TypeGraphQL ArgumentValidationError', new ArgumentValidationError([])],
            ['TypeGraphQL ForbiddenError', new ForbiddenError()],
            ['TypeGraphQL UnauthorizedError', new UnauthorizedError()],
        ])('should not change error if %s', (_desc, originalError) => {
            const inputError = getGraphQLError(originalError);

            const resultError = act(inputError);

            expect(resultError).toBe(inputError);
            verify(uuidGenerator.generate()).never();
            logger.loggedSingle().verify(LogLevel.Warning, { error: inputError, requestId });
        });

        function getGraphQLError(original: Error) {
            return new GraphQLError('omg', undefined, undefined, undefined, ['err-path'], original);
        }
    });
});
