import { scalars } from '../../../src/entity/scalars';

describe('Scalars', () => {
    for (const [name, scalar] of Object.entries(scalars)) {
        it(`scalars.${name} should have name "${name}"`, () => {
            expect(scalar.name).toBe(name);
        });
    }
});
