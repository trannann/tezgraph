import {
    MempoolOperationGroupIdentifier,
} from '../../../../../src/entity/subscriptions/helpers/mempool-operation-group-identifier';
import {
    MempoolOperationGroup,
    OperationNotification,
} from '../../../../../src/entity/subscriptions/operation-notification';

describe(MempoolOperationGroupIdentifier.name, () => {
    const target = new MempoolOperationGroupIdentifier();

    it('should get signature from first operation in the group', () => {
        const group: MempoolOperationGroup = [
            { info: { signature: 's1' } } as OperationNotification,
            { info: { signature: 's2' } } as OperationNotification,
        ];

        const id = target.getId(group);

        expect(id).toBe('s1');
    });

    it.each([
        ['no operations in the group', []],
        ['no operation signature', [{ info: {} } as OperationNotification]],
        ['white-space operation signature', [{ info: { signature: '  ' } } as OperationNotification]],
    ])('should throw if %s', (_desc, group) => {
        expect(() => target.getId(group)).toThrow();
    });
});
