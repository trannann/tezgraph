import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { BlockInfoProvider } from '../../../../../src/entity/subscriptions/helpers/block-info-provider';
import { asReadonly } from '../../../../../src/utils/conversion';

describe(BlockInfoProvider.name, () => {
    const target = new BlockInfoProvider();

    it('should collect block info', () => {
        const block = {
            hash: 'hh',
            header: {
                level: 111,
                timestamp: new Date(222),
            },
            operations: asReadonly([{}, {}, {}]),
        } as BlockNotification;

        const info = target.getInfo(block);

        expect(info).toEqual({
            hash: 'hh',
            level: 111,
            timestamp: new Date(222),
            operationCount: 3,
        });
    });
});
