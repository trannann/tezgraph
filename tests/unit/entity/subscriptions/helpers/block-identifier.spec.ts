import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { BlockIdentifier } from '../../../../../src/entity/subscriptions/helpers/block-identifier';

describe(BlockIdentifier.name, () => {
    const target = new BlockIdentifier();

    it('should return block hash as its id', () => {
        const block = { hash: 'haha' } as BlockNotification;

        const id = target.getId(block);

        expect(id).toBe('haha');
    });
});
