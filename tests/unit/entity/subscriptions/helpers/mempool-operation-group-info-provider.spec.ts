import {
    MempoolOperationGroupInfoProvider,
} from '../../../../../src/entity/subscriptions/helpers/mempool-operation-group-info-provider';
import {
    MempoolOperationGroup,
    OperationNotification,
} from '../../../../../src/entity/subscriptions/operation-notification';

describe(MempoolOperationGroupInfoProvider.name, () => {
    const target = new MempoolOperationGroupInfoProvider();

    it('should get info about mempool operation group', () => {
        const group: MempoolOperationGroup = [
            { info: { signature: 's1' } } as OperationNotification,
            { info: { signature: 's2' } } as OperationNotification,
        ];

        const info = target.getInfo(group);

        expect(info).toEqual({
            signature: 's1',
            operationCount: 2,
        });
    });

    it.each([
        ['no operation', []],
        ['no signature', [{} as OperationNotification]],
        ['white-space signature', [{ info: { signature: '  ' } } as OperationNotification]],
    ])('should throw if %s in the group', (_desc, group) => {
        expect(() => target.getInfo(group)).toThrow();
    });
});
