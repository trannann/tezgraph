import { deepEqual, instance, mock, when } from 'ts-mockito';

import { Filter } from '../../../../../src/entity/subscriptions/filters/filter';
import { DefaultConstructor, getEnumValues } from '../../../../../src/utils/reflection';

export enum FilterResult {
    Undefined = 'Undefined',
    Passed = 'Passed',
    NotPassed = 'NotPassed',
}

export const filterResults: readonly FilterResult[] = getEnumValues(FilterResult);

export function combineFilterResults(count: 3): [FilterResult, FilterResult, FilterResult][];
export function combineFilterResults(count: number): FilterResult[][];
export function combineFilterResults(count: number): FilterResult[][] {
    return count === 1
        ? filterResults.map(r => [r])
        : combineFilterResults(count - 1).flatMap(a => filterResults.map(r => [...a, r]));
}

export function getFilterTestCases(count: 1): [boolean, FilterResult][];
export function getFilterTestCases(count: 2): [boolean, FilterResult, FilterResult][];
export function getFilterTestCases(count: 3): [boolean, FilterResult, FilterResult, FilterResult][];
export function getFilterTestCases(count: 4): [boolean, FilterResult, FilterResult, FilterResult, FilterResult][];
export function getFilterTestCases(count: number): [boolean, ...FilterResult[]][] {
    return combineFilterResults(count).map(r => [shouldPass(...r), ...r]);
}

export function shouldPass(...results: FilterResult[]): boolean {
    return !results.some(s => s === FilterResult.NotPassed);
}

export function mockFilter<TValue, TFilter extends Filter<TValue>>(
    filterType: DefaultConstructor<TFilter>,
    state: FilterResult,
    expectedValue: TValue,
): TFilter | undefined {
    if (state === FilterResult.Undefined) {
        return undefined;
    }
    const filter = mock<TFilter>(filterType);
    when(filter.passes(deepEqual(expectedValue))).thenReturn(state === FilterResult.Passed);
    return instance(filter);
}
