import { SeedNonceRevelationSpecificFilter } from '../../../../../src/entity/subscriptions/args/seed-nonce-revelation-args';
import { nameof } from '../../../../../src/utils/reflection';

describe(`${SeedNonceRevelationSpecificFilter.name}.${nameof<SeedNonceRevelationSpecificFilter>('passes')}()`, () => {
    it(`should return true`, () => {
        const target = new SeedNonceRevelationSpecificFilter();

        const passed = target.passes();

        expect(passed).toBe(true);
    });
});
