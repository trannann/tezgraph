import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import { OperationKind } from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    AssignBlockToOperationsProvider,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block-pipeline/assign-block-to-operations-provider';
import { asReadonly, deepFreeze } from '../../../../../../src/utils/conversion';

describe(AssignBlockToOperationsProvider.name, () => {
    let target: AssignBlockToOperationsProvider;

    beforeEach(() => {
        target = new AssignBlockToOperationsProvider(null!);
    });

    it('should assign block to operations of cloned block', () => {
        const inputBlock = deepFreeze({
            hash: 'haha',
            operations: asReadonly([
                { kind: OperationKind.ballot },
                { kind: OperationKind.reveal },
            ]),
        }) as BlockNotification;

        const block = target.convertItem(inputBlock);

        expect(block.hash).toBe('haha');
        expect(block.operations[0]?.kind).toBe(OperationKind.ballot);
        expect(block.operations[0]?.block).toBe(block);
        expect(block.operations[1]?.kind).toBe(OperationKind.reveal);
        expect(block.operations[1]?.block).toBe(block);
    });
});
