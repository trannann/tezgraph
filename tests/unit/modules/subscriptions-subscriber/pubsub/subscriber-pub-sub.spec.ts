import { OperationKind } from '../../../../../src/entity/subscriptions/operation-notification';
import { subscriberTriggers } from '../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { nameof } from '../../../../../src/utils/reflection';

describe('subscriberTriggers', () => {
    describe(nameof<typeof subscriberTriggers>('blocks'), () => {
        testTrigger(subscriberTriggers.blocks, 'BLOCKS');
    });

    describe(nameof<typeof subscriberTriggers>('blockOperations'), () => {
        testTrigger(subscriberTriggers.blockOperations[OperationKind.ballot], 'BLOCK_OPERATIONS:ballot');
        testTrigger(subscriberTriggers.blockOperations[OperationKind.transaction], 'BLOCK_OPERATIONS:transaction');
    });

    describe(nameof<typeof subscriberTriggers>('blockOperations'), () => {
        testTrigger(subscriberTriggers.mempoolOperations[OperationKind.reveal], 'MEMPOOL_OPERATIONS:reveal');
        testTrigger(subscriberTriggers.mempoolOperations[OperationKind.proposals], 'MEMPOOL_OPERATIONS:proposals');
    });

    function testTrigger(trigger: { name: string }, expectedName: string) {
        it(`should have correct name ${expectedName}`, () => {
            expect(trigger.name).toBe(expectedName);
        });
    }
});
