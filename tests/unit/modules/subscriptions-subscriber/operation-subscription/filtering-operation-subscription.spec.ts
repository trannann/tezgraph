import { anything, instance, mock, verify, when } from 'ts-mockito';

import { OperationFilter } from '../../../../../src/entity/subscriptions/args/operation-args';
import { BallotNotification } from '../../../../../src/entity/subscriptions/ballot-notification';
import { FilterOptimizer } from '../../../../../src/entity/subscriptions/filters/filter-optimizer';
import {
    FilteringOperationSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/filtering-operation-subscription';
import {
    OperationSubscription,
    SubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/operation-subscription';
import { toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { create } from '../../../../../src/utils/conversion';
import { Nullish } from '../../../../../src/utils/reflection';
import { generateRange } from '../../../mocks';

describe(FilteringOperationSubscription.name, () => {
    let target: OperationSubscription;
    let innerSubscription: OperationSubscription;
    let filterOptimizer: FilterOptimizer;

    beforeEach(() => {
        innerSubscription = mock<OperationSubscription>();
        filterOptimizer = mock(FilterOptimizer);
        target = new FilteringOperationSubscription(instance(innerSubscription), instance(filterOptimizer));
    });

    it('should yield item if it passes the filter', async () => {
        const ballots = generateRange(3, i => create(BallotNotification, { source: `src-${i}` }));
        const unoptimizedFilter = {} as OperationFilter<BallotNotification>;
        const filter = mock<OperationFilter<BallotNotification>>();
        const options = mockOptions(unoptimizedFilter);
        when(filterOptimizer.optimize(unoptimizedFilter)).thenReturn(instance(filter));
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(ballots));
        when(filter.passes(ballots[0])).thenReturn(true);
        when(filter.passes(ballots[1])).thenReturn(false);
        when(filter.passes(ballots[2])).thenReturn(true);

        const operations = await toArray(target.subscribe(options));

        expect(operations).toEqual([ballots[0], ballots[2]]);
    });

    it.each([
        ['undefined filter', undefined],
        ['null filter', null],
    ])('should return iterable from inner if %s', (_desc, filter) => {
        runReturnInnerIterableTest(filter);

        verify(filterOptimizer.optimize(anything())).never();
    });

    it('should return iterable from inner if filter optimized to null', () => {
        const filter = {} as OperationFilter<BallotNotification>;
        when(filterOptimizer.optimize(filter)).thenReturn(null);

        runReturnInnerIterableTest(filter);
    });

    function runReturnInnerIterableTest(filter: Nullish<OperationFilter<BallotNotification>>) {
        const options = mockOptions(filter);
        const innerIterable = {} as AsyncIterableIterator<BallotNotification>;
        when(innerSubscription.subscribe(options)).thenReturn(innerIterable);

        const iterable = target.subscribe(options);

        expect(iterable).toBe(innerIterable);
    }

    function mockOptions(filter: Nullish<OperationFilter<BallotNotification>>) {
        return { args: { filter } } as SubscriptionOptions<BallotNotification>;
    }
});
