import { instance, mock, when } from 'ts-mockito';

import { ResolverContext } from '../../../../../src/bootstrap/resolver-context';
import {
    AbstractLoggingSubscription,
    AbstractSubscription,
    AbstractSubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/helpers/abstract-logging-subscription';
import { toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { LogLevel } from '../../../../../src/utils/logging';
import { Writable } from '../../../../../src/utils/reflection';
import { expectToThrowAsync, TestLogger } from '../../../mocks';

class TargetSubscription extends AbstractLoggingSubscription {}

describe(AbstractLoggingSubscription.name, () => {
    let target: AbstractLoggingSubscription;
    let logger: TestLogger;
    let innerSubscription: AbstractSubscription;

    let options: Writable<AbstractSubscriptionOptions>;

    beforeEach(() => {
        logger = new TestLogger();
        innerSubscription = mock<AbstractSubscription>();
        target = new TargetSubscription(logger, instance(innerSubscription));

        options = {
            subscriptionName: 'fooAdded',
            context: {
                requestId: 'rr',
                connection: {
                    query: 'qq',
                    variables: { ['var' as string]: 123 as any },
                },
            } as ResolverContext,
            args: 'aa',
        };
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(['lol', 'wtf']));
    });

    it('should log subscription info before subscribed', async () => {
        const items = await toArray(target.subscribe(options));

        expect(items).toEqual(['lol', 'wtf']);
        logger.loggedSingle().verify(LogLevel.Information, {
            subscription: 'fooAdded',
            requestId: 'rr',
            args: 'aa',
            rawQuery: 'qq',
            rawQueryVariables: { var: 123 },
        });
    });

    it('should handle no connection', () => {
        options.context = {} as ResolverContext;

        target.subscribe(options);

        expect(logger.loggedSingle().data).toContainEntries([
            ['rawQuery', undefined],
            ['rawQueryVariables', undefined],
        ]);
    });

    it('should log subscription closing if iterator.return()', async () => {
        const iterator = target.subscribe(options);

        await iterator.return!();

        verifyLoggedClosing();
    });

    it('should log subscription closing if iterator.throw()', async () => {
        const iterable = target.subscribe(options);
        const testError = new Error('lol');

        const error = await expectToThrowAsync(async () => iterable.throw!(testError));

        expect(error).toBe(testError);
        verifyLoggedClosing();
    });

    function verifyLoggedClosing() {
        logger.verifyLoggedCount(2);
        logger.logged(1).verify(LogLevel.Information, {
            subscription: 'fooAdded',
            requestId: 'rr',
        });
    }
});
