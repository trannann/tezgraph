import { instance, mock, when } from 'ts-mockito';

import { Names, RedisPubSubEnvConfig } from '../../../../src/modules/redis-pubsub/redis-pubsub-env-config';
import { EnvConfigProvider } from '../../../../src/utils/configuration/env-config-provider';

describe(RedisPubSubEnvConfig.name, () => {
    let env: EnvConfigProvider;
    const act = () => new RedisPubSubEnvConfig(instance(env));

    beforeEach(() => {
        env = mock(EnvConfigProvider);

        when(env.getString(Names.RedisConnectionString)).thenReturn('red-con');
    });

    it(`should be created correctly`, () => {
        const config = act();

        expect(config).toEqual<RedisPubSubEnvConfig>({
            redisConnectionString: 'red-con',
        });
    });
});
