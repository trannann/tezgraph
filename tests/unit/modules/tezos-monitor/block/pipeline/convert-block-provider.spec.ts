import { BlockResponse } from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import { ConvertBlocksProvider } from '../../../../../../src/modules/tezos-monitor/block/pipeline/convert-block-provider';
import { RpcMonitorBlockHeader } from '../../../../../../src/modules/tezos-monitor/block/rpc-monitor-block-header';
import { BlockConverter } from '../../../../../../src/rpc/converters/block-converter';
import { TezosRpcClient } from '../../../../../../src/rpc/tezos-rpc-client';

describe(ConvertBlocksProvider.name, () => {
    let target: ConvertBlocksProvider;
    let rpcClient: TezosRpcClient;
    let blockConverter: BlockConverter;

    beforeEach(() => {
        rpcClient = mock<TezosRpcClient>();
        blockConverter = mock<BlockConverter>();
        target = new ConvertBlocksProvider(instance(rpcClient), instance(blockConverter), null!);
    });

    it('should get block from RPC and convert it', async () => {
        const monitorBlock = { hash: 'haha' } as RpcMonitorBlockHeader;
        const rpcBlock = {} as BlockResponse;
        const finalBlock = {} as BlockNotification;
        when(rpcClient.getBlock('haha')).thenResolve(rpcBlock);
        when(blockConverter.convert(rpcBlock)).thenReturn(finalBlock);

        const block = await target.convertItem(monitorBlock);

        expect(block).toBe(finalBlock);
    });
});
