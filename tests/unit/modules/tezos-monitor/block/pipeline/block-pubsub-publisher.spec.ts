import { anything, instance, mock, verify } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../../../src/entity/subscriptions/operation-notification';
import { BlockPubSubPublisher } from '../../../../../../src/modules/tezos-monitor/block/pipeline/block-pubsub-publisher';
import { asReadonly } from '../../../../../../src/utils/conversion';
import { ItemProcessor } from '../../../../../../src/utils/iterable/interfaces';
import { LogLevel } from '../../../../../../src/utils/logging';
import { ExternalPubSub, externalTriggers } from '../../../../../../src/utils/pubsub/external-pub-sub';
import { TestLogger } from '../../../../mocks';

describe(BlockPubSubPublisher.name, () => {
    let target: ItemProcessor<BlockNotification>;
    let externalPubSub: ExternalPubSub;
    let logger: TestLogger;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        logger = new TestLogger();
        target = new BlockPubSubPublisher(instance(externalPubSub), logger);
    });

    it('should publish block', () => {
        const block = {
            hash: 'haha',
            operations: asReadonly([{} as OperationNotification, {} as OperationNotification]),
        } as BlockNotification;

        target.processItem(block);

        verify(externalPubSub.publish(anything(), anything())).once();
        verify(externalPubSub.publish(externalTriggers.blocks, block)).once();

        logger.loggedSingle().verify(LogLevel.Information, {
            hash: 'haha',
            operationCount: 2,
        });
    });
});
