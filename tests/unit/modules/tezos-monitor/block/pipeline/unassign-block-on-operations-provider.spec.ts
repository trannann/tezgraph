import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    UnassignBlockOnOperationsProvider,
} from '../../../../../../src/modules/tezos-monitor/block/pipeline/unassign-block-on-operations-provider';
import { asReadonly, deepFreeze } from '../../../../../../src/utils/conversion';

describe(UnassignBlockOnOperationsProvider.name, () => {
    let target: UnassignBlockOnOperationsProvider;

    beforeEach(() => {
        target = new UnassignBlockOnOperationsProvider(null!);
    });

    it('should assign block to operations of cloned block', () => {
        const inputBlock = deepFreeze({
            hash: 'haha',
            operations: asReadonly([
                { kind: OperationKind.ballot, block: {} } as OperationNotification,
                { kind: OperationKind.reveal, block: {} } as OperationNotification,
            ]),
        }) as BlockNotification;

        const block = target.convertItem(inputBlock);

        expect(block.hash).toBe('haha');
        expect(block.operations[0]!.kind).toBe(OperationKind.ballot);
        expect(block.operations[0]!.block).toBeUndefined();
        expect(block.operations[1]!.kind).toBe(OperationKind.reveal);
        expect(block.operations[1]!.block).toBeUndefined();
    });
});
