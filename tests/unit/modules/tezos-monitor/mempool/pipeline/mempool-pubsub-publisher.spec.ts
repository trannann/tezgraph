import { anything, instance, mock, verify } from 'ts-mockito';

import {
    MempoolOperationGroup,
    OperationKind,
    OperationNotification,
} from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    MempoolPubSubPublisher,
} from '../../../../../../src/modules/tezos-monitor/mempool/pipeline/mempool-pubsub-publisher';
import { ItemProcessor } from '../../../../../../src/utils/iterable/interfaces';
import { LogLevel } from '../../../../../../src/utils/logging';
import { ExternalPubSub, externalTriggers } from '../../../../../../src/utils/pubsub/external-pub-sub';
import { TestLogger } from '../../../../mocks';

describe(MempoolPubSubPublisher.name, () => {
    let target: ItemProcessor<MempoolOperationGroup>;
    let externalPubSub: ExternalPubSub;
    let logger: TestLogger;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        logger = new TestLogger();
        target = new MempoolPubSubPublisher(instance(externalPubSub), logger);
    });

    it('should publish mempool operation group', () => {
        const group: MempoolOperationGroup = [
            { kind: OperationKind.ballot, info: { signature: 's1' } } as OperationNotification,
            { kind: OperationKind.reveal, info: { signature: 's2' } } as OperationNotification,
        ];

        target.processItem(group);

        verify(externalPubSub.publish(anything(), anything())).once();
        verify(externalPubSub.publish(externalTriggers.mempoolOperationGroups, group)).once();

        logger.loggedSingle().verify(LogLevel.Information, {
            signature: 's1',
            operationCount: 2,
        });
    });

    it('should throw if empty group', () => {
        const group: MempoolOperationGroup = [];

        expect(() => target.processItem(group)).toThrow();
    });
});
