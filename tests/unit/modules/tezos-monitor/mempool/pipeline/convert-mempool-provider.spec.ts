import { instance, mock, when } from 'ts-mockito';

import { OperationNotification } from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    ConvertMempoolProvider,
} from '../../../../../../src/modules/tezos-monitor/mempool/pipeline/convert-mempool-provider';
import { RpcMempoolOperationGroup } from '../../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group';
import { OperationGroupConverter } from '../../../../../../src/rpc/converters/operation-group-converter';

describe(ConvertMempoolProvider.name, () => {
    let target: ConvertMempoolProvider;
    let operationGroupConverter: OperationGroupConverter;

    beforeEach(() => {
        operationGroupConverter = mock(OperationGroupConverter);
        target = new ConvertMempoolProvider(instance(operationGroupConverter), null!);
    });

    it('should convert mempool operation group', () => {
        const rpcGroup = {} as RpcMempoolOperationGroup;
        const finalGroup = [{} as OperationNotification];
        when(operationGroupConverter.convert(rpcGroup)).thenReturn(finalGroup);

        const group = target.convertItem(rpcGroup);

        expect(group).toBe(finalGroup);
    });
});
