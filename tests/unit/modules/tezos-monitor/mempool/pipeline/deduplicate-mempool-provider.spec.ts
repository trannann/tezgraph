import {
    RpcMempoolOperationGroupIdentifier,
} from '../../../../../../src/modules/tezos-monitor/mempool/pipeline/deduplicate-mempool-provider';
import { RpcMempoolOperationGroup } from '../../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group';

describe(RpcMempoolOperationGroupIdentifier.name, () => {
    const target = new RpcMempoolOperationGroupIdentifier();

    it('should get signature as id of mempool operation group ', () => {
        const group = { signature: 'ss' } as RpcMempoolOperationGroup;

        const id = target.getId(group);

        expect(id).toBe('ss');
    });
});
