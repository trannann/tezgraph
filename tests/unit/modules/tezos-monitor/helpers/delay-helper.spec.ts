import { anything, instance, mock, verify, when } from 'ts-mockito';

import { MonitorDelayHelper } from '../../../../../src/modules/tezos-monitor/helpers/monitor-delay-helper';
import { TezosMonitorEnvConfig } from '../../../../../src/modules/tezos-monitor/tezos-monitor-env-config';
import { asReadonly } from '../../../../../src/utils/conversion';
import { SleepHelper } from '../../../../../src/utils/time/sleep-helper';

describe(MonitorDelayHelper.name, () => {
    let target: MonitorDelayHelper;
    let sleepHelper: SleepHelper;

    beforeEach(() => {
        const envConfig = { rpcMonitorReconnectDelaysMillis: asReadonly([100, 200, 300]) } as TezosMonitorEnvConfig;
        sleepHelper = mock(SleepHelper);
        target = new MonitorDelayHelper(envConfig, instance(sleepHelper));

        when(sleepHelper.sleep(anything())).thenResolve();
    });

    it(`should wait first delay`, async () => {
        await waitTimes(1);

        expect(target.isInInfiniteLoop).toBe(false);
        verify(sleepHelper.sleep(anything())).once();
        verify(sleepHelper.sleep(100)).once();
    });

    it(`should wait third delay`, async () => {
        await waitTimes(3);

        expect(target.isInInfiniteLoop).toBe(true);
        verify(sleepHelper.sleep(anything())).times(3);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
        verify(sleepHelper.sleep(200)).calledBefore(sleepHelper.sleep(300));
    });

    it(`should wait last delay`, async () => {
        await waitTimes(5);

        expect(target.isInInfiniteLoop).toBe(true);
        verify(sleepHelper.sleep(anything())).times(5);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
        verify(sleepHelper.sleep(200)).calledBefore(sleepHelper.sleep(300));
        verify(sleepHelper.sleep(300)).times(3);
    });

    it(`should wait second delay after reset`, async () => {
        await waitTimes(2);
        target.reset();
        await waitTimes(2);

        expect(target.isInInfiniteLoop).toBe(false);
        verify(sleepHelper.sleep(anything())).times(4);
        verify(sleepHelper.sleep(100)).times(2);
        verify(sleepHelper.sleep(200)).times(2);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
    });

    async function waitTimes(count: number) {
        for (let i = 0; i < count; i++) {
            await target.wait();
        }
    }
});
