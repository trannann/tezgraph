import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import {
    AbstractRetryMonitorProvider,
    healthMessages,
} from '../../../../../src/modules/tezos-monitor/helpers/abstract-retry-monitor-provider';
import { MonitorDelayHelper } from '../../../../../src/modules/tezos-monitor/helpers/monitor-delay-helper';
import { take, toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { ComponentHealthState } from '../../../../../src/utils/health/component-health-state';
import { HealthStatus } from '../../../../../src/utils/health/health-check';
import { IterableProvider } from '../../../../../src/utils/iterable/interfaces';
import { LogLevel } from '../../../../../src/utils/logging';
import { TestAbortError, TestLogger } from '../../../mocks';

describe(AbstractRetryMonitorProvider, () => {
    let target: IterableProvider<number>;
    let innerProvider: IterableProvider<number>;
    let healthState: ComponentHealthState;
    let delayHelper: MonitorDelayHelper;
    let logger: TestLogger;

    const act = async (count: number) => toArray(take(target.iterate(), count));

    beforeEach(() => {
        innerProvider = mock<IterableProvider<number>>();
        healthState = mock(ComponentHealthState);
        delayHelper = mock(MonitorDelayHelper);
        logger = new TestLogger();
        target = new AbstractRetryMonitorProvider(instance(innerProvider), instance(healthState), instance(delayHelper), logger);
    });

    it('should continuously iterate data', async () => {
        when(innerProvider.iterate()).thenReturn(toAsyncIterable([42, 43, 44]));

        const items = await act(3);

        expect(items).toEqual([42, 43, 44]);
        logger.verifyNothingLogged();

        verify(healthState.set(anything(), anything())).times(3);
        verify(healthState.set(HealthStatus.Healthy, healthMessages.healthy)).times(3);

        verify(delayHelper.wait()).never();
        verify(delayHelper.reset()).times(3);
    });

    it.each([
        [false, HealthStatus.Degraded, healthMessages.temporaryFailure],
        [true, HealthStatus.Unhealthy, healthMessages.persistentFailure],
    ])('should retry on failure if it is %s persistent', async (isPersistentError, expectedStatus, expectedReason) => {
        const error = new Error('Oups');
        when(innerProvider.iterate())
            .thenThrow(error)
            .thenReturn(toAsyncIterable([42]));
        when(delayHelper.isInInfiniteLoop).thenReturn(isPersistentError);

        const items = await act(1);

        expect(items).toEqual([42]);
        logger.loggedSingle().verify(LogLevel.Error, { error });

        verify(healthState.set(anything(), anything())).twice();
        verify(healthState.set(expectedStatus, deepEqual({ error, reason: expectedReason })))
            .calledBefore(healthState.set(HealthStatus.Healthy, healthMessages.healthy));

        verify(delayHelper.wait()).calledBefore(delayHelper.reset());
    });

    it('should retry on connection end', async () => {
        when(innerProvider.iterate())
            .thenReturn(toAsyncIterable([42]))
            .thenReturn(toAsyncIterable([43]));

        const items = await act(2);

        expect(items).toEqual([42, 43]);
        logger.loggedSingle(LogLevel.Error);
        expect(logger.logged(0).data?.error.toString()).toContain('ended');
    });

    it('should end on connection abort', async () => {
        when(innerProvider.iterate())
            .thenThrow(new TestAbortError());

        const items = await toArray(target.iterate());

        expect(items).toBeEmpty();
        logger.loggedSingle().verify(LogLevel.Information);
    });
});
