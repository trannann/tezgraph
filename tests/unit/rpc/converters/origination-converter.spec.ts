import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { OriginationMetadata, OriginationNotification } from '../../../../src/entity/subscriptions/origination-notification';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    OriginationResultConverter,
} from '../../../../src/rpc/converters/operation-results/origination-result-converter';
import { OriginationConverter } from '../../../../src/rpc/converters/origination-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(OriginationConverter.name, () => {
    let target: OriginationConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: OriginationResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultOrigination;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(OriginationResultConverter);
        target = new OriginationConverter(instance(metadataConverter), resultConverter);
    });

    it(`should convert values correctly`, () => {
        rpcOperation = {
            kind: rpc.OpKind.ORIGINATION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            balance: '555',
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            metadata: null!,
        };
        const metadata = mockOperationMetadata() as OriginationMetadata;
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        expect(operation).toEqual<OriginationNotification>({
            kind: OperationKind.origination,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            balance: BigInt(555),
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            ...mockBaseProperties(),
            metadata,
        });
    });
});
