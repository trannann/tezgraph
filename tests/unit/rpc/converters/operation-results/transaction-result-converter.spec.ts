import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { BigMapDiff } from '../../../../../src/entity/subscriptions/big-map-diff';
import { TransactionResult } from '../../../../../src/entity/subscriptions/transaction-notification';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common/balance-update-converter';
import { BigMapDiffConverter } from '../../../../../src/rpc/converters/common/big-map-diff-converter';
import {
    TransactionResultConverter,
} from '../../../../../src/rpc/converters/operation-results/transaction-result-converter';
import { mockBalanceUpdate } from '../mocks';
import { mockedBaseOpResultProps } from './base-operation-result.mock';

describe(TransactionResultConverter.name, () => {
    let target: TransactionResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;

    const act = (r: rpc.OperationResultTransaction) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        bigMapDiffConverter = mock(BigMapDiffConverter);
        target = new TransactionResultConverter(instance(balanceUpdateConverter), instance(bigMapDiffConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultTransaction = {
            ...mockedBaseOpResultProps.getRpc(),
            storage: { prim: 'storage-michelson' },
            big_map_diff: [{ key_hash: 'diff-input' } as rpc.ContractBigMapDiffItem],
            balance_updates: [{ contract: 'balance-input' } as rpc.OperationBalanceUpdatesItem],
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
            allocated_destination_contract: true,
        };
        const convertedBigMapDiff = [{ key_hash: 'diff-converted' } as BigMapDiff];
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(bigMapDiffConverter.convertNullish(rpcResult.big_map_diff)).thenReturn(convertedBigMapDiff);
        when(balanceUpdateConverter.convertNullish(rpcResult.balance_updates)).thenReturn(convertedBalanceUpdates);

        const result = act(rpcResult);

        expect(result).toEqual<TransactionResult>({
            ...mockedBaseOpResultProps.getExpected(),
            typeName: 'TransactionResult',
            storage: { prim: 'storage-michelson' },
            big_map_diff: convertedBigMapDiff,
            balance_updates: convertedBalanceUpdates,
            originated_contracts: ['c1', 'c2'],
            storage_size: BigInt(222),
            paid_storage_size_diff: BigInt(333),
            allocated_destination_contract: true,
        });
    });
});
