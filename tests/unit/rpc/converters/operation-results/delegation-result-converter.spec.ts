import * as rpc from '@taquito/rpc';

import { DelegationResult } from '../../../../../src/entity/subscriptions/delegation-notification';
import { DelegationResultConverter } from '../../../../../src/rpc/converters/operation-results/delegation-result-converter';
import { mockedBaseOpResultProps } from './base-operation-result.mock';

describe(DelegationResultConverter.name, () => {
    const target = new DelegationResultConverter();

    const act = (r: rpc.OperationResultDelegation) => target.convert(r);

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultDelegation = mockedBaseOpResultProps.getRpc();

        const result = act(rpcResult);

        expect(result).toEqual<DelegationResult>({
            ...mockedBaseOpResultProps.getExpected(),
            typeName: 'DelegationResult',
        });
    });
});
