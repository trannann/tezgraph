import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { TransactionMetadata, TransactionNotification } from '../../../../src/entity/subscriptions/transaction-notification';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    TransactionResultConverter,
} from '../../../../src/rpc/converters/operation-results/transaction-result-converter';
import { TransactionConverter } from '../../../../src/rpc/converters/transaction-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(TransactionConverter.name, () => {
    let target: TransactionConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: TransactionResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultTransaction;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(TransactionResultConverter);
        target = new TransactionConverter(instance(metadataConverter), resultConverter);
    });

    it(`should convert values correctly`, () => {
        rpcOperation = {
            kind: rpc.OpKind.TRANSACTION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            amount: '555',
            destination: 'ddd',
            parameters: {
                entrypoint: 'eee',
                value: { prim: 'michelson' },
            },
            metadata: null!,
        };
        const metadata = mockOperationMetadata() as TransactionMetadata;
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        expect(operation).toEqual<TransactionNotification>({
            kind: OperationKind.transaction,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            amount: BigInt(555),
            destination: 'ddd',
            parameters: {
                entrypoint: 'eee',
                value: { prim: 'michelson' },
            },
            ...mockBaseProperties(),
            metadata,
        });
    });
});
