import * as rpc from '@taquito/rpc';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { ProposalsNotification } from '../../../../src/entity/subscriptions/proposals-notification';
import { ProposalsConverter } from '../../../../src/rpc/converters/proposals-converter';
import { mockBaseProperties } from './mocks';

describe(ProposalsConverter.name, () => {
    const target = new ProposalsConverter();
    let rpcOperation: rpc.OperationContentsAndResultProposals;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    it(`should convert values correctly`, () => {
        rpcOperation = {
            kind: rpc.OpKind.PROPOSALS,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
        };

        const operation = act();

        expect(operation).toEqual<ProposalsNotification>({
            kind: OperationKind.proposals,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
            ...mockBaseProperties(),
        });
    });
});
