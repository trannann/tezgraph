import { BalanceUpdate } from '../../../../src/entity/subscriptions/balance-update';
import { BlockNotification } from '../../../../src/entity/subscriptions/block-notification';
import { OperationNotificationInfo, OperationOrigin } from '../../../../src/entity/subscriptions/operation-notification';
import { BaseOperationProperties } from '../../../../src/rpc/converters/operation-converter';
import { asReadonly } from '../../../../src/utils/conversion';

export function mockBaseProperties(): BaseOperationProperties {
    return {
        info: { protocol: 'ppp' } as OperationNotificationInfo,
        block: { hash: 'haha' } as BlockNotification,
        origin: OperationOrigin.BLOCK,
    };
}

export function mockOperationMetadata() {
    return {
        balance_updates: asReadonly([mockBalanceUpdate()]),
    };
}

export function mockBalanceUpdate() {
    return { contract: 'ccc' } as BalanceUpdate;
}
