import * as rpc from '@taquito/rpc';
import { anyNumber, instance, mock, spy, verify, when } from 'ts-mockito';

import { InternalOperationResult } from '../../../../../src/entity/subscriptions/operation-result';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common/balance-update-converter';
import {
    MetadataWithResult,
    OperationMetadataConverter,
    OperationResultConverter,
    RpcMetadataWithResult,
    RpcOperation,
} from '../../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    InternalOperationResultConverter,
} from '../../../../../src/rpc/converters/operation-results/internal-operation-result-converter';
import { nameof } from '../../../../../src/utils/reflection';
import { mockBalanceUpdate } from '../mocks';

describe(OperationMetadataConverter.name, () => {
    let target: OperationMetadataConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let internalOperationResultConverter: InternalOperationResultConverter;

    let rpcOperation: RpcOperation<RpcMetadataWithResult<number>>;
    let resultConverter: OperationResultConverter<number, string>;
    let expectedResult: MetadataWithResult<string>;

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        internalOperationResultConverter = mock(InternalOperationResultConverter);
        target = new OperationMetadataConverter(instance(balanceUpdateConverter), instance(internalOperationResultConverter));

        rpcOperation = {
            kind: rpc.OpKind.BALLOT,
            metadata: {
                balance_updates: [{ contract: 'balance-input' } as rpc.OperationMetadataBalanceUpdates],
                internal_operation_results: [{ source: 'src-input' } as rpc.InternalOperationResult],
                operation_result: 123,
            },
        };
        resultConverter = { convert: (r: number) => `result-${r}` };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        const convertedInternalResult = { source: 'src-converted' } as InternalOperationResult;
        expectedResult = {
            balance_updates: convertedBalanceUpdates,
            internal_operation_results: [convertedInternalResult],
            operation_result: 'result-123',
        };

        when(balanceUpdateConverter.convert(rpcOperation.metadata!.balance_updates)).thenReturn(convertedBalanceUpdates);
        when(internalOperationResultConverter.convert(rpcOperation.metadata!.internal_operation_results![0]!)).thenReturn(convertedInternalResult);
    });

    describe(nameof<OperationMetadataConverter>('convertSimple'), () => {
        it('should convert values correctly', () => {
            const metadata = target.convertSimple(rpcOperation);

            expect(metadata).toEqual({ balance_updates: expectedResult.balance_updates });
        });

        it('should return undefined if undefined metadata', () => {
            rpcOperation.metadata = undefined;

            const metadata = target.convertSimple(rpcOperation);

            expect(metadata).toBeUndefined();
        });
    });

    describe(nameof<OperationMetadataConverter>('convert'), () => {
        it('should convert values correctly', () => {
            const metadata = target.convert(rpcOperation, resultConverter);

            expect(metadata).toEqual(expectedResult);
        });

        it('should return undefined if undefined metadata', () => {
            rpcOperation.metadata = undefined;
            const resultConverterSpy = spy(resultConverter);

            const metadata = target.convert(rpcOperation, resultConverter);

            expect(metadata).toBeUndefined();
            verify(resultConverterSpy.convert(anyNumber())).never();
        });
    });
});
