import {
    convertBigInt,
    convertMutez,
    convertNullishBigInt,
    convertNullishMutez,
} from '../../../../../src/rpc/converters/common/big-int-converter';
import { Nullish } from '../../../../../src/utils/reflection';
import { expectToThrow } from '../../../mocks';

describe('BigInt converter', () => {
    const commonPassedTests = [
        { valueDesc: 'positive number', input: '123', expected: BigInt(123) },
        { valueDesc: 'bigger than regular number ', input: '9007199254740991123456', expected: BigInt('9007199254740991123456') },
        { valueDesc: 'zero', input: '0', expected: BigInt(0) },
    ];
    const negativeTests = [{ valueDesc: 'negative number', input: '-123', expected: BigInt(-123), expectedError: ['positive'] }];
    const expectedNullishError = [`can't be nullish`];
    const nullishPassedTests = [
        { valueDesc: 'empty string', input: '', expected: undefined, expectedError: expectedNullishError },
        { valueDesc: 'white-space string', input: '  ', expected: undefined, expectedError: expectedNullishError },
        { valueDesc: 'null', input: null!, expected: undefined, expectedError: expectedNullishError },
        { valueDesc: 'undefined', input: undefined, expected: undefined, expectedError: expectedNullishError },
    ];
    const commonFailedError = [SyntaxError.name];
    const commonFailedTests = [
        { valueDesc: 'not a number', input: 'abc', expectedError: commonFailedError },
        { valueDesc: 'number with decimal part', input: '1.2', expectedError: commonFailedError },
    ];

    describe(convertBigInt.name, () => {
        runPassedTests(convertBigInt, [
            ...commonPassedTests,
            ...negativeTests,
        ]);
        runFailedTests(convertBigInt, 'invalid big int', [
            ...nullishPassedTests,
            ...commonFailedTests,
        ]);
    });

    describe(convertNullishBigInt.name, () => {
        runPassedTests(convertNullishBigInt, [
            ...commonPassedTests,
            ...negativeTests,
            ...nullishPassedTests,
        ]);
        runFailedTests(convertNullishBigInt, 'invalid nullish big int', commonFailedTests);
    });

    describe(convertMutez.name, () => {
        runPassedTests(convertMutez, commonPassedTests);
        runFailedTests(convertMutez, 'invalid mutez', [
            ...negativeTests,
            ...nullishPassedTests,
            ...commonFailedTests,
        ]);
    });

    describe(convertNullishMutez.name, () => {
        runPassedTests(convertNullishMutez, [
            ...commonPassedTests,
            ...nullishPassedTests,
        ]);
        runFailedTests(convertNullishMutez, 'invalid nullish mutez', [
            ...negativeTests,
            ...commonFailedTests,
        ]);
    });

    function runPassedTests<TInput, TExpected>(
        act: (v: TInput) => TExpected,
        tests: { valueDesc: string; input: TInput; expected: TExpected }[],
    ) {
        for (const { valueDesc, input, expected } of tests) {
            it(`should convert correctly if value is ${valueDesc}`, () => {
                const actual = act(input);

                expect(actual).toEqual(expected);
            });
        }
    }

    function runFailedTests(
        act: (v: string) => any,
        expectedValueDesc: string,
        tests: { valueDesc: string; input: Nullish<string>; expectedError: readonly string[] }[],
    ) {
        for (const { valueDesc, input, expectedError } of tests) {
            it(`should convert correctly if value is ${valueDesc}`, () => {
                const error = expectToThrow(() => act(input!));

                expect(error.message).toIncludeMultiple([
                    ...expectedError,
                    expectedValueDesc,
                    `${JSON.stringify(input)}`,
                ]);
            });
        }
    }
});
