import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateKind,
} from '../../../../../src/entity/subscriptions/balance-update';
import {
    BalanceUpdateConverter,
    RpcBalanceUpdate,
} from '../../../../../src/rpc/converters/common/balance-update-converter';

describe(BalanceUpdateConverter.name, () => {
    const target = new BalanceUpdateConverter();
    let rpcUpdates: RpcBalanceUpdate[];

    beforeEach(() => {
        rpcUpdates = [
            {
                kind: 'contract',
                category: 'rewards',
                contract: 'ccc',
                delegate: 'ddd',
                cycle: 111,
                change: '222',
            },
        ];
    });

    describe(target.convert.name, () => {
        testConversion(u => target.convert(u));

        [undefined, null].forEach(input => {
            it(`should throw if ${input} input`, () => {
                expect(() => target.convert(input!)).toThrow();
            });
        });
    });

    describe(target.convertNullish.name, () => {
        testConversion(u => target.convertNullish(u));

        [undefined, null].forEach(input => {
            it(`should return undefined if ${input} input`, () => {
                const updates = target.convertNullish(input);

                expect(updates).toBeUndefined();
            });
        });
    });

    function testConversion(act: (u: RpcBalanceUpdate[]) => any) {
        it(`should convert values correctly`, () => {
            const updates = act(rpcUpdates);

            const expected: BalanceUpdate = {
                kind: BalanceUpdateKind.contract,
                category: BalanceUpdateCategory.rewards,
                contract: 'ccc',
                delegate: 'ddd',
                cycle: 111,
                change: BigInt(222),
            };
            expect(updates).toEqual([expected]);
        });

        it(`should support undefined "category"`, () => {
            rpcUpdates[0]!.category = undefined;

            const updates = act(rpcUpdates);

            expect(updates[0].category).toBeUndefined();
        });
    }
});
